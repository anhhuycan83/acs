// WindowAction.h: interface for the CWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WINDOWACTION_H__3F492294_9997_4AE9_AC06_10F30D5D644C__INCLUDED_)
#define AFX_WINDOWACTION_H__3F492294_9997_4AE9_AC06_10F30D5D644C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "FunctionHelper.h"

//enum eKindOfClick {LeftClick, RightClick, MiddleClick, ScrollUp, ScrollDown};
//
//enum eKindOfWindowAction {MouseWindowAction, KeyWindowAction};

// String support for Furious Five Master


class CWindowAction  : public CObject
{

public:
//	DECLARE_SERIAL(CWindowAction);
	CWindowAction();	
	virtual ~CWindowAction();
//	virtual void Serialize(CArchive& ar);
public:
	int m_milisecond;
	HWND m_hWnd;
	CString m_caption;//title
	CString m_className;
	long m_calssStyle;
	RECT m_rect;
	ULONG m_procId;
	eKindOfWindowAction m_eKindOfWindowAction;
	CWindowInfor m_parentWI;
protected:
	
};

class CMouseClick  : public CObject
{
public:
	DECLARE_SERIAL(CMouseClick);
	CMouseClick();
	virtual ~CMouseClick();
public:
	int m_milisecond;
	virtual void Serialize(CArchive &ar);
	CPoint m_poiterClick;
	eKindOfClick m_eKindOfClick;
};

class CKeyPress  : public CObject
{
public:
	DECLARE_SERIAL(CKeyPress);
	CKeyPress();
	virtual ~CKeyPress();
public:
	int m_milisecond;
	byte m_vkCode;
	virtual void Serialize(CArchive &ar);
	byte m_asciiCode;
	KeyState m_ks;
	eKindOfChar m_eKindOfChar;
};

#endif // !defined(AFX_WINDOWACTION_H__3F492294_9997_4AE9_AC06_10F30D5D644C__INCLUDED_)
