////////////////////////////////////////////////////////////////////////////
//
// WndPosManager.cpp
// 
// Copyright 2005 Xia Xiongjun( ���۾� ), All Rights Reserved.
//
// E-mail: ShongTu@Gmail.com
//
// This source file may be copyed, modified, redistributed  by any means
// PROVIDING that this notice and the author's name and all copyright 
// notices remain intact, and PROVIDING it is NOT sold for profit without 
// the authors expressed written consent. The author accepts no liability 
// for any damage/loss of business that this product may cause.
//
////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include ".\wndposmanager.h"

////////////////////////////////////////////////////////////////////////////
// class CWndPosManager

CWndPosManager::CWndPosManager(void)
{
	m_nResOption = WPR_RECOMMEND;
	::memset(&m_wp, 0, sizeof(m_wp));
	::memset(&m_wpDef, 0, sizeof(m_wpDef));

	m_bFirstRes = TRUE;
}

CWndPosManager::~CWndPosManager(void)
{
}

// XR: Save the window position information
BOOL CWndPosManager::SaveWndPos(HWND hWnd, LPCTSTR lpszEntry, 
								LPCTSTR lpszSection /*= _T("Position")*/) const
{
	ASSERT(::IsWindow(hWnd));
	ASSERT(lpszEntry != _T(""));

	if (!::GetWindowPlacement(hWnd, (WINDOWPLACEMENT *)&m_wp))
		return FALSE;

	CWinApp* pApp = ::AfxGetApp();
	if (!pApp->WriteProfileBinary(lpszSection, lpszEntry, (LPBYTE)&m_wp, sizeof(m_wp)))
		return FALSE;
	if (!pApp->WriteProfileInt(lpszSection, _T(""), m_nResOption))
		return FALSE;

	return TRUE;
}

// XR: Restore the window according to the saved window position information
BOOL CWndPosManager::RestoreWndPos(HWND hWnd, LPCTSTR lpszEntry, 
								   LPCTSTR lpszSection /*= _T("Position")*/) 
{
	ASSERT(::IsWindow(hWnd));
	ASSERT(lpszEntry != _T(""));

	CWinApp* pApp = ::AfxGetApp();

	if (m_bFirstRes == TRUE)
	{
		// XR: Get the current window placement which will be used as default later
		m_bFirstRes = FALSE;
		if (!::GetWindowPlacement(hWnd, &m_wpDef))
			return FALSE;
		CenterWndPos(m_wpDef);	// XR: Center the default window placement

		// XR: Get the initial restore option
		m_nResOption = pApp->GetProfileInt(lpszSection, _T(""), WPR_RECOMMEND);
	}

	// XR: Read the saved window position information
	UINT nSize;
	WINDOWPLACEMENT* pwp;
	if (pApp->GetProfileBinary(lpszSection, lpszEntry, (LPBYTE*)&pwp, &nSize))
	{
		// Success
		::memcpy((void *)&m_wp, pwp, sizeof(m_wp));
		delete [] pwp; // free the buffer
	}
	else
	{
		// Failed. It has not saved. The typical case is that this is 
		// the first time the application runs.
		::memcpy((void *)&m_wp,	&m_wpDef, sizeof(m_wpDef));
	}

	AdjustShowCmd(m_wp.showCmd, m_wp.flags);
	SetWindowPlacement(hWnd, &m_wp);

	return TRUE;
}

// XR: Restore the window to the default window position
BOOL CWndPosManager::RestoreDefWndPos(HWND hWnd, 
									  LPCTSTR lpszSection /*= _T("Position")*/) 
{
	return SetWindowPlacement(hWnd, &m_wpDef);
}

// XR: Get the restore option
int  CWndPosManager::GetResOption() const
{
	return m_nResOption;
}

// XR: Set the restore option
void CWndPosManager::SetResOption(int nResOption)
{
	m_nResOption = nResOption;
}

// XR: Reassign the showCmd according to the specified restore option
void CWndPosManager::AdjustShowCmd(UINT& showCmd, const UINT flags) const
{
	switch(m_nResOption)
	{
	case WPR_DIRECT:
		break;
	case WPR_NORMAL: 
		{
			showCmd = SW_SHOWNORMAL;
			break;
		}
	case WPR_MAXIMIZE:
		{
			showCmd = SW_SHOWMAXIMIZED;
			break;
		}
	case WPR_MINIMIZE:
		{
			showCmd = SW_SHOWMINIMIZED;
			break;
		}
	default:	
		{
			// XR: WPR_RECOMMEND is included
			// XR: The newly startup window will be placed to the position before it 
			//	   was minimized the last time.
			if (showCmd == SW_SHOWMINIMIZED)
				showCmd = flags + 1;
		}
	}
}

// XR: Center the window placement
void CWndPosManager::CenterWndPos(WINDOWPLACEMENT &wp)
{
	int xScr = ::GetSystemMetrics(SM_CXSCREEN);
	int yScr = ::GetSystemMetrics(SM_CYSCREEN);

	int xSz = wp.rcNormalPosition.right - wp.rcNormalPosition.left;
	int ySz = wp.rcNormalPosition.bottom - wp.rcNormalPosition.top;
	
	wp.rcNormalPosition.left   = (xScr - xSz)/2;
	wp.rcNormalPosition.right  = (xScr + xSz)/2;
	wp.rcNormalPosition.top    = (yScr - ySz)/2;
	wp.rcNormalPosition.bottom = (yScr + ySz)/2;
}


////////////////////////////////////////////////////////////////////////////
// SDI support: class CFrameWndEx 

IMPLEMENT_DYNCREATE(CFrameWndEx, CFrameWnd)

CFrameWndEx::CFrameWndEx(LPCTSTR szEntry /*= _T("MainWnd")*/)
{
	m_bEnableRes = FALSE;
	m_strEntry = szEntry;
}

CFrameWndEx::~CFrameWndEx()
{
}

BEGIN_MESSAGE_MAP(CFrameWndEx, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

// CFrameWndEx message handlers

int CFrameWndEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// XR: Hide the main window for the moment to avoid flicker
	AfxGetApp()->m_nCmdShow = SW_HIDE;
	
	// XR: Enable restore operation
	m_bEnableRes = TRUE;

	return 0;
}

void CFrameWndEx::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CFrameWnd::OnShowWindow(bShow, nStatus);

	// XR: Restore window position
	if (m_bEnableRes == TRUE && bShow == TRUE)
	{
		m_bEnableRes = FALSE;
		m_wpMgr.RestoreWndPos(m_hWnd, m_strEntry);
	}
}

void CFrameWndEx::OnDestroy()
{
	CFrameWnd::OnDestroy();

	// XR: Save window position
	m_wpMgr.SaveWndPos(m_hWnd, m_strEntry);
}


////////////////////////////////////////////////////////////////////////////
// MDI support: class CMDIFrameWndEx 

IMPLEMENT_DYNCREATE(CMDIFrameWndEx, CMDIFrameWnd)

CMDIFrameWndEx::CMDIFrameWndEx(LPCTSTR szEntry /*= _T("MainWnd")*/)
{
	m_bEnableRes = FALSE;
	m_strEntry = szEntry;
}

CMDIFrameWndEx::~CMDIFrameWndEx()
{
}

BEGIN_MESSAGE_MAP(CMDIFrameWndEx, CMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CMDIFrameWndEx message handlers

int CMDIFrameWndEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// XR: Enable restore operation
	m_bEnableRes = TRUE;

	return 0;
}

void CMDIFrameWndEx::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIFrameWnd::OnShowWindow(bShow, nStatus);

	// XR: Restore window position
	if (m_bEnableRes == TRUE && bShow == TRUE)
	{
		m_bEnableRes = FALSE;
		m_wpMgr.RestoreWndPos(m_hWnd, m_strEntry);
	}
}

void CMDIFrameWndEx::OnDestroy()
{
	CMDIFrameWnd::OnDestroy();

	// XR: Save window position
	m_wpMgr.SaveWndPos(m_hWnd, m_strEntry);
}


////////////////////////////////////////////////////////////////////////////
// Dialog support: class CDialogEx 

CDialogEx::CDialogEx(UINT nIDTemplate, CWnd* pParentWnd /*= NULL*/, 
					 LPCTSTR szEntry /*= _T("Dlg")*/)
	: CDialog(nIDTemplate, pParentWnd)
{
	m_strEntry = szEntry;
}

CDialogEx::~CDialogEx()
{
}

BEGIN_MESSAGE_MAP(CDialogEx, CDialog)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

BOOL CDialogEx::OnInitDialog()
{
	CDialog::OnInitDialog();

	// XR: Restore window position
	m_wpMgr.RestoreWndPos(m_hWnd, m_strEntry);

	return TRUE;  // return TRUE unless you set the focus to a control
}

// CDialogEx message handlers

void CDialogEx::OnDestroy()
{
	CDialog::OnDestroy();

	// XR: Save window position
	m_wpMgr.SaveWndPos(m_hWnd, m_strEntry);
}
