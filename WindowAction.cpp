// WindowAction.cpp: implementation of the CWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "WindowAction.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction class CWindowAction
//////////////////////////////////////////////////////////////////////

//IMPLEMENT_SERIAL(CWindowAction, CObject, 1);

CWindowAction::CWindowAction()
{
	m_hWnd = NULL;
	m_caption = _T("");//title
	m_className = _T("");
	m_calssStyle = 0;	
	m_procId = 0;
	//enum eKindOfWindowAction {KeyWindowAction, MouseWindowAction} m_eKindOfWindowAction;
	m_eKindOfWindowAction = KeyWindowAction;
	m_milisecond = 1000;
}

CWindowAction::~CWindowAction()
{

}


//void CWindowAction::Serialize(CArchive &ar)
//{
//	switch(m_eKindOfWindowAction)
//	{
//	case KeyWindowAction:
//		if(ar.IsStoring())
//		{
//			CString s;
//			s.Format(_T("%d"), m_eKindOfWindowAction);			
//			//ar << m_hWnd << m_caption << m_className << m_calssStyle;// << m_eKindOfWindowAction << m_strToEnter;		
//			ar << m_caption << m_className << m_calssStyle << s;		
//		}
//		else
//		{
//			//ar >> m_hWnd >> m_caption >> m_className >> m_calssStyle;// >> m_eKindOfWindowAction >> m_strToEnter;
//		}
//		break;
//	case MouseWindowAction:
//		break;
//	}
//	
//}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction CMouseClick
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CMouseClick, CObject, 1);
CMouseClick::CMouseClick()
{
	m_eKindOfClick = LeftClick;
	m_milisecond = 1000;
}

CMouseClick::~CMouseClick()
{
}


void CMouseClick::Serialize(CArchive &ar)
{
	CString x,y,sEnum, sMilisecond;
	if(ar.IsStoring())
	{			
		//s.Format(_T("%d"), m_eKindOfWindowAction);			
		//s2.Format(_T("0x%08X"), m_hWnd);
		//CFunctionHelper::convert_long_to_hex(sHwnd, (ULONG)m_hWnd);
		sEnum = CFunctionHelper::EnumToStringKindOfClick(m_eKindOfClick);		
		//ar << m_hWnd << m_caption << m_className << m_calssStyle;// << m_eKindOfWindowAction << m_strToEnter;		
		ar << sEnum;
		x.Format(_T("%d"), m_poiterClick.x);
		ar << x;
		y.Format(_T("%d"), m_poiterClick.y);
		ar << y;
		sMilisecond.Format(_T("%d"), m_milisecond);
		ar << sMilisecond;
	}
	else
	{
		//ar >> sHwnd >> m_caption >> m_className >> m_calssStyle >> sEnum >> m_strToEnter;
		ar >> sEnum;
		//m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sHwnd); //(sHwnd, (ULONG)m_hWnd);
		m_eKindOfClick = CFunctionHelper::StringToEnumKindOfClick(sEnum);// (m_eKindOfWindowAction);
		ar >> x;
		m_poiterClick.x = _ttoi(x);
		ar >> y;
		m_poiterClick.y = _ttoi(y);
		ar >> sMilisecond;
		m_milisecond = _ttoi(sMilisecond);

		//m_eKindOfWindowAction = _ttoi(s);
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction CMouseClick
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CKeyPress, CObject, 1);
CKeyPress::CKeyPress()
{
	m_eKindOfChar = NormalChar;
	m_milisecond = 1000;
}

CKeyPress::~CKeyPress()
{
}


void CKeyPress::Serialize(CArchive &ar)
{
	CString strAcscii, strVKCode, sLparam, sEnum, sMilisecond;
	if(ar.IsStoring())
	{			
		//s.Format(_T("%d"), m_eKindOfWindowAction);			
		//s2.Format(_T("0x%08X"), m_hWnd);
		//CFunctionHelper::convert_long_to_hex(sHwnd, (ULONG)m_hWnd);
		sEnum = CFunctionHelper::EnumToStringKindOfChar(m_eKindOfChar);		
		//ar << m_hWnd << m_caption << m_className << m_calssStyle;// << m_eKindOfWindowAction << m_strToEnter;		
		ar << sEnum;
		switch(m_eKindOfChar)
		{
		case NormalChar:
			strAcscii.Format(_T("%d"), m_asciiCode);
			ar << strAcscii;
			break;
		case FunctionChar:
			strVKCode.Format(_T("%d"), m_vkCode);
			ar << strVKCode;
			sLparam.Format(_T("%d"), m_ks.lparam);
			ar << sLparam;
			break;
		}
		sMilisecond.Format(_T("%d"), m_milisecond);
		ar << sMilisecond;
	}
	else
	{
		//ar >> sHwnd >> m_caption >> m_className >> m_calssStyle >> sEnum >> m_strToEnter;
		ar >> sEnum;
		//m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sHwnd); //(sHwnd, (ULONG)m_hWnd);
		m_eKindOfChar = CFunctionHelper::StringToEnumKindOfChar(sEnum);// (m_eKindOfWindowAction);
		switch(m_eKindOfChar)
		{
		case NormalChar:
			ar >> strAcscii;
			m_asciiCode = _ttoi(strAcscii);
			break;
		case FunctionChar:
			ar >> strVKCode;
			_stscanf(strVKCode, _T("%d"), &m_vkCode);
			ar >> sLparam;
			//m_asciiCode = _ttoi(strAcscii);
			_stscanf(sLparam, _T("%d"), &m_ks.lparam );
			break;
		}
		ar >> sMilisecond;
		_stscanf(sMilisecond, _T("%d"), &m_milisecond);
	}
}
