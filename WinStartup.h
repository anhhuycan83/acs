#if !defined(AFX_WINSTARTUP_H__B9241900_9A84_4F0F_8A8E_CB5662464002__INCLUDED_)
#define AFX_WINSTARTUP_H__B9241900_9A84_4F0F_8A8E_CB5662464002__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WinStartup.h : header file
//

/////////////////////////////////////////////////////////////////////////////
enum StartupUser { CurrentUser, AllUsers };

// CWinStartup window

class CWinStartup
{
// Construction
public:
	CWinStartup();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWinStartup)
	//}}AFX_VIRTUAL

// Implementation
public:
	static bool isAppAdded(HINSTANCE hInst, LPCTSTR lpszName, StartupUser user);
	static bool RemoveApp(LPCTSTR lpszName, StartupUser user);
	static bool AddApp(LPCTSTR lpszName, LPCTSTR lpszPath, StartupUser user);
	static bool AddApp(HINSTANCE hInst, LPCTSTR lpszName, StartupUser user);
	virtual ~CWinStartup();

	// Generated message map functions
protected:
	static LPCTSTR GetCurrentAppPath(HINSTANCE hInst, LPTSTR lpszPath, int nSize);
	//{{AFX_MSG(CWinStartup)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	//DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINSTARTUP_H__B9241900_9A84_4F0F_8A8E_CB5662464002__INCLUDED_)
