#if !defined(AFX_OPTIONTAB_H__3A944DF9_7DD2_403F_B00D_E5A829902E67__INCLUDED_)
#define AFX_OPTIONTAB_H__3A944DF9_7DD2_403F_B00D_E5A829902E67__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionTab dialog

class COptionTab : public CDialog
{
// Construction
public:
	void setConfigValue(bool ins_getSet);
	void createConfigFile();
	CString GetFileStorePath();
	void SetFileStorePath(CString ins_fullPath);
	void SetTopMost( HWND hWnd, const BOOL TopMost);

	COptionTab(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionTab)
	enum { IDD = IDD_DIALOG2 };
	CButton	m_rbF11Ctrl;
	CButton	m_rbF10Ctrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionTab)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionTab)
	afx_msg void OnbtnDefault();
	afx_msg void OnbtnSave();
	afx_msg void OnchbStartup();
	afx_msg void OnrbF10();
	afx_msg void OnrbF11();
	afx_msg void OnchbTopMost();
	afx_msg void OnchbShowAtStart();
	virtual BOOL OnInitDialog();
	afx_msg void OnchbRepeat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONTAB_H__3A944DF9_7DD2_403F_B00D_E5A829902E67__INCLUDED_)
