// MouseWindowAction.h: interface for the CMouseWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MOUSEWINDOWACTION_H__0B89BD4A_DE14_448E_9315_4ABC0CAC93EA__INCLUDED_)
#define AFX_MOUSEWINDOWACTION_H__0B89BD4A_DE14_448E_9315_4ABC0CAC93EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WindowAction.h"


class CMouseWindowAction  : public CWindowAction 
{
public:
	DECLARE_SERIAL(CMouseWindowAction);
	CMouseWindowAction();
	virtual ~CMouseWindowAction();
public:
	virtual void Serialize(CArchive &ar);
	void StringToMouseClickArr(CString strInput);
	CString MouseClickArrToString();
	CTypedPtrArray<CObArray, CMouseClick*> m_listMouseClick;

};

#endif // !defined(AFX_MOUSEWINDOWACTION_H__0B89BD4A_DE14_448E_9315_4ABC0CAC93EA__INCLUDED_)
