// MouseWindowAction.cpp: implementation of the CMouseWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "MouseWindowAction.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CMouseWindowAction, CObject, 1);//for implement virtual serialize

CMouseWindowAction::CMouseWindowAction()
{
	m_eKindOfWindowAction = MouseWindowAction;
//	m_parentWA = new CWindowAction();
//	m_parentWA->m_hWnd = NULL;
//	m_parentWA->m_caption.Empty();
//	m_parentWA->m_className.em();
//	m_parentWA->m_calssStyle = 0;
}

CMouseWindowAction::~CMouseWindowAction()
{	
	int k = m_listMouseClick.GetSize();
	while(k--)
		delete m_listMouseClick.GetAt(k);
	m_listMouseClick.RemoveAll();//remove all poiters currently store by m_list
}

CString CMouseWindowAction::MouseClickArrToString()
{
	CString sRs;
	CMouseClick* mItem;
	for(int i = 0; i<m_listMouseClick.GetSize(); i++)
	{
		mItem = m_listMouseClick.GetAt(i);
		//sRs = CFunctionHelper::EnumToStringKindOfClick(mItem->m_eKindOfClick) + ";";
		sRs.Format(_T("%s;%d;%d"),
			CFunctionHelper::EnumToStringKindOfClick(mItem->m_eKindOfClick),
			mItem->m_poiterClick.x, mItem->m_poiterClick.y);

		if(i != m_listMouseClick.GetUpperBound())
		{
			sRs += _T("@");
		}		
	}
	return sRs;
}

void CMouseWindowAction::StringToMouseClickArr(CString strInput)
{	
	CFunctionHelper fnHelper;	
	CMouseClick* mItem;
	CStringArray strArrMouse;
	CStringArray strArrMouseItem;

	fnHelper.StringSplit(strInput, strArrMouse, _T('@'));
	//CString mcItem;
	for(int i = 0; i<strArrMouse.GetSize(); i++)
	{
		fnHelper.StringSplit(strArrMouse.GetAt(i), strArrMouseItem, _T(';'));
		if(strArrMouseItem.GetSize() == 3)
		{
			mItem = new CMouseClick();
			mItem->m_eKindOfClick = CFunctionHelper::StringToEnumKindOfClick(strArrMouseItem.GetAt(0));
			mItem->m_poiterClick.x = _ttoi(strArrMouseItem.GetAt(1));
			mItem->m_poiterClick.y = _ttoi(strArrMouseItem.GetAt(2));
			m_listMouseClick.Add(mItem);
		}		
	}	
}

void CMouseWindowAction::Serialize(CArchive &ar)
{
	CString sHwnd,sEnum, sParentHwnd, sMilisecond;
	if(ar.IsStoring())
	{			
		//s.Format(_T("%d"), m_eKindOfWindowAction);			
		//s2.Format(_T("0x%08X"), m_hWnd);
		sHwnd.Format(_T("%d"), m_hWnd);
		//CFunctionHelper::convert_long_to_hex(sHwnd, (ULONG)m_hWnd);
		sEnum = CFunctionHelper::EnumToStringKindOfWindowAction(m_eKindOfWindowAction);
		//ar << m_hWnd << m_caption << m_className << m_calssStyle;// << m_eKindOfWindowAction << m_strToEnter;		
		//ar << sHwnd << m_caption << m_className << m_calssStyle << sEnum << MouseClickArrToString();		
		sMilisecond.Format(_T("%d"), m_milisecond);
		ar << sHwnd << m_caption << m_className << m_calssStyle << sEnum << sMilisecond;
		//save parent infor
		sParentHwnd.Format(_T("%d"), m_parentWI.m_hWnd);
		//CFunctionHelper::convert_long_to_hex(sParentHwnd, (ULONG)m_parentWI.m_hWnd);		
		ar << sParentHwnd << m_parentWI.m_caption << m_parentWI.m_className << m_parentWI.m_calssStyle;
		m_listMouseClick.Serialize(ar);
	}
	else
	{
		//CString strMouseClickArr;
		//ar >> sHwnd >> m_caption >> m_className >> m_calssStyle >> sEnum >> strMouseClickArr;
		ar >> sHwnd >> m_caption >> m_className >> m_calssStyle >> sEnum >> sMilisecond;
		_stscanf(sHwnd, _T("%d"), &m_hWnd);
		_stscanf(sMilisecond, _T("%d"), &m_milisecond);
		//m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sHwnd); //(sHwnd, (ULONG)m_hWnd);
		//get parent infor		
		ar >> sParentHwnd >> m_parentWI.m_caption >> m_parentWI.m_className >> m_parentWI.m_calssStyle;
		_stscanf(sParentHwnd, _T("%d"), &m_parentWI.m_hWnd);
		//m_parentWI.m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sParentHwnd);
		m_eKindOfWindowAction = CFunctionHelper::StringToEnumKindOfWindowAction(sEnum);// (m_eKindOfWindowAction);
		//convert stringarr to list mouse click
		//StringToMouseClickArr(strMouseClickArr);
		m_listMouseClick.Serialize(ar);
		//m_eKindOfWindowAction = _ttoi(s);
	}
}
