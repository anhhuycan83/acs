// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
#define _WIN32_WINNT 0x0500 //for mouse event WH_MOUSE_LL
#define WINVER 0x0500

#define MOUSEEVENTF_VIRTUALDESK 0x4000 //dont understand

#if !defined(AFX_STDAFX_H__065C586C_529B_4403_A302_F96661F7AE7B__INCLUDED_)
#define AFX_STDAFX_H__065C586C_529B_4403_A302_F96661F7AE7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions

//#include "FunctionHelper.h" //for all my functions helper
#include <afxtempl.h>	//for support CTypedPtrArray




//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

//below for mutext
//my define begin
//#define _WIN32_WINNT 0x0500
//#define MOUSEEVENTF_VIRTUALDESK 0x4000
#define ONCE_INSTANCE _T("ACSHpGroup")
#define MYWM_NOTIFYICON (WM_USER + 2)
#define MYWM_KEYPRESS_F10 (WM_USER + 3) //F10
#define MYWM_KEYPRESS_F11 (WM_USER + 4) //F11
#define MYWM_CHECKTIME (WM_USER + 5) //start new my thread
#define MYVERSION _T("1.0.0.3")
//below is my event message to update GUI sent by other thread
#define MYWM_SCRIPT_FINISHED (WM_USER + 6)
#define MYWM_SCRIPT_RUNNING_INDEX (WM_USER + 7)
#define MYWM_SCRIPT_REPEAT (WM_USER + 8)
#define MYWM_TIME_REMAIN (WM_USER + 9)
//MYDEL#define MYWM_NOTIFYICON (WM_USER + 8)
//MYDEL#define MYWM_NOTIFYICON (WM_USER + 9)
//MYDEL#define MYWM_NOTIFYICON (WM_USER + 10)
//MYDEL#define MYWM_NOTIFYICON (WM_USER + 11)
//my define end




#endif // !defined(AFX_STDAFX_H__065C586C_529B_4403_A302_F96661F7AE7B__INCLUDED_)

