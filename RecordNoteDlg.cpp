// RecordNoteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "acs.h"
#include "ACSView.h"
#include "RecordNoteDlg.h"
#include "DateTimeFormat.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecordNoteDlg dialog


CRecordNoteDlg::CRecordNoteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecordNoteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecordNoteDlg)
	m_txtMsgValue = _T("");
	m_chbScheduleEnable = FALSE;
	m_dtpSchedule = COleDateTime::GetCurrentTime();
	//}}AFX_DATA_INIT
	m_bIsChanged = FALSE;
	m_pDoc = NULL;
}


void CRecordNoteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecordNoteDlg)
	DDX_Control(pDX, dtpSchedule, m_dtpScheduleCtrl);
	DDX_Control(pDX, btnScheduleSave, m_btnScheduleSave);
	DDX_Control(pDX, btnScheduleClose, m_btnScheduleClose);
	DDX_Text(pDX, txtMsg, m_txtMsgValue);
	DDX_Control(pDX, rbOnce, m_rbOnceCtrl);
	DDX_Control(pDX, rbDaily, m_rbDailyCtrl);
	DDX_Control(pDX, rbWeekly, m_rbWeeklyCtrl);
	DDX_Control(pDX, rbMonthly, m_rbMonthlyCtrl);	
	DDX_Check(pDX, chbSchedueEnable, m_chbScheduleEnable);
	DDX_DateTimeCtrl(pDX, dtpSchedule, m_dtpSchedule);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecordNoteDlg, CDialog)
	//{{AFX_MSG_MAP(CRecordNoteDlg)
	ON_BN_CLICKED(btnScheduleSave, OnbtnScheduleSave)
	ON_BN_CLICKED(btnScheduleClose, OnbtnScheduleClose)
	ON_BN_CLICKED(chbSchedueEnable, OnchbSchedueEnable)
	ON_EN_CHANGE(txtMsg, OnChangetxtMsg)
	ON_NOTIFY(DTN_DATETIMECHANGE, dtpSchedule, OnDatetimechangedtpSchedule)
	ON_BN_CLICKED(rbScheduleOnce, OnrbScheduleOnce)
	ON_BN_CLICKED(rbScheduleDaily, OnrbScheduleDaily)
	ON_BN_CLICKED(rbScheduleMonthly, OnrbScheduleMonthly)
	ON_BN_CLICKED(rbScheduleWeekly, OnrbScheduleWeekly)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecordNoteDlg message handlers

BOOL CRecordNoteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	//set font to main dialog font
	//CDailyNoteDlg* parentWnd = ((CDailyNoteDlg*)GetParent());
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	SendMessageToDescendants(WM_SETFONT,
		(WPARAM)pView->GetFont()->m_hObject,
		MAKELONG(FALSE, 0), 
		FALSE);
	//set font of CEdit textbox
	//setRtbFont(_T("Tahoma"));

	//set format of dtpDateTime
	DateTime_SetFormat(m_dtpScheduleCtrl, _T("dd'/'MM'/'yyyy' 'hh':'mm':'ss' 'tt"));

	//get values init
	//m_isNew = true;
	getValues();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRecordNoteDlg::getValues()
{
	CACSApp* pApp = (CACSApp*) AfxGetApp();
	
	m_chbScheduleEnable = pApp->m_xmlSetting.GetSettingLong(_T("SCHEDULE"), _T("ENABLE"), 0);
	//CheckDlgButton(chbScheduleEnable, );
	SetEnableControls(m_chbScheduleEnable);
	m_txtMsgValue = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("MESSAGE"), _T(""));
	//MYDEL   		double dtData;
	CString sDt = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("DATETIME"), _T(""));
	if(sDt.GetLength() == 0)
		sDt.Format(_T("%f"), COleDateTime::GetCurrentTime().m_dt);
	
	m_dtpSchedule.m_dt = CFunctionHelper::ConvertCStringToDouble(sDt);
	TRACE(_T("s: %s, dtp: %f\n"), sDt, m_dtpSchedule.m_dt);
	UpdateData(false);
	//MYDEL   		COleDateTime dt = dtData;
	//MYDEL   		m_dtpSchedule.m_dt = dtData;
	//MYDEL   		dt.GetAsSystemTime(m_editSysTime);
	//MYDEL   		DateTime_SetSystemtime(m_dtpDateTime.m_hWnd,GDT_VALID, &m_editSysTime);
	CString sFre = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("FREQUENCY"), _T("ONCE"));
	
	SetFreRb(GetFrequencyEnum(sFre));
	UpdateData(true);

//MYDEL   	if(pApp->m_xmlSetting.GetSettingLong(_T("SCHEDULE"), _T("ENABLE"), 0))
//MYDEL   	{
//MYDEL   		//if user scheduled => load from config file to GUI
//MYDEL   		CheckDlgButton(chbSchedueEnable, TRUE);
//MYDEL   		SetEnableControls(TRUE);
//MYDEL   		m_txtMsgValue = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("MESSAGE"), _T(""));
//MYDEL   		//MYDEL   		double dtData;
//MYDEL   		CString sDt = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("DATETIME"), _T(""));
//MYDEL   		if(sDt.GetLength() == 0)
//MYDEL   			sDt.Format(_T("%f"), COleDateTime::GetCurrentTime().m_dt);
//MYDEL   		m_dtpSchedule.m_dt = CFunctionHelper::ConvertCStringToDouble(sDt);
//MYDEL   		//MYDEL   		COleDateTime dt = dtData;
//MYDEL   		//MYDEL   		m_dtpSchedule.m_dt = dtData;
//MYDEL   		//MYDEL   		dt.GetAsSystemTime(m_editSysTime);
//MYDEL   		//MYDEL   		DateTime_SetSystemtime(m_dtpDateTime.m_hWnd,GDT_VALID, &m_editSysTime);
//MYDEL   		CString sFre = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("FREQUENCY"), _T("ONCE"));
//MYDEL   		SetFreRb(GetFrequencyEnum(sFre));
//MYDEL   		UpdateData(true);
//MYDEL   	}
//MYDEL   	else
//MYDEL   	{
//MYDEL   		CheckDlgButton(chbSchedueEnable, FALSE);
//MYDEL   		SetEnableControls(FALSE);
//MYDEL   		//if user have not saved yet => load default
//MYDEL   		m_txtMsgValue = _T("");
//MYDEL   		//m_dtpDateTime
//MYDEL   		SetFreRb(ONCE);
//MYDEL   		//m_rbOnceCtrl.SetCheck(true);
//MYDEL   		UpdateData(false);
//MYDEL   	}
}

//DEL void CRecordNoteDlg::OnOK() 
//DEL {
//DEL 	// TODO: Add extra validation here
//DEL 	UpdateData(true);
//DEL 	if(m_txtMsgValue.IsEmpty())
//DEL 	{
//DEL 		MessageBox(_T("You must enter value in the textbox!"));	
//DEL 		return;
//DEL 	}
//DEL 	
//DEL //MYDEL	CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
//DEL //MYDEL
//DEL //MYDEL	TCHAR temp[5];
//DEL //MYDEL	CString strAdd;
//DEL //MYDEL	CDateTimeFormat dtFormat;
//DEL //MYDEL	SYSTEMTIME systime;	
//DEL //MYDEL	DateTime_GetSystemtime(m_dtpDateTime, &systime);	
//DEL //MYDEL	COleDateTime oleDateTime;
//DEL //MYDEL	oleDateTime = systime;
//DEL //MYDEL	dtFormat.SetDateTime(oleDateTime);
//DEL //MYDEL
//DEL //MYDEL	dtFormat.SetFormat(_T("dd MMMM yyyy hh:mm tt"));
//DEL //MYDEL	CString strFre = GetFrequencyString(GetSelectedFrequencyRb());
//DEL //MYDEL
//DEL //MYDEL	strAdd = dtFormat.GetString(); 
//DEL //MYDEL	strAdd += _T(";") + strFre;
//DEL //MYDEL	strAdd += _T(";") + m_txtMsgValue;
//DEL //MYDEL
//DEL //MYDEL	CMAINTAB *pDlgMain = (CMAINTAB *)(pMainDlg->m_tbCtrl.m_Dialog[0]);
//DEL //MYDEL
//DEL //MYDEL	if(m_isNew)//add new record
//DEL //MYDEL	{
//DEL //MYDEL		
//DEL //MYDEL		//pDlgMain->m_wndList.ins
//DEL //MYDEL		const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(IDX, 1, dtFormat.GetString());
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(IDX, 2, strFre);
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(IDX, 3, m_txtMsgValue);
//DEL //MYDEL		
//DEL //MYDEL		//MessageBox(strAdd);
//DEL //MYDEL
//DEL //MYDEL		CDailyNoteDlg* pMainFrom = (CDailyNoteDlg*) AfxGetMainWnd();
//DEL //MYDEL		pMainFrom->MyAddItemDLL(strAdd);
//DEL //MYDEL
//DEL //MYDEL		CRecordNote* pNewRectNote = new CRecordNote(strAdd);
//DEL //MYDEL
//DEL //MYDEL		pMainFrom->m_RecordNoteArray.Add(pNewRectNote);
//DEL //MYDEL		//increase the index of selected current record
//DEL //MYDEL		//pMainFrom->m_currentIndexWaitingToShow--;
//DEL //MYDEL	}
//DEL //MYDEL	else
//DEL //MYDEL	{
//DEL //MYDEL		//edit record		
//DEL //MYDEL		//const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//DEL //MYDEL		//pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 1, dtFormat.GetString());
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 2, strFre);
//DEL //MYDEL		pDlgMain->m_wndList.SetItemText(m_indexItemUpdating, 3, m_txtMsgValue);
//DEL //MYDEL
//DEL //MYDEL		//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,1);
//DEL //MYDEL		//m_wndList.DeleteItem(nItem);
//DEL //MYDEL		//MyRefreshSTT();
//DEL //MYDEL
//DEL //MYDEL		//delete item in list record
//DEL //MYDEL		
//DEL //MYDEL
//DEL //MYDEL		pMainDlg->m_RecordNoteArray.GetAt(m_indexItemUpdating)->m_line = strAdd;
//DEL //MYDEL
//DEL //MYDEL		CDailyNoteDlg* pMainDlg = (CDailyNoteDlg*) AfxGetApp()->m_pMainWnd;
//DEL //MYDEL		pMainDlg->MyDeleteItemDLL(-1);
//DEL //MYDEL	}
//DEL 
//DEL 	CDialog::OnOK();
//DEL }

CString CRecordNoteDlg::GetFrequencyString(eFrequency eFre)
{
	CString sRs = "";
	switch(eFre)
	{
	case ONCE:
		sRs = "ONCE";
		break;
	case DAILY:
		sRs = "DAILY";
		break;
	case WEEKLY:
		sRs = "WEEKLY";
		break;
	case MONTHLY:
		sRs = "MONTHLY";
		break;
	default:
		sRs = "ONCE";
		break;
	}
	return sRs;
}



void CRecordNoteDlg::SetFreRb(eFrequency eFre)
{
	switch(eFre)
	{
	case ONCE:
		m_rbOnceCtrl.SetCheck(true);
		break;
	case DAILY:
		m_rbDailyCtrl.SetCheck(true);
		break;
	case WEEKLY:
		m_rbWeeklyCtrl.SetCheck(true);		
		break;
	case MONTHLY:
		m_rbMonthlyCtrl.SetCheck(true);		
		break;
	default:
		m_rbOnceCtrl.SetCheck(true);		
		break;
	}
}

eFrequency CRecordNoteDlg::GetFrequencyEnum(CString strFre)
{
	eFrequency eRs = ONCE;
	//CString sCase("AAAA");
	STR_SWITCH(strFre)      //Start of switch
	{                        //Opening and closing braces
		//NOT MANDATORY for switch.
		STR_CASE(_T("ONCE"))
		{                      //MANDATORY for case.
			eRs = ONCE;
			break;               //break has to in braces of case
		}  
		STR_CASE(_T("DAILY"))
		{                      //MANDATORY for case.
			eRs = DAILY;
			break;               //break has to in braces of case
		}                      //Opening and closing braces
		//MANDATORY for case.
		
		STR_CASE(_T("WEEKLY"))
		{
			eRs = WEEKLY;
			break;
		}
		STR_CASE(_T("MONTHLY"))
		{
			eRs = MONTHLY;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = ONCE;//Default handling if any
			break;
		}
	}                        //Opening and closing braces
	//NOT MANDATORY for switch
	STR_SWITCH_END()         //MANDATORY statement
		

	return eRs;
}

eFrequency CRecordNoteDlg::GetSelectedFrequencyRb()
{
	eFrequency eRs = ONCE;

	if( BST_CHECKED == m_rbOnceCtrl.GetCheck())
		eRs = ONCE;
	else
		if(m_rbDailyCtrl.GetCheck() == BST_CHECKED)
			eRs = DAILY;
		else
			if(m_rbWeeklyCtrl.GetCheck() == BST_CHECKED)
				eRs = WEEKLY;
			else
				if(m_rbMonthlyCtrl.GetCheck() == BST_CHECKED)
					eRs = MONTHLY;
				else
					eRs = DAILY;
	return eRs;
	//return DAILY;
			
}

void CRecordNoteDlg::setRtbFont(CString ins_strFont)
{
	CDC *pDC = GetDC();

	// create UNICODE font
	LOGFONT lf;
	
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight =
	MulDiv(25, ::GetDeviceCaps(pDC->m_hDC,
		 LOGPIXELSY), 172);
	lf.lfWeight = FW_NORMAL;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	wcscpy(lf.lfFaceName, ins_strFont);
	if(NULL == (HFONT) m_font)
		m_font.CreateFontIndirect(&lf);

	// apply the font to the controls
	//m_list.SetFont(&m_font);
	//m_edit.SetFont(&m_font);
	CRichEditCtrl* pTxtToSet;
	pTxtToSet = (CRichEditCtrl*) GetDlgItem(txtMsg);
	pTxtToSet->SetFont(&m_font);
	// release the device context.
	ReleaseDC(pDC);
}



//DEL void CRecordNoteDlg::OnDestroy()
//DEL {
//DEL 	if(m_bIsChanged)
//DEL 	{
//DEL 		if(AfxMessageBox(_T("Do you want to save?"), MB_YESNO) == IDYES)
//DEL 			OnbtnScheduleSave();
//DEL 	}
//DEL 
//DEL 	CDialog::OnDestroy();	
//DEL 	// TODO: Add your message handler code here	
//DEL 
//DEL 	//When you no longer need the font, call the DeleteObject function to delete the font.
//DEL 	//especially if you have a sub dialog
//DEL 	m_font.DeleteObject();
//DEL }

//DEL int CRecordNoteDlg::DoModal() 
//DEL {
//DEL 	// TODO: Add your specialized code here and/or call the base class
//DEL 	CDialogTemplate dlt;
//DEL 	int nResult;
//DEL 	// load dialog template
//DEL 	if (!dlt.Load(MAKEINTRESOURCE(CRecordNoteDlg::IDD))) return -1;
//DEL 	// set your own font, for example �Arial�, 10 pts.
//DEL 	dlt.SetFont(_T("Arial"), 10);
//DEL 	// get pointer to the modified dialog template
//DEL 	LPSTR pdata = (LPSTR)GlobalLock(dlt.m_hTemplate);
//DEL 	// let MFC know that you are using your own template
//DEL 	m_lpszTemplateName = NULL;
//DEL 	InitModalIndirect(pdata);
//DEL 	// display dialog box
//DEL 	nResult = CDialog::DoModal();
//DEL 	// unlock memory object
//DEL 	GlobalUnlock(dlt.m_hTemplate);
//DEL 	return nResult;
//DEL 	//	return CDialog::DoModal();
//DEL }

void CRecordNoteDlg::SetEnableControls(BOOL bIsEnable)
{
	//(txtMsg, bIsEnable);
	GetDlgItem(txtMsg)->EnableWindow(bIsEnable);
	m_dtpScheduleCtrl.EnableWindow(bIsEnable);
	m_rbOnceCtrl.EnableWindow(bIsEnable);
	m_rbDailyCtrl.EnableWindow(bIsEnable);
	m_rbWeeklyCtrl.EnableWindow(bIsEnable);
	m_rbMonthlyCtrl.EnableWindow(bIsEnable);
//MYDEL   	m_btnScheduleSave.EnableWindow(bIsEnable);
}

void CRecordNoteDlg::OnbtnScheduleSave() 
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	CACSApp* pApp = (CACSApp*) AfxGetApp();
	//pApp->m_xmlSetting.SetSettingLong
	BOOL bIsEnable = IsDlgButtonChecked(chbSchedueEnable);
	pApp->m_xmlSetting.SetSettingLong(_T("SCHEDULE"), _T("ENABLE"), (int)bIsEnable);
	pApp->m_xmlSetting.SetSettingString(_T("SCHEDULE"), _T("MESSAGE"), m_txtMsgValue);
	
//MYDEL   	COleDateTime dt = CFunctionHelper::GetOleDateTime(&m_dtpDateTime);
	CString strTimeSpane;
	strTimeSpane.Format(_T("%f"), m_dtpSchedule.m_dt);
	pApp->m_xmlSetting.SetSettingString(_T("SCHEDULE"), _T("DATETIME"), strTimeSpane);
	eFrequency eFre = GetSelectedFrequencyRb();
	pApp->m_xmlSetting.SetSettingString(_T("SCHEDULE"), _T("FREQUENCY"), GetFrequencyString(eFre));
	m_bIsChanged = FALSE;
//MYDEL   	//now reset thread checking time because of user changed setting
//MYDEL   	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
//MYDEL   	pView->ReSetThreadCheckingTime();
}

void CRecordNoteDlg::OnbtnScheduleClose() 
{
	// TODO: Add your control notification handler code here
	if(m_bIsChanged)
	{
		if(AfxMessageBox(_T("Do you want to save?"), MB_YESNO) == IDYES)
			OnbtnScheduleSave();
	}
	this->EndDialog(IDOK);
}

void CRecordNoteDlg::OnchbSchedueEnable() 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
	SetEnableControls(IsDlgButtonChecked(chbSchedueEnable));
}

void CRecordNoteDlg::OnChangetxtMsg() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
}

void CRecordNoteDlg::OnDatetimechangedtpSchedule(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
	*pResult = 0;
}

void CRecordNoteDlg::OnrbScheduleOnce() 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
}

void CRecordNoteDlg::OnrbScheduleDaily() 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
}

void CRecordNoteDlg::OnrbScheduleMonthly() 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
}

void CRecordNoteDlg::OnrbScheduleWeekly() 
{
	// TODO: Add your control notification handler code here
	m_bIsChanged = TRUE;
}

void CRecordNoteDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	//When you no longer need the font, call the DeleteObject function to delete the font.
	//especially if you have a sub dialog
	m_font.DeleteObject();	
}


