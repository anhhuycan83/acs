////////////////////////////////////////////////////////////////////////////
//
// WndPosManager.h
// 
// Copyright 2005 Xia Xiongjun( ���۾� ), All Rights Reserved.
//
// E-mail: ShongTu@Gmail.com
//
// This source file may be copyed, modified, redistributed  by any means
// PROVIDING that this notice and the author's name and all copyright 
// notices remain intact, and PROVIDING it is NOT sold for profit without 
// the authors expressed written consent. The author accepts no liability 
// for any damage/loss of business that this product may cause.
//
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
// 
// The following constants are used to define the way how the window is 
// dispalyed at next startup. 
//
// 
// WPR_RECOMMEND	:	Restore in the recommended way
//						[Nor->Nor; Max->Max; (Max, Min)->Max; (Nor, Min)->Nor]
//
// WPR_DIRECT		:   Restore to the exact state when exited the last time 
//						[Nor->Nor; Max->Max; Min->Min]
//
// WPR_NORMAL		:	Restore to normal state 
//						[Any->Nor]
//
// WPR_MAXIMIZE		:	Restore to maximized state
//						[Any->Max]
//
// WPR_MINIMIZE		:	Restore to maximized state
//						[Any->Min]
//
////////////////////////////////////////////////////////////////////////////

#pragma once

#define WPR_RECOMMEND	0
#define WPR_DIRECT		1
#define WPR_NORMAL		2
#define WPR_MAXIMIZE	3
#define WPR_MINIMIZE	4

////////////////////////////////////////////////////////////////////////////
// class CWndPosManager

class CWndPosManager
{
// XR: construction/destruction
public:
	CWndPosManager(void);
	~CWndPosManager(void);
	
// XR: Operations
public:
	// XR: Save the window position information
	BOOL SaveWndPos(HWND hWnd, LPCTSTR lpszEntry, 
		LPCTSTR lpszSection = _T("Position")) const;

	// XR: Restore the window according to the saved window position information
	BOOL RestoreWndPos(HWND hWnd, LPCTSTR lpszEntry, 
		LPCTSTR lpszSection = _T("Position"));

	// XR: Restore the window to the default window position
	BOOL RestoreDefWndPos(HWND hWnd, 
		LPCTSTR lpszSection = _T("Position"));

// XR: Attributes
public:
	// XR: Get the restore option
	int  GetResOption() const;

	// XR: Set the restore option
	void SetResOption(int nResOption);

// XR: Implementation
private:
	// XR: Reassign the showCmd according to the specified restore option
	void AdjustShowCmd(UINT& showCmd, const UINT flags) const;

	// XR: Center the window placement
	void CenterWndPos(WINDOWPLACEMENT &wp);

// XR: Data
private:
	int m_nResOption;
	WINDOWPLACEMENT m_wp;
	WINDOWPLACEMENT m_wpDef;

	BOOL m_bFirstRes;
};


////////////////////////////////////////////////////////////////////////////
// SDI support: class CFrameWndEx 

class CFrameWndEx : public CFrameWnd
{
	DECLARE_DYNCREATE(CFrameWndEx)

// XR: construction/destruction
protected:
	CFrameWndEx(LPCTSTR szEntry = _T("MainWnd"));
	virtual ~CFrameWndEx();

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()

// XR: Data:
protected:
	CWndPosManager m_wpMgr;

private:
	BOOL	m_bEnableRes;
	CString m_strEntry;
};


////////////////////////////////////////////////////////////////////////////
// MDI support: class CMDIFrameWndEx 

class CMDIFrameWndEx : public CMDIFrameWnd
{
	DECLARE_DYNCREATE(CMDIFrameWndEx)

// XR: construction/destruction
protected:
	CMDIFrameWndEx(LPCTSTR szEntry = _T("MainWnd"));
	virtual ~CMDIFrameWndEx();

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()

// XR: Data:
protected:
	CWndPosManager m_wpMgr;

private:
	BOOL	m_bEnableRes;
	CString m_strEntry;
};


////////////////////////////////////////////////////////////////////////////
// Dialog support: class CDialogEx 

class CDialogEx : public CDialog
{
// XR: construction/destruction
public:
	explicit CDialogEx(UINT nIDTemplate, CWnd* pParentWnd = NULL, 
		LPCTSTR szEntry = _T("Dlg"));
	virtual ~CDialogEx();

// Generated message map functions
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()

// XR: Data:
protected:
	CWndPosManager m_wpMgr;

private:
	CString m_strEntry;
};


