// ACS.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ACS.h"

#include "MainFrm.h"
#include "ACSDoc.h"
#include "BsDocTemplate.h"
#include "ACSView.h"
#include "diskid32.h"
#include "FileVersionInfo.h"




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CACSApp

BEGIN_MESSAGE_MAP(CACSApp, CWinApp)
	//{{AFX_MSG_MAP(CACSApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_HELP_INDEX, OnHelpIndex)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

//define for extern variable
BOOL m_bIsRegistered;
int m_maxNumRunAction;
BOOL m_bIsRepeat;
BOOL m_isHaveCurrentTimeCheking;
COleDateTime m_timeChecking;

/////////////////////////////////////////////////////////////////////////////
// CACSApp construction

CACSApp::CACSApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_maxNumRunAction = 10;
	m_bIsRegistered = FALSE;
	m_bIsRepeat = FALSE;	
}

CACSApp::~CACSApp()
{
	
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CACSApp object

CACSApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {8908D8FD-66B0-40A0-A95A-5450E9D073A5}
static const CLSID clsid =
{ 0x8908d8fd, 0x66b0, 0x40a0, { 0xa9, 0x5a, 0x54, 0x50, 0xe9, 0xd0, 0x73, 0xa5 } };

/////////////////////////////////////////////////////////////////////////////
// CACSApp initialization

BOOL CACSApp::InitInstance()
{

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	//my check if have only one instance begin
	// Create mutex
	//CWnd *pWndFrame;
	//CWnd *pWndChild;
	//WNDCLASS wndcls;

	m_hMutex = ::CreateMutex(NULL, TRUE, ONCE_INSTANCE);

	switch(::GetLastError())
	{
		case ERROR_SUCCESS:
		// Mutex created successfully. There is no instance running						
			break;

		case ERROR_ALREADY_EXISTS:
		// Mutex already exists so there is a running instance of our app.						
			AfxMessageBox(_T("This soft is already running"));
			return FALSE;

		default:
		// Failed to create mutex by unknown reason
			return FALSE;
	}
	
	//my end

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("ACS - Automatic Controller System"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	//CSingleDocTemplate* pDocTemplate;	
	BsDocTemplate* pDocTemplate;
	pDocTemplate = new BsDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CACSDoc),		
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CACSView));
	AddDocTemplate(pDocTemplate);

	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, pDocTemplate, TRUE);
		// Note: SDI applications register server objects only if /Embedding
		//   or /Automation is present on the command line.

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Check to see if launched as OLE server
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();

		// Application was run with /Embedding or /Automation.  Don't show the
		//  main window in this case.
		return TRUE;
	}

	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();

	//create config file befor cview constructor	
	m_xmlSetting.SetSettingsFile();

	//function below call cview constructor
	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	
	// The one and only window has been initialized, so show and update it.
	//CFrameWnd * pFrame = (CFrameWnd *)(AfxGetApp()->m_pMainWnd);
    //CACSView * pView = (CACSView *)pFrame->GetActiveView();	
	CACSView * pView = (CACSView *)CFunctionHelper::GetActiveView();
	
	//CACSView* pView = (CACSView*)m_pMainWnd->GetActiveWindow();
	//call init cview because it was not called auto when you implement your custom cview class
	//to fix lagging when starting main app 
	pView->OnInitialUpdate();
	if(pView->m_visible)
	{
		m_pMainWnd->ShowWindow(SW_SHOW);
		m_pMainWnd->UpdateWindow();
		pView->ResizeFrameToFit();
		
		//check only one on init app whether the app is ontop or not
		if(m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("TOPMOST"), false))
		{
			pView->GetOptionTab()->SetTopMost(AfxGetMainWnd()->m_hWnd, true);
		}
	}
	else
	{		
		pView->myShowDlg(false);
		//m_pMainWnd->ShowWindow(SW_HIDE);
	}
	
	//BsDocTemplate* pDoc = (BsDocTemplate*) pView->GetDocument();

	CheckLicense(_T(""));
	//show warning message to client about trial mode if this is in
	if(!m_bIsRegistered)
		AfxMessageBox(_T("This program is in trial mode.\nPlease register a license key to use full features and supported"));
	else
	{
		UINT currentNumDay = (UINT)COleDateTime::GetCurrentTime().m_dt;
		if(currentNumDay  < (UINT)m_startDate.m_dt
			|| currentNumDay > (UINT)m_endDate.m_dt)
			AfxMessageBox(_T("The license key was expired. Now you are in a trial mode.\nPlease register new license key to use full features and supported"));
	}
	//set m_bIsRepeat variable
	if(m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("REPEAT"), false))
		m_bIsRepeat = TRUE;
	//adding file version to the title
	
	CFileVersionInfo fvi;
	if(fvi.Create())
	{
		CString sCurrentTitle, sNewTitle, sVersion;
		//CString t;
		TRACE(_T("version: %s"), fvi.GetFileVersion());
		m_pMainWnd->GetWindowText(sCurrentTitle);
		sVersion.Format(_T("%d.%d.%d.%d")
			, fvi.GetFileVersion(3), fvi.GetFileVersion(2), fvi.GetFileVersion(1), fvi.GetFileVersion(0));
		sNewTitle.Format(_T("%s v%s"), sCurrentTitle, sVersion);
		m_pMainWnd->SetWindowText(sNewTitle);
		m_pMainWnd->UpdateData();
	}
	
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	void RefrestStatusRegister();
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_txtActiveCode;
	CString	m_txtEndDate;
	CString	m_txtStartDate;
	CString	m_txtCurrentLicense;
	CString	m_txtNewLicense;
	CString	m_lblRegisterMsg;
	CString	m_txtContact;
	CString	m_lblVersion;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnbtnRegister();
	afx_msg void OnClickBtnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_txtActiveCode = _T("");
	m_txtEndDate = _T("");
	m_txtStartDate = _T("");
	m_txtCurrentLicense = _T("");
	m_txtNewLicense = _T("");
	m_lblRegisterMsg = _T("");
	m_txtContact = _T("");
	m_lblVersion = _T("");	
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, txtActiveCode, m_txtActiveCode);
	DDX_Text(pDX, txtEndDate, m_txtEndDate);
	DDX_Text(pDX, txtStartDate, m_txtStartDate);
	DDX_Text(pDX, txtCurrentLicense, m_txtCurrentLicense);
	DDX_Text(pDX, txtNewLicense, m_txtNewLicense);
	DDX_Text(pDX, lblRegisterMsg, m_lblRegisterMsg);
	DDX_Text(pDX, txtContact, m_txtContact);
	DDX_Text(pDX, lblVersion, m_lblVersion);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(btnRegister, OnbtnRegister)
	ON_BN_CLICKED(IDC_OK, OnClickBtnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CACSApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CACSApp message handlers


BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	RefrestStatusRegister();
	
	//MYDEL   	CACSDoc* pDoc = (CACSDoc*) CFunctionHelper::GetActiveDocument();
	//MYDEL   	m_txtActiveCode = pDoc->m_activeCode;
	//MYDEL   	COleDateTime dtNow;
	//MYDEL   	dtNow = COleDateTime::GetCurrentTime();
	//MYDEL   
	//MYDEL   	m_txtSerial = CFunctionHelper::CreateLicenseToClient(m_txtActiveCode, dtNow,30);
	//MYDEL   	CString sTrace;
	//MYDEL   	sTrace.Format(_T("server generates serial: %s\n"), m_txtSerial);
	//MYDEL   	TRACE(sTrace);
	//MYDEL   	BOOL bIsLicenseCorrect;
	//MYDEL   	CFunctionHelper::CheckLicense(bIsLicenseCorrect, m_txtSerial);
	
	//MYDEL   	CString hddSerial;
	//MYDEL   	diskid32 m_disk;
	//MYDEL   	m_disk.GetHddSerial(hddSerial);
	//MYDEL   	m_txtSerial = hddSerial;
	//MYDEL   	AfxMessageBox(hddSerial);
	
	
	//MYDEL   	CString sLicenseClient;
	//MYDEL   	sTemp.Format(_T("%s%s%s"), sLicense);
	//MYDEL   	sTrace.Format(_T("server generates serial: %s\n"), m_txtSerial);
	//MYDEL   	TRACE(sTrace);
	//UpdateData(false);
	
	// TODO: Add extra initialization here
	//set text box contact:
//MYDEL	CString s;
//MYDEL	s = _T("M\x1ECDi th\x1EAF\x63 \x78in vui l\xF2ng li\xEAn h\x1EC7:");
//MYDEL	m_txtContact.Format(_T("\n%s\n%s\n%s\n%s")
//MYDEL		, s
//MYDEL		, _T("Yahoo: acs.hotro@y\x61hoo.\x63om")
//MYDEL		, _T("\x110t: ")
//MYDEL		, _T("Skyp\x65: acs.hotro"));
	//m_txtContact = s;
	m_txtContact.Format(_T("%-15s%-40s\r\n%-15s%-40s\r\n%-15s%-40s\r\n%-15s%-40s")
		, _T("Yahoo: "), _T("acs.hotro@yahoo.com")
		, _T("gmail: "), _T("acs.hotro@gmail.com")
		, _T("Phone: "), _T("0935157617")
		, _T("Skype: "), _T("acs.hotro@gmail.com"));

	//m_txtTempValue.Format(_T("%s\n%s"), _T("Phone: 0937xxx"), _T("Skype: 0937xxx"));
	//show file version in lable
	CACSApp* pApp = (CACSApp*)AfxGetApp();	
	m_lblVersion.Format(_T("ACS this program %s"), pApp->GetFileVersion());

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CACSApp::CheckLicense(CString sLicense)
{
	//CACSDoc* pDoc = (CACSDoc*) CFunctionHelper::GetActiveDocument();
	//m_txtActiveCode = pDoc->m_activeCode;
	//step 1: get activate code
	m_activeCode = CFunctionHelper::CreateActiveCodeFromHddSerial();
	//step 2: get license key from register or config
	//Get license string from register:
	CString sLicenseFromReg;
	CString sStartDateEncoded;
	if(sLicense.GetLength() == 0)
	{
		sLicenseFromReg = GetProfileString(_T("CONFIG"), _T("LICENSE_KEY"), _T(""));
		m_licenseKey = sLicenseFromReg;
	}
	else
		sLicenseFromReg = sLicense;
	//NOTE THAT: the start date will be save in number of days format
	int iStartDate = (int) GetProfileInt(_T("CONFIG"), _T("START_DATE"), 0);
	
	if(sLicenseFromReg.GetLength() == 0)
	{
		//if not found, get start date		
		m_bIsRegistered = FALSE;
		m_maxNumRunAction = 5;
		
		COleDateTime dtNow = COleDateTime::GetCurrentTime();
		
		
		if(iStartDate <= 0)
		{
			//first time running => set start date = current
			//save current to registry
			m_startDate.m_dt = dtNow.m_dt;
			WriteProfileInt(_T("CONFIG"), _T("START_DATE"), (UINT) m_startDate.m_dt);
			//end date = start + 3;
			m_endDate.m_dt = m_startDate.m_dt + 3;
		}
		else
		{
			//if start date exist and unregisterd => check if end date < current date => display exprite message			
			m_startDate.m_dt = iStartDate;			
			//end date = start + 3;
			m_endDate.m_dt = m_startDate.m_dt + 3;
//MYDEL			//check if end date < current date => display exprite message
//MYDEL			if((UINT)m_endDate.m_dt < (UINT) dtNow.m_dt)
//MYDEL			{
//MYDEL				//display message
//MYDEL				AfxMessageBox(_T("Trial time has exprited, Please register this program to use full feature and support!"));
//MYDEL			}
		}
	}
	else//if has license => check if this license is correct
	{
		//BOOL bIsLicenseCorrect = FALSE;
		CFunctionHelper::CheckLicense(m_bIsRegistered , m_startDate, m_endDate, sLicenseFromReg);
		if(sLicense.GetLength() > 0)//if user enter license key
		{
			if(m_bIsRegistered)
			{
				WriteProfileString(_T("CONFIG"), _T("LICENSE_KEY"), sLicense);
				m_licenseKey = sLicense;
				WriteProfileInt(_T("CONFIG"), _T("START_DATE"), (int) m_startDate.m_dt);
			}
		}
		//     if(!m_bIsRegistered)
		//     {
		//       AfxMessageBox(_T("The license key is invalid!\nPlease register again with the correct license key!"));
		// 
		//       //       UINT numDay = 0;
		//       //       WriteProfileString(_T("CONFIG"), _T("LICENSE_KEY"), _T(""));
		//       //       WriteProfileInt(_T("CONFIG"), _T("START_DATE"), 0);
		// //       numDay = (UINT) m_endDate.m_dt - m_startDate.m_dt;
		//     }
	}
	//if(m_txtSer
	//MYDEL   	COleDateTime dtNow;
	//MYDEL   	dtNow = COleDateTime::GetCurrentTime();
	//MYDEL   
	//MYDEL   	m_txtSerial = CFunctionHelper::CreateLicenseToClient(m_txtActiveCode, dtNow,30);
	//MYDEL   	CString sTrace;
	//MYDEL   	sTrace.Format(_T("server generates serial: %s\n"), m_txtSerial);
	//MYDEL   	TRACE(sTrace);
	//MYDEL   	BOOL bIsLicenseCorrect;
	//MYDEL   	CFunctionHelper::CheckLicense(bIsLicenseCorrect, m_txtSerial);
	//MYDEL   
	//MYDEL   	CString hddSerial;
	//MYDEL   	diskid32 m_disk;
	//MYDEL   	m_disk.GetHddSerial(hddSerial);
	//MYDEL   	m_txtSerial = hddSerial;
	//MYDEL   	AfxMessageBox(hddSerial);
}



void CAboutDlg::OnbtnRegister() 
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	if(m_txtNewLicense.GetLength() > 0)
	{
		theApp.CheckLicense(m_txtNewLicense);
		if(m_bIsRegistered)
		{
			RefrestStatusRegister();
			AfxMessageBox(_T("Register successful!"));
		}
		else
		{
			AfxMessageBox(_T("Please enter correct license key to text box!"));
			GetDlgItem(txtNewLicense)->SetFocus();
		}
	}
	else
	{
		AfxMessageBox(_T("Please enter correct license key to text box!"));
		//m_txtNewLicense.Focus();
	}
}

void CAboutDlg::RefrestStatusRegister()
{
	m_txtActiveCode = theApp.m_activeCode;
	CDateTimeFormat dtf;
	dtf.SetDateTime(theApp.m_startDate);
	dtf.SetFormat(_T("dd-MM-yyyy"));
	m_txtStartDate = dtf.GetString();
	//end
	dtf.SetDateTime(theApp.m_endDate);
	dtf.SetFormat(_T("dd-MM-yyyy"));
	m_txtEndDate = dtf.GetString();
	//set license text box
	m_txtCurrentLicense = theApp.GetProfileString(_T("CONFIG"), _T("LICENSE_KEY"), _T(""));
	//m_txtNewLicense = CFunctionHelper::CreateLicenseToClient(m_txtActiveCode, COleDateTime::GetCurrentTime(),5);
	if(m_bIsRegistered)
		m_lblRegisterMsg = _T("This program was registered successfully. Thank you!");
	else
	{
		UINT currentNumDay = (UINT)COleDateTime::GetCurrentTime().m_dt;
		if((UINT)theApp.m_startDate.m_dt <= currentNumDay 
				&& currentNumDay <= (UINT)theApp.m_endDate.m_dt)
			m_lblRegisterMsg = _T("This program is in trial mode.\r\nPlease register a license key to get full features");
		else
			m_lblRegisterMsg = _T("The license key was expired. Now you are in a trial mode.\r\nPlease register new license key to use full features and supported");
	}
	UpdateData(false);
}

void CAboutDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWnd* pwndCtrl = GetFocus();
	CWnd* pwndCtrlNext = pwndCtrl;
	int ctrl_ID = pwndCtrl->GetDlgCtrlID();
	
	switch (ctrl_ID) {
//MYDEL	case IDC_EDIT1:
//MYDEL		pwndCtrlNext = GetDlgItem(IDC_EDIT2);
//MYDEL		break;
	case IDC_OK:
		CDialog::OnOK();
		break;
	default:
		break;
	}
	pwndCtrlNext->SetFocus();
	//CDialog::OnOK();
}

void CAboutDlg::OnClickBtnOK() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add extra validation here
	CWnd* pwndCtrl = GetFocus();
	CWnd* pwndCtrlNext = pwndCtrl;
	int ctrl_ID = pwndCtrl->GetDlgCtrlID();
	
	switch (ctrl_ID) {
//MYDEL	case IDC_EDIT1:
//MYDEL		pwndCtrlNext = GetDlgItem(IDC_EDIT2);
//MYDEL		break;
	case IDC_OK:
		CDialog::OnOK();
		break;
	default:
		break;
	}
	pwndCtrlNext->SetFocus();
	//CDialog::OnOK();
}

BOOL CAboutDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
			int i = 0;
	} 
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CACSApp::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL bRs = FALSE;
	if(pMsg->message==WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			bRs = TRUE;
			break;
		case VK_F1:
			OnHelpIndex();
			bRs = TRUE;
			break;
		}
		if(bRs)
			return bRs;
	}
	
	return CWinApp::PreTranslateMessage(pMsg);
}

void CACSApp::OnHelpIndex() 
{
	// TODO: Add your command handler code here
	CString helpFile;
	
	helpFile.Format(_T("%s%s"), CFunctionHelper::GetCurrentDirectory(), _T("\\acs.chm"));
	HINSTANCE hRs = ShellExecute((HWND)m_hInstance, _T("open"), helpFile,_T(""), _T(""), SW_SHOWMAXIMIZED);
//MYDEL   	switch((int)hRs)
//MYDEL   	{
//MYDEL   	case 1:
//MYDEL   	case 2:
//MYDEL   		TRACE(_T("file: %s was not found\n"), helpFile);
//MYDEL   		AfxMessageBox(helpFile);
//MYDEL   		break;
//MYDEL   	}
}

//DEL void CACSApp::OnToolSchedule() 
//DEL {
//DEL 	// TODO: Add your command handler code here
//DEL 	CRecordNoteDlg recDlg;
//DEL 	recDlg.DoModal();
//DEL }

CString CACSApp::GetFileVersion()
{
	CString sVersion = _T("");
	CFileVersionInfo fvi;
	if(fvi.Create())
	{
		sVersion.Format(_T("%d.%d.%d.%d")
			, fvi.GetFileVersion(3), fvi.GetFileVersion(2), fvi.GetFileVersion(1), fvi.GetFileVersion(0));
	}
	
	return sVersion;
}
