// ACSDoc.h : interface of the CACSDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACSDOC_H__60B23172_6083_4273_86B0_72822F0AD1A6__INCLUDED_)
#define AFX_ACSDOC_H__60B23172_6083_4273_86B0_72822F0AD1A6__INCLUDED_

#include "HighTime.h"	// Added by ClassView
#include "DateTimeFormat.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WindowAction.h"
#include "ReportCtrl.h"
#include "KeyWindowAction.h"
#include "MouseWindowAction.h"
#include "FunctionHelper.h"

//#include "MouseManager.h"

extern ePlayer m_statusScript;
extern ePlayer m_statusTimer;



//globle struct
struct THREADINFO
{
    HWND hWnd;
	CTypedPtrArray<CObArray, CWindowAction*>* m_listWindowAction;
	BOOL bIsSetActive;
	BOOL bIsMoveMouseBeforeClick;
};

struct THREADINFOTIMECHECKING
{
    HWND hWndToSendMsg;	
};


class CACSDoc : public CDocument
{
protected: // create from serialization only
	CACSDoc();
	DECLARE_DYNCREATE(CACSDoc)

// Attributes
public:
	CTypedPtrArray<CObArray, CWindowAction*> m_listWindowAction;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CACSDoc)
	public:
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnNewDocument();
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// Implementation
public:
	void RefreshThreadTimeChecking();
	CWindowAction* GetLastWindowAction();
	THREADINFO * m_pInfo;
	THREADINFOTIMECHECKING * m_pInfoCheckingTime;
	HWND GetLastHwnd();
	HWND m_hDesktop;
	void SetItemTextMouse(CReportCtrl *m_wndList, CMouseWindowAction* pMouse, int IDX);
	void SetItemTextKey(CReportCtrl *m_wndList, CKeyWindowAction* pKey, int IDX);
	void RefreshItemToListReport(CWindowAction *pWindowAction);
	void InsertItemToListReport(CReportCtrl* m_wndList, CWindowAction* pWindowAction);
	void RemoveWindowActionItem(int Index);
	void RefreshListReport();
	void AddWindowAction(CWindowAction *pWindowAction);
	CWindowAction* GetWindowAction(int index);
	void AddWindowAction(HWND hWnd, CString strCaption, CString strClassName
									, long classStyle, RECT rect,long procId, eKindOfWindowAction eKind);
	
	virtual ~CACSDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CACSDoc)
	afx_msg void OnEditClearAll();
	afx_msg void OnUpdateEditClearAll(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CACSDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	//COleDateTime m_timePre;	
	CHighTime m_timePre;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACSDOC_H__60B23172_6083_4273_86B0_72822F0AD1A6__INCLUDED_)
