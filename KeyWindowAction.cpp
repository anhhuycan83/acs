// KeyWindowAction.cpp: implementation of the CKeyWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "KeyWindowAction.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CKeyWindowAction, CObject, 1);//for implement virtual serialize

CKeyWindowAction::CKeyWindowAction()
{
	//m_strToEnter = _T("");
	m_eKindOfWindowAction = KeyWindowAction;
//	m_parentWA = new CWindowAction();
//	m_parentWA->m_hWnd = NULL;
//	m_parentWA->m_caption.Empty();
//	m_parentWA->m_className.em();
//	m_parentWA->m_calssStyle = 0;
}

CKeyWindowAction::~CKeyWindowAction()
{

}

void CKeyWindowAction::Serialize(CArchive &ar)
{
	CString sHwnd,sEnum, sParentHwnd;
	if(ar.IsStoring())
	{			
		//s.Format(_T("%d"), m_eKindOfWindowAction);			
		//s2.Format(_T("0x%08X"), m_hWnd);
		sHwnd.Format(_T("%d"), m_hWnd);
		//CFunctionHelper::convert_long_to_hex(sHwnd, (ULONG)m_hWnd);
		sEnum = CFunctionHelper::EnumToStringKindOfWindowAction(m_eKindOfWindowAction);
		//ar << m_hWnd << m_caption << m_className << m_calssStyle;// << m_eKindOfWindowAction << m_strToEnter;		
		ar << sHwnd << m_caption << m_className << m_calssStyle << sEnum;// << m_strToEnter;		
		//save parent infor
		//CFunctionHelper::convert_long_to_hex(sParentHwnd, (ULONG)m_parentWI.m_hWnd);
		sParentHwnd.Format(_T("%d"), m_parentWI.m_hWnd);
		ar << sParentHwnd << m_parentWI.m_caption << m_parentWI.m_className << m_parentWI.m_calssStyle;
		m_listKeyPress.Serialize(ar);
	}
	else
	{
		ar >> sHwnd >> m_caption >> m_className >> m_calssStyle >> sEnum;// >> m_strToEnter;
		_stscanf(sHwnd, _T("%d"), &m_hWnd);
		//m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sHwnd); //(sHwnd, (ULONG)m_hWnd);
		m_eKindOfWindowAction = CFunctionHelper::StringToEnumKindOfWindowAction(sEnum);// (m_eKindOfWindowAction);
		//get parent infor		
		ar >> sParentHwnd >> m_parentWI.m_caption >> m_parentWI.m_className >> m_parentWI.m_calssStyle;
		_stscanf(sParentHwnd, _T("%d"), &m_parentWI.m_hWnd);
		//m_parentWI.m_hWnd = (HWND)CFunctionHelper::hex_string_to_long(sParentHwnd);
		m_listKeyPress.Serialize(ar);
		
		//m_eKindOfWindowAction = _ttoi(s);
	}
}
