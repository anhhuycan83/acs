// ScriptManager.cpp: implementation of the CScriptManager class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "ScriptManager.h"
#include "FunctionHelper.h"
#include "ACSView.h"
#include "MainTab.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//for globle declare
HINSTANCE hKMCDll;
MYKMCPROCMOUSECLICK m_pMouseClick;
MYKMCSENDKEY m_pSendKey;
HMODULE hModuleKMC;
MYPostMessageExPROC pfMyPostMessageEx;
MYSetForegroundWindowInternalPROC pfSetForegroundWindowInternal;
MYSendKeyUpDownToHwndPROC pfSendKeyUpDownToHwnd;
MYSendVKToHwndPROC pfSendVKToHwnd;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScriptManager::CScriptManager()
{
	m_pDoc = NULL;
	hKMCDll = NULL;
	m_currentIndex = 0;
}

CScriptManager::~CScriptManager()
{
	MyUnUseDLL();
}

void CScriptManager::SetPoiterDocument(CACSDoc *pDoc)
{
	m_pDoc = pDoc;
}

bool CScriptManager::RunScriptByIndex(int index)
{
return true;	
}

bool CScriptManager::MyUseDLL()
{
	if(!hKMCDll)
	{
		hKMCDll = LoadLibrary(_T("KMC.dll"));//(LPCTSTR) 
		if(hKMCDll == NULL)
		{
			AfxMessageBox(_T("Can't load KMC.dll"));
			return false;
		}
		hModuleKMC = GetModuleHandle(_T("KMC"));
		if(hModuleKMC)
			return false;
	}
	return true;
}

void CScriptManager::MyUnUseDLL()
{
	if(hKMCDll)
	{
		FreeLibrary(hKMCDll);
		hKMCDll = 0;
	}
}

bool MouseClick(CMouseWindowAction *mwa, BOOL bIsSetActive, BOOL bIsMoveMouseBeforeClick)
{
	bool bRs= false;	
//MYDEL	if(hKMCDll == NULL)
//MYDEL	{
//MYDEL		return false;
//MYDEL	}
//MYDEL	

//	MYKMCPROCMOUSECLICK pProcFunction;
//	//poiter to function in dll
//	pProcFunction = (MYKMCPROCMOUSECLICK) GetProcAddress(hKMCDll, "ClickHandle");
//	//test pSetHHook if nessesary
//	
//	//MessageBox(filePath);
//	//MyClearMem();
//	//(pGetArrRecordNote)(m_RecordNoteArray, filePath);
	CMouseClick* mc;
	LPARAM lParam;
	//bool bIsMoveMouseBeforeClick;
	
	int nMessageDown, nMessageUp;
	int wparam = MK_LBUTTON;
	//set hwnd to active
	//CACSView* pView = (CACSView*)CFunctionHelper::GetActiveView();
	//pView->UpdateData(true);
//MYDEL	if(pView->IsDlgButtonChecked(chbSetActive))
//MYDEL		SetForegroundWindowInternal(mwa->m_hWnd);
	//::
//MYDEL	bIsMoveMouseBeforClick = pView->IsDlgButtonChecked(chbMoveMouseBeforClick);
	
	for(int i = 0; i< mwa->m_listMouseClick.GetSize(); i++)
	{
		if(m_statusScript != PRUN)
			return false;
		mc = mwa->m_listMouseClick.GetAt(i); //move to first because waiting for window created
		if(i > 0)
		{
			TRACE(_T("\nnext item: %d\n"), mc->m_milisecond);
			Sleep(mc->m_milisecond);
		}
		if(bIsSetActive)
			SetForegroundWindowInternal(mwa->m_hWnd);

		
//		if(::IsMenu((HMENU)mwa->m_hWnd))
//		{
//			nMessageDown = WM_COMMAND;
//			nMessageUp = 0;//not ues because send command message menu item click
//			wparam = MAKELPARAM(mc->m_poiterClick.x, mc->m_poiterClick.y);
//			lParam = 0;
//		}
//		else
//		{
//			
//		}

		switch(mc->m_eKindOfClick)
		{
		case LeftClick:
			wparam = MK_LBUTTON;
			nMessageDown = WM_LBUTTONDOWN;
			nMessageUp = WM_LBUTTONUP;			
			break;
		case RightClick:
			wparam = MK_RBUTTON;
			nMessageDown = WM_RBUTTONDOWN;
			nMessageUp = WM_RBUTTONUP;		
			break;
		case MiddleClick:
			wparam = MK_MBUTTON;
			nMessageDown = WM_MBUTTONDOWN;
			nMessageUp = WM_MBUTTONUP;		
			break;//MiddleClick
		case ScrollUp:
			//wparam = VK_RBUTTON;
			break;//MiddleClick
		case ScrollDown:
			//wparam = VK_RBUTTON;
			break;//MiddleClick
		}
		lParam = MAKELPARAM(mc->m_poiterClick.x, mc->m_poiterClick.y);
		
		//(pProcFunction)(mwa->m_hWnd, mc->m_poiterClick.x, mc->m_poiterClick.y);
//use sendinput
	// Toggle Caps Lock key:
		//CPoint screenPoint(mc->m_poiterClick.x, mc->m_poiterClick.y);
		//CPoint screenPoint;
		CRect rect;
		GetWindowRect(mwa->m_hWnd, &rect);
		//ClientToScreen(mwa->m_hWnd, &screenPoint);
//MYDEL    INPUT input[1];



//MYDEL    ::ZeroMemory(input, sizeof(input));        
//MYDEL    input->type = INPUT_MOUSE;
//MYDEL	
//MYDEL	input[0].mi.dx = rect.left + mc->m_poiterClick.x;
//MYDEL	input[0].mi.dy = rect.top + mc->m_poiterClick.y;
//MYDEL//	input[0].mi.dx = screenPoint.x;
//MYDEL//		input[0].mi.dy = screenPoint.y;
//MYDEL	input[0].mi.dwFlags = MOUSEEVENTF_VIRTUALDESK|MOUSEEVENTF_LEFTDOWN;
//MYDEL	SetCursorPos(input[0].mi.dx,input[0].mi.dy);
//MYDEL	input[0].mi.mouseData = 0;
//MYDEL	
//MYDEL	input[0].mi.time = 0;
//MYDEL	input[0].mi.dwExtraInfo = 0;
//MYDEL//    input[0].ki.wVk  = input[1].ki.wVk = VK_CAPITAL;        
//MYDEL//    input[1].ki.dwFlags = KEYEVENTF_KEYUP;  // THIS IS IMPORTANT
//MYDEL    ::SendInput(1, input, sizeof(INPUT));
//MYDEL	input[0].mi.dwFlags = MOUSEEVENTF_VIRTUALDESK|MOUSEEVENTF_LEFTUP;
//MYDEL	::SendInput(1, input, sizeof(INPUT));

			bRs= MyPostMessageEx(mwa->m_hWnd, nMessageDown, nMessageUp, wparam, lParam,
			0, lParam, bIsMoveMouseBeforeClick, false, true);
		//Sleep(1000);
	}
	return bRs;
}

bool CScriptManager::RunScriptAll()
{
	bool bRs = true;	
	SetTxtWAIF(NULL, false, 0);
/*
	bool bRs = false;
	MyUseDLL();
	//clear txtWAIF
	SetTxtWAIF(NULL, false);
	CACSApp* pApp = (CACSApp*)AfxGetMainWnd();
	for(int i = 0; i<m_pDoc->m_listWindowAction.GetSize(); i++)
	{
		if(!pApp->m_bIsRegistered)
		{
			if(i > pApp->m_maxNumRunAction)
			{
				CString msg;
				msg.Format(_T("In trial mode, you can only run 5 actions\nPlease register for using full feature!"),
					pApp->m_maxNumRunAction);
				AfxMessageBox(msg);
				break;
			}
		}
		bRs = RunScriptByIndex(i);
		Sleep(1000);
	}
*/
	CACSView* pV = (CACSView*)CFunctionHelper::GetActiveView();
	m_statusScript = PRUN;
	RefreshThread();
	return bRs;


}


UINT ThreadRunningSript(PVOID pParam)
{
	THREADINFO *pInfo = reinterpret_cast<THREADINFO*> (pParam);
	//MYDEL	CWnd *pWnd = CWnd::FromHandle(pInfo->hWnd);
	//MYDEL	CACSView* pV = (CACSView*)pWnd;
	CWindowAction* wa = NULL;
	int exitCode = 2;
//MYDEL	CString s;
		//CACSApp* pApp = (CACSApp*)AfxGetMainWnd();
	do
	{
		for(int i = 0; i<pInfo->m_listWindowAction->GetSize(); i++)
		{
			//MYDEL		s.Format(_T("i: %d, status: %d\n"), i, m_statusScript);
			//MYDEL		TRACE(s);
			//post to CView to tell a index of current running
			::PostMessage(pInfo->hWnd, MYWM_SCRIPT_RUNNING_INDEX, i, 0);
			
			if(!m_bIsRegistered)
			{
				if(i > m_maxNumRunAction - 1)
				{
					CString msg;
					msg.Format(_T("In trial mode, you can only run max: %d actions\nPlease register for using full feature!"),
						m_maxNumRunAction);
					AfxMessageBox(msg);
					wa = NULL;
					exitCode = 4;
					break;
				}
			}
			
			if(m_statusScript == PRUN)
			{
				wa = pInfo->m_listWindowAction->GetAt(i);
				TRACE(_T("first item: %d\n"), wa->m_milisecond);
				Sleep(wa->m_milisecond);

				if(ReSetWindownHandle(wa))
				{
					switch(wa->m_eKindOfWindowAction)
					{
					case KeyWindowAction:
						KeyPress((CKeyWindowAction*)wa, pInfo->bIsSetActive);
						break;
					case MouseWindowAction:
						MouseClick((CMouseWindowAction*)wa, pInfo->bIsSetActive, pInfo->bIsMoveMouseBeforeClick);
						break;
					}
					exitCode = 0;					
				}
				else
				{
					exitCode = 1;
					break;//exit for loop
				}
				
			}
			else
			{
				exitCode = 3;//user stop thread
				break;
			}
		}
		if(m_bIsRepeat && exitCode == 0)
			PostMessage(pInfo->hWnd, MYWM_SCRIPT_REPEAT, 0, 0);
	}while(m_bIsRepeat && exitCode == 0);//end while repeat loop
	//MYDEL	pV->GetMainTab()->StopAction();
	//MYDEL	m_statusScript = PSTOP;
	//MYDEL	pS->MyUnUseDLL();
	if(m_statusScript == PSTOP)
	{
		exitCode = 3;//user stop thread
	}
	
	
	//post to CView to tell a thread has finished
	PostMessage(pInfo->hWnd, MYWM_SCRIPT_FINISHED, exitCode, (LPARAM)wa);
	//CString s;
//MYDEL	s.Format(_T("hInfoMain: %x\n"), pInfo->hWnd);
//MYDEL	TRACE(s);
//MYDEL   	delete pInfo; //NO DELETE BECAUSE CDOC WILL DELETE IT IN DESCTRUCTOR
//MYDEL   	pInfo = 0;

//MYDEL	switch(exitCode)
//MYDEL	{
//MYDEL	case 0://exit normal
//MYDEL		PostMessage(pInfo->hWnd, MYWM_SCRIPT_FINISHED, exitCode, wa);
//MYDEL		break;
//MYDEL	case 1://handle not found
//MYDEL		PostMessage(pInfo->hWnd, MYWM_SCRIPT_FINISHED, exitCode, wa);
//MYDEL		break;
//MYDEL	}
	
	return 0;
}



bool MyPostMessageEx(HWND hWnd, int messageDown, int messageUp,
									  WPARAM wParamDown, LPARAM lParamDown,
									  WPARAM wParaUp, LPARAM lParamUp,
									  bool bIsMoveMouse, bool bIsReturnOldPoint, bool bIsMouseMoveBetween)
{
	bool bRs=true;	

	
	if(hModuleKMC == NULL)
	{
		return false;
	}

	pfMyPostMessageEx = (MYPostMessageExPROC) GetProcAddress(hModuleKMC, "PostMessageEx");

	(pfMyPostMessageEx)(hWnd, messageDown, messageUp,
									  wParamDown, lParamDown,
									  wParaUp, lParamUp,
									  bIsMoveMouse, bIsReturnOldPoint, bIsMouseMoveBetween);

	return bRs;
}

bool SetForegroundWindowInternal(HWND hWnd)
{
	bool bRs=false;	
//MYDEL	if(hModuleKMC == NULL)
//MYDEL	{
//MYDEL		return false;
//MYDEL	}

	pfSetForegroundWindowInternal = (MYSetForegroundWindowInternalPROC) GetProcAddress(hModuleKMC, "SetForegroundWindowInternal");

	(pfSetForegroundWindowInternal)(hWnd);
	if(::GetForegroundWindow() == hWnd)
		bRs = true;
	return bRs;
}


BOOL CALLBACK EnumWindowsTopWA(HWND hWnd, LPARAM lParam)
{
	if(!::IsWindowVisible(hWnd))
		return true;
	CWindowAction* wa = (CWindowAction*)lParam;
	if(wa)
	{
		CWindowInfor wIF;
		CFunctionHelper::SetWindowInfor(&wIF, hWnd);
		if(wIF.m_calssStyle == wa->m_calssStyle)
		{
			if(wIF.m_className == wa->m_className)
			{
				if(wa->m_caption.GetLength() == 0)
				{
					//save this new hanle
					wa->m_hWnd = hWnd;
					//stop search next window => must return false
					return false;
				}
				else
				{
					if(wIF.m_caption == wa->m_caption)
					{
						//save this new hanle
						wa->m_hWnd = hWnd;
						//stop search next window => must return false
						return false;						
					}
				}
			}
		}
	}
	return true;
}

BOOL CALLBACK EnumChildWAProc(HWND hWnd, LPARAM lParam)
{
	if(!::IsWindowVisible(hWnd))
		return true;
	CWindowAction* wa = (CWindowAction*)lParam;
	if(wa)
	{
		CWindowInfor wIF;
		CFunctionHelper::SetWindowInfor(&wIF, hWnd);
		if(wIF.m_calssStyle == wa->m_calssStyle)
		{
			if(wIF.m_className == wa->m_className)
			{
				if(wIF.m_caption == _T("OK"))
				{
					int a=0;
				}

				if(wa->m_caption.GetLength() == 0)
				{
					//save this new hanle
					wa->m_hWnd = hWnd;
					//stop search next window => must return false
					return false;
				}
				else
				{
					if(wIF.m_caption == wa->m_caption)
					{
						//save this new hanle
						wa->m_hWnd = hWnd;
						//stop search next window => must return false
						return false;
					}
				}
			}
		}
	}
	return true;
}

BOOL CALLBACK EnumWindowsParentWA(HWND hWnd, LPARAM lParam)
{
	if(!::IsWindowVisible(hWnd))
		return true;
	CWindowAction* wa = (CWindowAction*)lParam;
	if(wa)
	{
		CWindowInfor wIF;		
		CFunctionHelper::SetWindowInfor(&wIF, hWnd);
		if(wIF.m_calssStyle == wa->m_parentWI.m_calssStyle)
		{
			if(wIF.m_className == wa->m_parentWI.m_className)
			{
				if(wa->m_parentWI.m_caption.GetLength() == 0)
				{
					//save this new hanle
					wa->m_parentWI.m_hWnd = hWnd;
					//stop search next window => must return false
					return false;
				}
				else
				{
					if(wIF.m_caption == wa->m_parentWI.m_caption)
					{
						//save this new hanle
						wa->m_parentWI.m_hWnd = hWnd;
						//stop search next window => must return false
						return false;
					}
				}
			}
		}
	}
	return true;
}

bool ReSetWindownHandle(CWindowAction *wa)
{
	bool bRs = false;
	//now reset the handle of this window
	//check if window is a popup menu
//	if(wa->m_className== _T("#32768"))//0) //&& wa->m_className == _T(""))
//		wa->m_parentWI.m_hWnd = 0;
//	HWND hGA_PARENT, hGA_ROOT, hGA_ROOTOWNER;
//	hGA_ROOT = ::GetAncestor(wa->m_hWnd, GA_ROOT);
//		hGA_ROOTOWNER = ::GetAncestor(wa->m_hWnd, GA_ROOTOWNER);
//		if((long) hGA_ROOTOWNER == 65552 || wa->m_calssStyle == -1803550720)
//		{
//			//wa->m_parentWI.m_hWnd = 0;
////			CWindowInfor wIF;
////			CFunctionHelper::SetWindowInfor(&wIF, hGA_ROOT);
//			wa->m_parentWI.m_caption = wa->m_caption;
//			wa->m_parentWI.m_className = wa->m_className;
//			wa->m_parentWI.m_calssStyle = wa->m_calssStyle;
//			wa->m_parentWI.m_hWnd = hGA_ROOT;
//
//		}
//		else
//		{
//			hGA_PARENT = ::GetAncestor(wa->m_parentWI.m_hWnd, GA_PARENT);
//			if((long)hGA_PARENT == 0)
//			{
//				CWindowInfor wIF;
//			CFunctionHelper::SetWindowInfor(&wIF, hGA_ROOT);
//			wa->m_parentWI = wIF;
//							wa->m_parentWI.m_hWnd = hGA_ROOT;
//
//			}
//		}
		//check if window does not exist => top window or menu
//MYDEL		if(!IsWindow(wa->m_hWnd))
//MYDEL			wa->m_parentWI.m_hWnd =	NULL;


		//if(wa->m_parentWI.m_hWnd == 0  || (long)hGA_PARENT == 0)//top window 65552 = 10010
		if(wa->m_parentWI.m_hWnd == NULL)
		{
			bRs = EnumWindows((WNDENUMPROC)EnumWindowsTopWA, (LPARAM)wa);//EnumWindowsTopWA
			if(!bRs)
				bRs = true;
			else 
				bRs = false;
		}		
		else //child window => search child
		{
			//step 1: search top (main app) belong to parent of mwa
			bRs = EnumWindows((WNDENUMPROC)EnumWindowsParentWA, (LPARAM)wa);
			if(!bRs)
			{
				//step 2: now enum all child window of this parent to compare with current mwa
				bRs = EnumChildWindows(wa->m_parentWI.m_hWnd, (WNDENUMPROC)EnumChildWAProc, (LPARAM)wa);
				if(!bRs)					
					bRs = true;
				else//not found
					bRs = false;
			}
			else
			{
				bRs = false;//not found
			}
//MYDEL			else//not found
//MYDEL			{
//MYDEL				// recheck if this window is a top window:
//MYDEL				bRs = EnumWindows((WNDENUMPROC)EnumWindowsParentWA, (LPARAM)wa);//EnumWindowsTopWA
//MYDEL			if(!bRs)
//MYDEL				bRs = true;
//MYDEL			else 
//MYDEL				bRs = false;
//MYDEL				
//MYDEL			}
		}
	return bRs;

}

void CScriptManager::SetTxtWAIF(CWindowAction *pWA, bool bIsFound, int exitCode)
{
	CACSView* pView = (CACSView*)CFunctionHelper::GetActiveView();
	CMainTab* pMainTab = (CMainTab*)pView->GetMainTab();
	if(pWA != NULL)
	{	
		pMainTab->m_txtWAIFValue.Format(_T("hwnd: %5x, root parent: %5x, IsFound: %d, classname: %s, style: %5x, exitCode: %d"), 
			pWA->m_hWnd, pWA->m_parentWI.m_hWnd, bIsFound, pWA->m_className, pWA->m_calssStyle, exitCode);
	}
	else
	{
		if(exitCode == 4)
			pMainTab->m_txtWAIFValue.Format(_T("This program is in trial mode so you can only run max: 5 actions"));
		else
			pMainTab->m_txtWAIFValue.Empty();
	}
	pMainTab->UpdateData(false);
	//pView->GetMainTab()


}

bool KeyPress(CKeyWindowAction *kwa, BOOL bIsSetActive)
{
	bool bRs= false;	
//MYDEL	if(hKMCDll == NULL)
//MYDEL	{
//MYDEL		return false;
//MYDEL	}
//MYDEL	

	CKeyPress* kp;
	
	//set hwnd to active
//MYDEL	CACSView* pView = (CACSView*)CFunctionHelper::GetActiveView();
//MYDEL	//pView->UpdateData(true);
	
//MYDEL	GenerateKey('T', FALSE);
//MYDEL	GenerateKey('a', FALSE);
	
	for(int i = 0; i< kwa->m_listKeyPress.GetSize(); i++)
	{
		if(m_statusScript != PRUN)
			return false;
		kp = kwa->m_listKeyPress.GetAt(i);
		
		if(i > 0)
		{
			TRACE(_T("\nnext item: %d\n"), kp->m_milisecond);
			Sleep(kp->m_milisecond);
		}
		if(bIsSetActive)
			SetForegroundWindowInternal(kwa->m_hWnd);		
				
		GenerateChar(kp, 200);
	}
	return bRs;
}

bool CScriptManager::SendKeyUpDownToHwnd(HWND hwnd, TCHAR ch, BOOL bIsShiftDown)
{
	bool bRs=true;	

	
	if(hModuleKMC == NULL)
	{
		return false;
	}
//MYSendKeyCharToHwndPROC pfSendKeyCharToHwnd;
	pfSendKeyUpDownToHwnd = (MYSendKeyUpDownToHwndPROC) GetProcAddress(hModuleKMC, "SendKeyUpDownToHwnd");

	(pfSendKeyUpDownToHwnd)(hwnd, ch, bIsShiftDown);

	return bRs;
}

void CScriptManager::SendVKToHwnd(HWND hwnd, short vkKey, BOOL bIsSysKey)
{
	
	if(hModuleKMC == NULL)
	{
		return;
	}
	pfSendVKToHwnd = (MYSendVKToHwndPROC) GetProcAddress(hModuleKMC, "SendVKToHwnd");

	(pfSendVKToHwnd)(hwnd, vkKey, bIsSysKey);
}

bool CScriptManager::SendNormalChar(HWND hwnd, byte asciiCode)
{
	bool bRs = false;
	//upper case chars
//MYDEL	if(asciiCode >= 65 && asciiCode <= 90)
//MYDEL	{
//MYDEL		//send char with holding shift key to upper case these chars
//MYDEL		bRs=SendKeyUpDownToHwnd(hwnd, (TCHAR) asciiCode, TRUE);
//MYDEL	}
//MYDEL	else
//MYDEL	{
//MYDEL		switch(asciiCode)
//MYDEL		{
//MYDEL			//other chars with holding shift key
//MYDEL		case 126: //~
//MYDEL		case 33://!
//MYDEL		case 64://@
//MYDEL		case 35://#
//MYDEL		case 36://$
//MYDEL		case 37://%
//MYDEL		case 94://^
//MYDEL		case 38://&
//MYDEL		case 42://*
//MYDEL		case 40://(
//MYDEL		case 41://)
//MYDEL		case 95://_
//MYDEL		case 43://+
//MYDEL		case 123://{
//MYDEL		case 125://}
//MYDEL		case 124://|
//MYDEL		case 58://:
//MYDEL		case 34://"
//MYDEL		case 60://<
//MYDEL		case 62://>
//MYDEL		case 63://?
//MYDEL			bRs=SendKeyUpDownToHwnd(hwnd, (TCHAR) asciiCode, TRUE);
//MYDEL			break;
//MYDEL		default://and the others, send normal key with asccii code
//MYDEL			bRs=SendKeyUpDownToHwnd(hwnd, (TCHAR) asciiCode, FALSE);
//MYDEL			break;
//MYDEL
//MYDEL		}
//MYDEL	}
//MYDEL	GenerateKey((TCHAR) asciiCode, FALSE);
//MYDEL	GenerateKey('T', FALSE);
//MYDEL	GenerateKey('a', FALSE);
	return bRs;	
}

void CScriptManager::GenerateKey(int ch, BOOL bExtended)
{
//MYDEL	SHORT vks = VkKeyScan(ch);
//MYDEL	//byte vk = LOBYTE(vks);
//MYDEL	byte vk = vks & 0x00FF;
	//byte shift = vks & 0xFF00;

	//SHORT vk = VkKeyScan(ch);
	KEYBDINPUT  kb = {0};

    INPUT       Input = {0};


    /* Generate a "key down" */

    if (bExtended) 
	{ 
		kb.dwFlags  = KEYEVENTF_EXTENDEDKEY; 
		kb.wVk;
	}
	else
	{
		kb.dwFlags |= 0x0004 ;
		kb.wVk  = 0;
		kb.wScan  = ch;
	}

    Input.type  = INPUT_KEYBOARD;

    Input.ki  = kb;

    SendInput(1, &Input, sizeof(Input));


    /* Generate a "key up" */

    ZeroMemory(&kb, sizeof(KEYBDINPUT));

    ZeroMemory(&Input, sizeof(INPUT));

    kb.dwFlags  =  KEYEVENTF_KEYUP;

    if (bExtended) 
	{ 
		kb.dwFlags  |= KEYEVENTF_EXTENDEDKEY; 
		kb.wVk;
	}
	else
	{
		kb.dwFlags |= 0x0004 ;
		kb.wVk  = 0;
		kb.wScan  = ch;
	}

    Input.type = INPUT_KEYBOARD;

    Input.ki = kb;

    SendInput(1, &Input, sizeof(Input));


    return;


}

//using KeyInput method
void GenerateChar(CKeyPress* kp, int time)
{
	bool bShift, bControl, bAlt;	
	unsigned int nPos, nCpt;
	TCHAR cChar;
	unsigned short nKeyScan, wVk, wScan;
	INPUT input[8];
	bool bIsExentdedKeyFlag;	
	switch(kp->m_eKindOfChar)
	{
	case NormalChar://kwa->m_hWnd 00020514
		//bRs = SendNormalChar(kwa->m_hWnd, kp->m_asciiCode);		
		//GenerateKey((TCHAR) kp->m_asciiCode, FALSE);
		cChar = (TCHAR) kp->m_asciiCode;
		nKeyScan = VkKeyScan(cChar);
		wVk = LOBYTE(nKeyScan);
		wScan = MapVirtualKey(LOBYTE(nKeyScan), 0);
		bShift = (HIBYTE(nKeyScan) & 1?1:0);
		bControl = (HIBYTE(nKeyScan) & 2?1:0);
		bAlt = (HIBYTE(nKeyScan) & 4?1:0);
		bIsExentdedKeyFlag = false;	
		break;
	case FunctionChar:
		//MAPVK_VSC_TO_VK = 1
		//MapVirtualKey(kp->m_ks.nScanCode, 1);
		//SendVKToHwnd(kwa->m_hWnd,(SHORT) MapVirtualKey(kp->m_ks.nScanCode, 1), FALSE);
		//GenerateKey((TCHAR) kp->m_vkCode, TRUE);
		bShift = bControl = bAlt = false;
		wVk = kp->m_vkCode;
		wScan = MapVirtualKey(kp->m_vkCode, 0);
		bIsExentdedKeyFlag = kp->m_ks.nExtended;
		break;
	default:
		return;
	}

	ZeroMemory(input, sizeof input);
	nCpt = 0;
	if (bShift)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_SHIFT;
		input[nCpt].ki.wScan = MapVirtualKey(VK_SHIFT, 0);
		input[nCpt].ki.time = time;
		nCpt++;
	}
	if (bControl)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_CONTROL;
		input[nCpt].ki.wScan = MapVirtualKey(VK_CONTROL, 0);
		input[nCpt].ki.time = time;
		nCpt++;
	}
	if (bAlt)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_MENU;
		input[nCpt].ki.wScan = MapVirtualKey(VK_MENU, 0);
		input[nCpt].ki.time = time;
		nCpt++;
	}
	input[nCpt].type = INPUT_KEYBOARD;
	input[nCpt].ki.wVk = wVk;
	input[nCpt].ki.wScan = wScan;
	input[nCpt].ki.time = time;
	//check extended key flag
	if(bIsExentdedKeyFlag)
		input[nCpt].ki.dwFlags |= KEYEVENTF_EXTENDEDKEY;
	CString s;
	s.Format(_T("vk: %x scan: %x ex flag: %d char: %d"), input[nCpt].ki.wVk, input[nCpt].ki.wScan, input[nCpt].ki.dwFlags, cChar);
	TRACE(s);
	nCpt++;
	input[nCpt].type = INPUT_KEYBOARD;
	input[nCpt].ki.wVk = wVk;
	input[nCpt].ki.wScan = wScan;
	input[nCpt].ki.dwFlags = KEYEVENTF_KEYUP;
	input[nCpt].ki.time = time;
	//check extended key flag
	if(bIsExentdedKeyFlag)
		input[nCpt].ki.dwFlags |= KEYEVENTF_EXTENDEDKEY;
	nCpt++;
	if (bShift)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_SHIFT;
		input[nCpt].ki.wScan = MapVirtualKey(VK_SHIFT, 0);
		input[nCpt].ki.dwFlags = KEYEVENTF_KEYUP;
		input[nCpt].ki.time = time;
		nCpt++;
	}
	if (bControl)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_CONTROL;
		input[nCpt].ki.wScan = MapVirtualKey(VK_CONTROL, 0);
		input[nCpt].ki.dwFlags = KEYEVENTF_KEYUP;
		input[nCpt].ki.time = time;
		nCpt++;
	}
	if (bAlt)
	{
		input[nCpt].type = INPUT_KEYBOARD;
		input[nCpt].ki.wVk = VK_MENU;
		input[nCpt].ki.wScan = MapVirtualKey(VK_MENU, 0);
		input[nCpt].ki.dwFlags = KEYEVENTF_KEYUP;
		input[nCpt].ki.time = time;
		nCpt++;
	}
	SendInput(nCpt, input, sizeof INPUT);
}



void CScriptManager::RefreshThread()
{
	CACSView* pV = (CACSView*)CFunctionHelper::GetActiveView();
	switch(m_statusScript)
	{
	case PSTOP:
		DWORD exitCode;
		::GetExitCodeThread(pV->m_pThreadSriptRunning, &exitCode);
		if(exitCode == STILL_ACTIVE)
		{
			WaitForSingleObject(pV->m_pThreadSriptRunning, INFINITE);
		}
//MYDEL		if(pV->m_pThreadSriptRunning)
//MYDEL			delete pV->m_pThreadSriptRunning;
//MYDEL		pV->m_pThreadSriptRunning = 0;
		MyUnUseDLL();
		break;
	case PPAUSE:	
		pV->m_pThreadSriptRunning->SuspendThread();
		break;
	case PRUN:
		if(!pV->m_pThreadSriptRunning)//NOT CREATED
		{
			MyUseDLL();
			if(m_pDoc->m_pInfo)
			{
				delete m_pDoc->m_pInfo;
				m_pDoc->m_pInfo = 0;
			}
			m_pDoc->m_pInfo= new THREADINFO;
			m_pDoc->m_pInfo->hWnd = pV->m_hWnd;
			m_pDoc->m_pInfo->m_listWindowAction = &(m_pDoc->m_listWindowAction);
			if(pV->IsDlgButtonChecked(chbSetActive))
				m_pDoc->m_pInfo->bIsSetActive = TRUE;
			else
				m_pDoc->m_pInfo->bIsSetActive = FALSE;
			if(pV->IsDlgButtonChecked(chbMoveMouseBeforClick))
				m_pDoc->m_pInfo->bIsMoveMouseBeforeClick = TRUE;
			else
				m_pDoc->m_pInfo->bIsMoveMouseBeforeClick = FALSE;
				
			pV->m_pThreadSriptRunning = AfxBeginThread(ThreadRunningSript, (PVOID)m_pDoc->m_pInfo, THREAD_PRIORITY_NORMAL, 0,
				CREATE_SUSPENDED);
			pV->m_pThreadSriptRunning->m_bAutoDelete = false;				
			pV->m_pThreadSriptRunning->ResumeThread();
		}
		else
		{
			DWORD dwExitCode;
			::GetExitCodeThread (pV->m_pThreadSriptRunning->m_hThread, &dwExitCode);
			if (dwExitCode == STILL_ACTIVE) 
			{
				pV->m_pThreadSriptRunning->ResumeThread();
			}
			else
			{
				delete pV->m_pThreadSriptRunning;
				pV->m_pThreadSriptRunning = 0;
				MyUseDLL();
				if(m_pDoc->m_pInfo)
				{
					delete m_pDoc->m_pInfo;
					m_pDoc->m_pInfo = 0;
				}
				m_pDoc->m_pInfo= new THREADINFO;
				m_pDoc->m_pInfo->hWnd = pV->m_hWnd;
				m_pDoc->m_pInfo->m_listWindowAction = &(m_pDoc->m_listWindowAction);
				if(pV->IsDlgButtonChecked(chbSetActive))
					m_pDoc->m_pInfo->bIsSetActive = TRUE;
				else
					m_pDoc->m_pInfo->bIsSetActive = FALSE;
				if(pV->IsDlgButtonChecked(chbMoveMouseBeforClick))
				m_pDoc->m_pInfo->bIsMoveMouseBeforeClick = TRUE;
			else
				m_pDoc->m_pInfo->bIsMoveMouseBeforeClick = FALSE;
							
				pV->m_pThreadSriptRunning = AfxBeginThread(ThreadRunningSript, (PVOID)m_pDoc->m_pInfo, THREAD_PRIORITY_NORMAL, 0,
					CREATE_SUSPENDED);
				pV->m_pThreadSriptRunning->m_bAutoDelete = false;				
				pV->m_pThreadSriptRunning->ResumeThread();
			}
		}
		
		break;
	default:
		break;
	}
	return;
}
