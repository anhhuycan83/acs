// KeyWindowAction.h: interface for the CKeyWindowAction class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KEYWindowAction_H__1D95DA39_2EB8_4B54_A976_F473F52820F0__INCLUDED_)
#define AFX_KEYWindowAction_H__1D95DA39_2EB8_4B54_A976_F473F52820F0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WindowAction.h"

class CKeyWindowAction : public CWindowAction 
{
public:
	DECLARE_SERIAL(CKeyWindowAction);
	CKeyWindowAction();
	virtual ~CKeyWindowAction();	
	virtual void Serialize(CArchive& ar);
public:	
	//CString m_strToEnter;
	CTypedPtrArray<CObArray, CKeyPress*> m_listKeyPress;
};

#endif // !defined(AFX_KEYWindowAction_H__1D95DA39_2EB8_4B54_A976_F473F52820F0__INCLUDED_)
