// FunctionHelper.h: interface for the CFunctionHelper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FUNCTIONHELPER_H__473D9A36_D429_44B1_A1DF_1A79827E1E99__INCLUDED_)
#define AFX_FUNCTIONHELPER_H__473D9A36_D429_44B1_A1DF_1A79827E1E99__INCLUDED_

//#include "WindowAction.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxcoll.h>

//begin define for using switch with cstring
#define STR_SWITCH(str)  {TCHAR* __ps = (TCHAR*)((const TCHAR*)str);while(1) {

#define STR_SWITCH_END()  break; } }

#define STR_CASE(str) if(0 == _tcsicmp(__ps,((const TCHAR*)str)))

#define STR_CASE_EXACT(str)  if( 0 == _tcscmp( __ps,((const TCHAR*)str) ) )

#define DEFAULT_CASE()
//end define for using switch with cstring
///////////////section for all enum type declaration
enum eKindOfClick {LeftClick, RightClick, MiddleClick, ScrollUp, ScrollDown};
//key press
enum eKindOfChar {NormalChar, FunctionChar};

enum eKindOfWindowAction {MouseWindowAction, KeyWindowAction};

extern enum ePlayer {
	   PRUN = 0,
       PSTOP = 1,       
       PPAUSE = 2,
	   PRECORD = 3,
    };

extern enum eFrequency {
	ONCE,
		DAILY,
		WEEKLY,
		MONTHLY
};
//struc for LPARAM 
union KeyState
{
    LPARAM lparam;

    struct
    {
        unsigned nRepeatCount : 16;
        unsigned nScanCode : 8;
        unsigned nExtended : 1;
        unsigned nReserved : 4;
        unsigned nContext : 1;
        unsigned nPrev : 1;
        unsigned nTrans : 1;
    };
};
//Bits    Meaning
//0-15    The repeat count for the current message. The value is the number of times the keystroke is autorepeated as a result of the user holding down the key. If the keystroke is held long enough, multiple messages are sent. However, the repeat count is not cumulative.
//16-23   The scan code. The value depends on the OEM.
//24  Indicates whether the key is an extended key, such as the right-hand ALT and CTRL keys that appear on an enhanced 101- or 102-key keyboard. The value is 1 if it is an extended key; otherwise, it is 0.
//25-28   Reserved; do not use.
//29  The context code. The value is always 0 for a WM_KEYDOWN message.
//30  The previous key state. The value is 1 if the key is down before the message is sent, or it is zero if the key is up.
//31  The transition state. The value is always 0 for a WM_KEYDOWN message.

///////////////end section for all enum type declaration

class CWindowInfor
{
public:
	HWND m_hWnd;
	CString m_caption;//title
	CString m_className;
	long m_calssStyle;
//	RECT m_rect;
//	ULONG m_procId;

	CWindowInfor();
	virtual ~CWindowInfor();
};

class CFunctionHelper  
{
public:
	static int MyGetDaysInMonth(int month, int year);
	static COleDateTime GetPresentDT(COleDateTime dtInput, eFrequency eFre);
	static double ConvertCStringToDouble(CString strDouble);
	static COleDateTime GetOleDateTime(CDateTimeCtrl* dateTimeCtrl);
	static CString GetFrequencyString(eFrequency eFre);
	static eFrequency GetFrequencyEnum(CString strFre);
	static bool GetFileVersion(CString& sFileVersion);
	static HWND GetHwndFocus(HWND hwnd);
	static void MyGetLastError(LPTSTR lpszFunction);
	static int GetNumDayEncodedString(CString strNumDayEncoded);
	static COleDateTime GetDateFromDateEncodedString(CString strDateEncoded);
	static CString GetWindowsDirectory();
	static CString GetCurrentDirectory();
	static CString GetSystemDirectory();
	static void CheckLicense(BOOL& bRs, COleDateTime &dtStarDate, COleDateTime &dtEndDate, CString sLicense);
	static CString CreateLicenseNotEncode(CString ActiveCode, COleDateTime starDate, int numDay);
//MYDEL	static CString CreateLicenseToClient(CString ActiveCode, COleDateTime starDate, int numDay);
	static CString CreateActiveCodeFromHddSerial();
	static unsigned int CreateMD5Server(CString sAC, CString sStartDate, CString sNumDay);
	static CString LicenseToActualMD5Server(CString sLicense);
	static CString DecodeCharToNum(CString strCharToConvert);
	static unsigned int EncodeMD5Server(unsigned int uMD5Server);
	static unsigned int ConvertMD5ToIntMD5(CString strMD5);
	static CString EncodeNumToChar(CString strNumToConvert);
	static unsigned int ConcertStringToIntEx(CString strToConvert, byte controlCode);
	static unsigned int ConcertStringToInt(CString strToConvert);
	static void CloseConsole();
	static int InitConsoleWindow();
	//static BOOL GetTopWindowFromTop(CWindowInfor* wIF, HWND hWnd);
	static BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam);
	static BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam);
	void static SetWindowInfor(CWindowInfor* wIF, HWND hWnd);
	void StringSplit(CString &str, CStringArray &arr, TCHAR chDelimitior);
	static eKindOfClick StringToEnumKindOfClick(CString s);
	static CString EnumToStringKindOfClick(eKindOfClick ins_enum);

	static eKindOfChar StringToEnumKindOfChar(CString s);
	static CString EnumToStringKindOfChar(eKindOfChar ins_enum);
	static eKindOfWindowAction StringToEnumKindOfWindowAction(CString s);
	static CString EnumToStringKindOfWindowAction(eKindOfWindowAction ins_enum);
	static CDocument* GetActiveDocument();
	static CView* GetActiveView();
	static CString  GetProgramDir();
	__int64 static hex_string_to_long(CString hexStg);
	void static convert_long_to_hex(CString &str, __int64 num);
	CFunctionHelper();
	virtual ~CFunctionHelper();
};



#endif // !defined(AFX_FUNCTIONHELPER_H__473D9A36_D429_44B1_A1DF_1A79827E1E99__INCLUDED_)
