// KeyManager.cpp: implementation of the CKeyManager class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "KeyManager.h"
#include "MainFrm.h"
#include "ACSView.h"
#include "Win32Error.h"

//#include <tlhelp32.h> // for list all thread of specify proccess id

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeyManager::CKeyManager()
{	
	m_hWndOld = NULL;
	m_pkeyWindowAction = NULL;
}

CKeyManager::~CKeyManager()
{
	
}

void CKeyManager::SetPoiterDocument(CACSDoc *pDoc)
{
	m_pDoc = pDoc;
}

void CKeyManager::OnKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//down or up
{
	//	RECT		rect;              // Rectangle area of the found window.
	//TCHAR		szText[256];
	TCHAR		szBuff[100];
	long		lRet = 0;
	CKeyPress* pKeyPress;
	
	bool bIsGetKey = false;
	//	int nVirtKey;
	//	CMainFrame* pMainFrame;
	CString s;
	//	MSG *msg;
	//to hold the character passed in the MSG structure's wParam
	//	int asciiCode;
	switch (wParam) 
	{ 
	case VK_NUMLOCK:
		//case VK_MENU:
	case VK_CONTROL:
		//case VK_CAPITAL:
	case VK_SHIFT:
		bIsGetKey = false;
		break;
		//			case VK_LSHIFT:
		//			case VK_RSHIFT:
		//	case VK_BACK:  // backspace 
		//	case 0x0A:  // linefeed 
		//	case VK_ESCAPE:  // escape 
		//		MessageBeep((UINT) -1); 
		//		//return 0; 
		//		
		//		
		//	case VK_TAB:  // tab 
		//		
		//		// Convert tabs to four consecutive spaces.
		//		
		//		//				for (i = 0; i < 4; i++) 
		//		//					SendMessage(hwndMain, WM_CHAR, 0x20, 0); 
		//		//				return 0; 
		//		
		//	case VK_RETURN:  // carriage return 0x0D
		
		// Record the carriage return and position the 
		// caret at the beginning of the new line.
		
		//				pchInputBuf[cch++] = 0x0D; 
		//				nCaretPosX = 0; 
		//				nCaretPosY += 1; 
		//		asciiCode = MapVirtualKey(wParam, 2);//char
		//		pKeyPress = new CKeyPress();
		//		pKeyPress->m_asciiCode = asciiCode;
		//		pKeyPress->m_eKindOfChar = FunctionChar;
		
		
	case WM_KEYDOWN:
		
		break;
	default:// displayable character  (normal chars)
		bIsGetKey = true;			
		pKeyPress = GetKeyPress(wParam, lParam);
		TRACE(_T("GetKeyPress: %d"), pKeyPress->m_eKindOfChar);
		break;
	}//end switch
	
	
	if(!bIsGetKey)
		return;
	//check whether this handle has a child wich has currently keyboard focus status
	HWND hFocus = CFunctionHelper::GetHwndFocus(hWnd);
	if(hFocus)
		hWnd = hFocus;
	else
		return;
	//now get last hwn of current list window action
	CWindowAction* pLastWA = m_pDoc->GetLastWindowAction();
	//if last handle is kind of mouse click => generate new key object
	if(pLastWA)
	{
		if(pLastWA->m_eKindOfWindowAction == MouseWindowAction)
			m_hWndOld = NULL;
		else
			m_hWndOld = pLastWA->m_hWnd;//if last is mouse object => add event to it
	}
	else
		m_hWndOld = NULL;
	if(hWnd != m_hWndOld)
	{
		//save time for new key press item
		SYSTEMTIME st;
	    GetSystemTime(&st);
		m_timePre = st;
		//m_hWndNew = hWnd;
		//Get handle
		m_pkeyWindowAction = new CKeyWindowAction();
		m_pkeyWindowAction->m_hWnd = hWnd;
		
		// Get the screen coordinates of the rectangle of the found window.
		//GetWindowRect (hWnd, &rect);
		
		// Get the class name of the found window.
		lRet = ::GetClassName(hWnd, szBuff, sizeof (szBuff) - 1);
		m_pkeyWindowAction->m_className = szBuff;
		//Get the text (caption, title) of handle window. it can not copy text on control
		GetWindowText(hWnd, szBuff, sizeof (szBuff) - 1);
		m_pkeyWindowAction->m_caption = szBuff;
		//GWL_STYLE Get style of window
		m_pkeyWindowAction->m_calssStyle = GetWindowLong(hWnd, GWL_STYLE);
		//Get proccess ID
		
		//at final, check the GA_ROOTOWNER of this window
		HWND hGA_PARENT;
		
		if(m_pkeyWindowAction->m_calssStyle == -1803550720)//top window
			hGA_PARENT = 0;
		else
			hGA_PARENT = ::GetAncestor(hWnd, GA_PARENT);
		static long hDesktop = (long)m_pDoc->m_hDesktop;
		switch((long)hGA_PARENT)
		{
		case 0:
			m_pkeyWindowAction->m_parentWI.m_hWnd = NULL;
			break;
		default:
			if(hGA_PARENT == m_pDoc->m_hDesktop)
				m_pkeyWindowAction->m_parentWI.m_hWnd = NULL;
			else
			{
				HWND hGA_ROOT;//hGA_ROOTOWNER, 
				//MYDEL			hGA_ROOTOWNER = ::GetAncestor(hWnd, GA_ROOTOWNER);
				hGA_ROOT = ::GetAncestor(hWnd, GA_ROOT);
				m_pkeyWindowAction->m_parentWI.m_hWnd = hGA_ROOT;
				//and get information for this top window
				CFunctionHelper::SetWindowInfor(&m_pkeyWindowAction->m_parentWI, hGA_ROOT);
			}
			break;
		}
		//save this handle window
		m_hWndOld = hWnd;
		m_pkeyWindowAction->m_listKeyPress.Add(pKeyPress);
		m_pDoc->AddWindowAction(m_pkeyWindowAction);
	}
	else//hwnd not change
	{
		//now the item is next key item => caculate second for it
		SYSTEMTIME st;
	    GetSystemTime(&st);
		CHighTime timeNow = st;
		CHighTimeSpan ts = timeNow - m_timePre;
		pKeyPress->m_milisecond = (int)ts.GetTotalMilliSeconds();
		m_timePre = timeNow;

		m_pkeyWindowAction->m_listKeyPress.Add(pKeyPress);
		m_pDoc->RefreshItemToListReport(m_pkeyWindowAction);
	}  
	   //m_keyWindowAction
}



CKeyPress* CKeyManager::GetKeyPress(WPARAM wParam, LPARAM lParam)
{
	bool bIsNormalKey = false;
	CKeyPress*  pKeyPress = new CKeyPress();
	pKeyPress->m_eKindOfChar = NormalChar;
//	char buffer[100]; 
	
	
	//M� scan cu?a ph�m
	CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
	TCHAR keyName[30];
	CString s,s2;
	int scanCode, asciiCode;
	scanCode = MapVirtualKey(wParam, 0);
	asciiCode = MapVirtualKey(wParam, 2);//char
	s2 = (TCHAR)asciiCode;
	SHORT statusCapLock, statusShift;
	
	if(s2 != "")
	{
		if(asciiCode != 13)
			if(asciiCode != 8)
				bIsNormalKey = true;
	}
	else
		bIsNormalKey = false;
	
	//if(s2 != "" && asciiCode != 13)//not a special character
	if(bIsNormalKey)
	{
		//bIsNormalKey = true;
		//statusCapLock = GetAsyncKeyState(VK_CAPITAL) & 0x8000;//this is high order bit used for non toggle key
		statusCapLock = GetAsyncKeyState(VK_CAPITAL) & 0x1;//this is low order bit used for toggle key
		if(!statusCapLock)//cap lock off
		{
			statusShift = GetAsyncKeyState(VK_SHIFT) ;
			statusShift = statusShift & 0x8000;
			if(statusShift)//VK_LSHIFT|VK_RSHIFT|VK_CAPITAL
			{
				//filter character with shift key press
				switch(asciiCode)
				{
				case 48:// number 0 => convert to char )
					asciiCode = 41;
					break;
				case 49:// number 1 => convert to char !
					asciiCode = 33;
					break;
				case 50:// number 2 => convert to char )
					asciiCode = 64;
					break;
				case 51:// number 3 => convert to char )
					asciiCode = 35;
					break;
				case 52:// number 4 => convert to char )
					asciiCode = 36;
					break;
				case 53:// number 5 => convert to char )
					asciiCode = 37;
					break;
				case 54:// number 6 => convert to char )
					asciiCode = 94;
					break;
				case 55:// number 7 => convert to char )
					asciiCode = 38;
					break;
				case 56:// number 8 => convert to char )
					asciiCode = 42;
					break;
				case 57:// number 9 => convert to char )
					asciiCode = 40;
					break;
				case 96:// char: ` => convert to char ~
					asciiCode = 126;
					break;
				case 45:// char: - => convert to char _
					asciiCode = 95;
					break;
				case 61:// char: = => convert to char +
					asciiCode = 43;
					break;
				case 91:// char [  => convert to char {
					asciiCode = 123;
					break;
				case 93:// char ] => convert to char }
					asciiCode = 125;
					break;
				case 59:// char ;  => convert to char :
					asciiCode = 58;
					break;
				case 39:// number ' => convert to char "
					asciiCode = 34;
					break;
				case 44:// char , => convert to char <
					asciiCode = 60;
					break;
				case 46:// char .  => convert to char >
					asciiCode = 62;
					break;
				case 47:// char / => convert to char ?
					asciiCode = 63;
					break;							
				default:							
					break;
					
				}
				//						
			}
			else//not press shift key and cap lock is off
			{
				//check if the chars are alpha chars => to lower them
				if(asciiCode >= 65 && asciiCode <= 90)
				{
					//default char is capital
					asciiCode = tolower(asciiCode);
				}
				else
				{
					
				}
			}
		}//end cap lock is off
		else //cap lock is on
		{
			if((GetAsyncKeyState(VK_SHIFT) & 0x8000))//shift key is currently pressed
			{
				//check if the chars are alpha chars => to lower them
				if(asciiCode >= 65 && asciiCode <= 90)
				{
					//default char is capital
					asciiCode = tolower(asciiCode);
				}
				else
				{
					//filter character with shift key press
					switch(asciiCode)
					{
					case 48:// number 0 => convert to char )
						asciiCode = 41;
						break;
					case 49:// number 1 => convert to char !
						asciiCode = 33;
						break;
					case 50:// number 2 => convert to char )
						asciiCode = 64;
						break;
					case 51:// number 3 => convert to char )
						asciiCode = 35;
						break;
					case 52:// number 4 => convert to char )
						asciiCode = 36;
						break;
					case 53:// number 5 => convert to char )
						asciiCode = 37;
						break;
					case 54:// number 6 => convert to char )
						asciiCode = 94;
						break;
					case 55:// number 7 => convert to char )
						asciiCode = 38;
						break;
					case 56:// number 8 => convert to char )
						asciiCode = 42;
						break;
					case 57:// number 9 => convert to char )
						asciiCode = 40;
						break;
					case 96:// char: ` => convert to char ~
						asciiCode = 126;
						break;
					case 45:// char: - => convert to char _
						asciiCode = 95;
						break;
					case 61:// char: = => convert to char +
						asciiCode = 43;
						break;
					case 91:// char [  => convert to char {
						asciiCode = 123;
						break;
					case 93:// number ] => convert to char }
						asciiCode = 125;
						break;
					case 59:// char ;  => convert to char :
						asciiCode = 58;
						break;
					case 39:// char ' => convert to char "
						asciiCode = 34;
						break;
					case 44:// char , => convert to char <
						asciiCode = 60;
						break;
					case 46:// char .  => convert to char >
						asciiCode = 62;
						break;
					case 47:// char / => convert to char ?
						asciiCode = 63;
						break;							
					default:							
						break;								
					}
					//						
				}
				
			}//end shift key pressed
			else//not press shift key and cap lock is on
			{
				
			}
		}//end cap lock is on
		
		//				HideCaret(hwndMain); 
		pKeyPress->m_asciiCode = asciiCode;
	}
	else//special chars
	{
		//bIsNormalKey = false;
		pKeyPress->m_vkCode = wParam;
		pKeyPress->m_ks.lparam = lParam;
		
		if(!(GetAsyncKeyState(VK_NUMLOCK) & 0x8000))//check the num pad is not actived => convert num pad to correct key
		{
			switch(scanCode)
			{
			case 0x47: // (Keypad-7/Home)
			case 0x48: // (Keypad-8/UpArrow)
			case 0x49: // (Keypad-9/PgUp)
			case 0x4b: // (Keypad-4/Left)
			case 0x4d: // (Keypad-6/Right)
			case 0x4f: // (Keypad-1/End)
			case 0x50: // (Keypad-2/DownArrow)
			case 0x51: // (Keypad-3/PgDn)
			case 0x52: // (Keypad-0/Ins)
				pKeyPress->m_ks.nExtended = 1;
				lParam = pKeyPress->m_ks.lparam;
				break;
			}
		}
	}
	
	if(bIsNormalKey)
	{
		pKeyPress->m_eKindOfChar = NormalChar;//NormalChar, FunctionChar};
		pKeyPress->m_vkCode = 0;
		pKeyPress->m_ks.lparam = 0;
	}
	else
	{
		pKeyPress->m_eKindOfChar = FunctionChar;
		pKeyPress->m_asciiCode = 0;
	}
	
	///////////
	s2 = (TCHAR)asciiCode;
	//	LPARAM myParam = 28;
	//	myParam <<= 16;
	GetKeyNameText(lParam,keyName,sizeof(keyName));
	//s.Format(_T("message: %d wparam: %d, keyname: %s"), message, wParam, keyName);
	//byte keysDown[256];
	KeyState ks;
	ks.lparam = lParam;
	
	
	//	s.Format(_T("VirtualKey: %d, Hex: %x, Scan code: %d, ASCIIcode: %d,ASCIIchar: %s, Name: %s, lParam: %x"), 
	//		wParam, wParam, scanCode, asciiCode, s2, keyName, lParam);
	s.Format(_T("VirtualKey: %d, Hex: %x, Scan code: %d, ASCIIcode: %d,ASCIIchar: %s, Name: %s\r\n scancode: %d, repeat: %d, extended: %d, context: %d, preState: %d, trans: %d"), 
		wParam, wParam, ks.nScanCode, asciiCode, s2, keyName, ks.nScanCode, ks.lparam & 0xFFFF, ks.nExtended, ks.nPrev, ks.nTrans);
	
	//MYDEL			unsigned int scanCode = MapVirtualKey(lParam, 0);
	//MYDELGetKeyNameText(scanCode << 16, keyName, sizeof(keyName));
	
	//s = (TCHAR) wParam;
	pMainFrame->m_wndStatusBar.SetPaneText(1,s);
	
	CACSView* pView = (CACSView*)CFunctionHelper::GetActiveView();
	CMainTab* pMainTab = (CMainTab*)pView->GetMainTab();
	
	pMainTab->m_txtWAIFValue = s;
	pMainTab->UpdateData(false);
	//	
	//	2 notes:
	//	1) if function char => save LPARAM else => save acsciicode and asciicode now save the vk code
	//		2) char back space => is a function char, not a normal car
	return pKeyPress;
	
}
