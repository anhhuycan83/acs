// ACSDoc.cpp : implementation of the CACSDoc class
//

#include "stdafx.h"
#include "ACS.h"

#include "ACSDoc.h"
#include "ACSView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CACSDoc

IMPLEMENT_DYNCREATE(CACSDoc, CDocument)

BEGIN_MESSAGE_MAP(CACSDoc, CDocument)
	//{{AFX_MSG_MAP(CACSDoc)
	ON_COMMAND(ID_EDIT_CLEAR_ALL, OnEditClearAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR_ALL, OnUpdateEditClearAll)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CACSDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CACSDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IACS to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {6FA2C6CF-1660-4757-8552-94AF3CDEAC70}
static const IID IID_IACS =
{ 0x6fa2c6cf, 0x1660, 0x4757, { 0x85, 0x52, 0x94, 0xaf, 0x3c, 0xde, 0xac, 0x70 } };

BEGIN_INTERFACE_MAP(CACSDoc, CDocument)
	INTERFACE_PART(CACSDoc, IID_IACS, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CACSDoc construction/destruction

ePlayer m_statusScript;
ePlayer m_statusTimer;

CACSDoc::CACSDoc()
{
	// TODO: add one-time construction code here

	EnableAutomation();

	AfxOleLockApp();

	m_hDesktop = ::GetDesktopWindow();
	m_pInfo = 0;
	m_pInfoCheckingTime = 0;
}

CACSDoc::~CACSDoc()
{
	int Index = m_listWindowAction.GetSize();
	int k;
	CKeyWindowAction* pKWA;
	while(Index--)
	{		
		switch(m_listWindowAction.GetAt(Index)->m_eKindOfWindowAction)
		{
		case KeyWindowAction:
			pKWA = (CKeyWindowAction*)m_listWindowAction.GetAt(Index);
			k = pKWA->m_listKeyPress.GetSize();
			while(k--)
				delete pKWA->m_listKeyPress.GetAt(k);
			pKWA->m_listKeyPress.RemoveAll();
			delete pKWA;
			break;
		case MouseWindowAction:
			CMouseWindowAction* pMWA = (CMouseWindowAction*)m_listWindowAction.GetAt(Index);
			k = pMWA->m_listMouseClick.GetSize();
			while(k--)
				delete pMWA->m_listMouseClick.GetAt(k);
			pMWA->m_listMouseClick.RemoveAll();
			delete pMWA;
			break;
		}
	}
	m_listWindowAction.RemoveAll();//remove all poiters currently store by m_list

	//detete thread info object if it still has not been deleted by other thread
	if(AfxIsMemoryBlock(m_pInfo, sizeof(THREADINFO)))
		delete m_pInfo;
	//detete thread checking time info object if it still has not been deleted by other thread
	if(AfxIsMemoryBlock(m_pInfoCheckingTime, sizeof(THREADINFOTIMECHECKING)))
		delete m_pInfoCheckingTime;
	
	AfxOleUnlockApp();
}

BOOL CACSDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)	

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CACSDoc serialization

void CACSDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		m_listWindowAction.Serialize(ar);
	}
	else
	{
		// TODO: add loading code here
		m_listWindowAction.Serialize(ar);
		RefreshListReport();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CACSDoc diagnostics

#ifdef _DEBUG
void CACSDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CACSDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CACSDoc commands

BOOL CACSDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

CWindowAction* CACSDoc::GetWindowAction(int index)
{
	if(index >= 0 && index <= m_listWindowAction.GetUpperBound())
	{
		return m_listWindowAction.GetAt(index);
	}
	else
	{
		return NULL;
	}
}

void CACSDoc::AddWindowAction(HWND hWnd, CString strCaption, CString strClassName
									, long classStyle, RECT rect,long procId, eKindOfWindowAction eKind)
{
	CWindowAction* pWin = new CWindowAction();
	pWin->m_hWnd = hWnd;
	pWin->m_caption = strCaption;
	pWin->m_className = strClassName;
	pWin->m_calssStyle = classStyle;
	pWin->m_rect = rect;
	pWin->m_procId = procId;
	pWin->m_eKindOfWindowAction = eKind;
	m_listWindowAction.Add(pWin);
	SetModifiedFlag();
}

void CACSDoc::AddWindowAction(CWindowAction *pWindowAction)
{
//	switch(pWindowAction->m_eKindOfWindowAction)
//	{
//	case KeyWindowAction:
//		CKeyWindowAction* pKey = (CKeyWindowAction*) pWindowAction;
//		break;
//	case MouseWindowAction:
//		break;
//	default:
//		break;
//	}
	if(m_listWindowAction.GetSize() == 0)
	{
		//first item will be inserted now. => save time for the fist item
		SYSTEMTIME st;
	    GetSystemTime(&st);
		m_timePre = st;
	}
	else
	{
		//now, this item is from second to n => caculate time with the pre item
		SYSTEMTIME st;
	    GetSystemTime(&st);
		CHighTime timeNow = st;
		CHighTimeSpan ts = timeNow - m_timePre;
		pWindowAction->m_milisecond = (int)ts.GetTotalMilliSeconds();
		m_timePre = timeNow;
	}
	m_listWindowAction.Add(pWindowAction);
	SetModifiedFlag();
	//add this item to cview.m_reportlistcontrol
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	CMainTab* pDlgMain = pView->GetMainTab();

	InsertItemToListReport(&pDlgMain->m_wndList, pWindowAction);
//
//	TCHAR temp[5];
//	//pView->m
//	//pDlgMain->m_wndList.ins
//	int IDX;
//	CKeyWindowAction* pKey;
//	CMouseWindowAction* pMouse;
//	switch(pWindowAction->m_eKindOfWindowAction)
//	{
//	case KeyWindowAction:
//		pKey = (CKeyWindowAction*) pWindowAction;
//		//const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//		IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//		pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
//		pDlgMain->m_wndList.SetItemText(IDX, 1, pKey->m_caption);
//		pDlgMain->m_wndList.SetItemText(IDX, 2, pKey->m_strToEnter);
//		break;
//	case MouseWindowAction:
//		pMouse = (CMouseWindowAction*) pWindowAction;
//		//const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//		IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//		pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
//		pDlgMain->m_wndList.SetItemText(IDX, 1, pMouse->m_caption);
//		//note: must be implement the loop to get all click array of this object later
//		CString s;
//		s = CFunctionHelper::EnumToStringKindOfClick(pMouse->m_listMouseClick.GetAt(0)->m_eKindOfClick);
//		pDlgMain->m_wndList.SetItemText(IDX, 2, s);
//		break;
//	}
}

void CACSDoc::DeleteContents() 
{
	// TODO: Add your specialized code here and/or call the base class
	int Index = m_listWindowAction.GetSize();
	int k;
	CKeyWindowAction* pKWA;
	while(Index--)
	{
		switch(m_listWindowAction.GetAt(Index)->m_eKindOfWindowAction)
		{
		case KeyWindowAction:
			pKWA = (CKeyWindowAction*)m_listWindowAction.GetAt(Index);
			k = pKWA->m_listKeyPress.GetSize();
			while(k--)
				delete pKWA->m_listKeyPress.GetAt(k);
			pKWA->m_listKeyPress.RemoveAll();
//MYDEL   			pKWA->m_listKeyPress.SetSize(0);
//MYDEL   			pKWA->m_listKeyPress.FreeExtra();
			delete pKWA;
			break;
		case MouseWindowAction:
			CMouseWindowAction* pMWA = (CMouseWindowAction*)m_listWindowAction.GetAt(Index);
			k = pMWA->m_listMouseClick.GetSize();
			while(k--)
				delete pMWA->m_listMouseClick.GetAt(k);
			pMWA->m_listMouseClick.RemoveAll();
//MYDEL   			pMWA->m_listMouseClick.SetSize(0);
//MYDEL   			pMWA->m_listMouseClick.FreeExtra();
			delete pMWA;
			break;
		}
	}
	m_listWindowAction.RemoveAll();//remove all poiters currently store by m_list
//MYDEL   	m_listWindowAction.SetSize(0);
//MYDEL   	m_listWindowAction.FreeExtra();

	if(AfxGetApp()->m_pMainWnd)//check whether CView exist or not
		RefreshListReport();

	CDocument::DeleteContents();
}

void CACSDoc::OnEditClearAll() 
{
	// TODO: Add your command handler code here
	DeleteContents();	
	UpdateAllViews(0);
	SetModifiedFlag();
	
}

void CACSDoc::OnUpdateEditClearAll(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_listWindowAction.GetSize());	
}

void CACSDoc::OnEditUndo() 
{
	// TODO: Add your command handler code here
	int idx = m_listWindowAction.GetUpperBound();
	RemoveWindowActionItem(idx);
	//UpdateAllViews(0);	
	SetModifiedFlag();
	//RefreshListReport();
}

void CACSDoc::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_listWindowAction.GetSize());
}
void CACSDoc::RefreshListReport()
{
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	CMainTab* pDlgMain = pView->GetMainTab();
	pDlgMain->m_wndList.DeleteAllItems();
	//CACSDoc* pDoc = (CACSDoc*)CFunctionHelper::GetActiveView();
	
	for(int i = 0; i <	m_listWindowAction.GetSize(); i++)
	{
		InsertItemToListReport(&pDlgMain->m_wndList, m_listWindowAction.GetAt(i));
	}
}

void CACSDoc::RemoveWindowActionItem(int Index)
{
	//int Index = m_listWindowAction.GetUpperBound();
	int k;
	CKeyWindowAction* pKWA;
	CMouseWindowAction* pMWA;
	if (Index > -1)
	{
		switch(m_listWindowAction.GetAt(Index)->m_eKindOfWindowAction)
		{
		case KeyWindowAction:
			pKWA = (CKeyWindowAction*)m_listWindowAction.GetAt(Index);
			k = pKWA->m_listKeyPress.GetSize();
			while(k--)
				delete pKWA->m_listKeyPress.GetAt(k);
			pKWA->m_listKeyPress.RemoveAll();
			delete pKWA;			
			break;
		case MouseWindowAction:
			pMWA = (CMouseWindowAction*)m_listWindowAction.GetAt(Index);
			k = pMWA->m_listMouseClick.GetSize();
			while(k--)
				delete pMWA->m_listMouseClick.GetAt(k);
			pMWA->m_listMouseClick.RemoveAll();
			delete pMWA;
			break;
		}
		m_listWindowAction.RemoveAt(Index);
		//remove the last event		
		CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
		CMainTab* pDlgMain = pView->GetMainTab();
		pDlgMain->m_wndList.DeleteItem(Index);		
	}
}

void CACSDoc::InsertItemToListReport(CReportCtrl *m_wndList, CWindowAction *pWindowAction)
{
	//m_wndList.SetColumnHeader(_T("STT, 30; Class caption, 150; Action, 70; Handle, 70; Class Style, 100"));

	int IDX = -1;
	CKeyWindowAction* pKey;
	CMouseWindowAction* pMouse;
	//CString strHwnd;
	switch(pWindowAction->m_eKindOfWindowAction)
	{
	case KeyWindowAction:
		pKey = (CKeyWindowAction*) pWindowAction;
		IDX = m_wndList->InsertItem(m_wndList->GetItemCount(), _T(""));
		SetItemTextKey(m_wndList, pKey, IDX);		
		break;
	case MouseWindowAction:
		pMouse = (CMouseWindowAction*) pWindowAction;
		IDX = m_wndList->InsertItem(m_wndList->GetItemCount(), _T(""));
		SetItemTextMouse(m_wndList, pMouse, IDX);
		break;
	}
	if(IDX != -1)
		m_wndList->EnsureVisible(IDX, FALSE);
}

void CACSDoc::RefreshItemToListReport(CWindowAction *pWindowAction)
{
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	CMainTab* pDlgMain = pView->GetMainTab();
	CKeyWindowAction* pKey;
	CMouseWindowAction* pMouse;
//MYDEL   	CString strHwnd;	
//MYDEL   	strHwnd.Format(_T("%x"), pWindowAction->m_hWnd);
	//CFunctionHelper::convert_long_to_hex(strHwnd, (ULONG)pWindowAction->m_hWnd);
//MYDEL   	for(int i = pDlgMain->m_wndList.GetItemCount() - 1; i>=0; i--)
//MYDEL   	{
//MYDEL   		if( strHwnd == pDlgMain->m_wndList.GetItemText(i, 4))
//MYDEL   		{
//MYDEL   			switch(pWindowAction->m_eKindOfWindowAction)
//MYDEL   			{
//MYDEL   			case KeyWindowAction:
//MYDEL   				pKey = (CKeyWindowAction*) pWindowAction;
//MYDEL   				SetItemTextKey(&pDlgMain->m_wndList, pKey, i);
//MYDEL   				break;
//MYDEL   			case MouseWindowAction:
//MYDEL   				pMouse = (CMouseWindowAction*) pWindowAction;
//MYDEL   				SetItemTextMouse(&pDlgMain->m_wndList, pMouse, i);
//MYDEL   				break;
//MYDEL   			}
//MYDEL   			break;//for loop
//MYDEL   			
//MYDEL   		}
//MYDEL   	}

	CWindowAction* pWA;
	for(int i = m_listWindowAction.GetUpperBound(); i>=0; i--)
	{
		pWA = m_listWindowAction.GetAt(i);
		if(pWindowAction->m_hWnd == pWA->m_hWnd)
		{
			switch(pWindowAction->m_eKindOfWindowAction)
			{
			case KeyWindowAction:
				pKey = (CKeyWindowAction*) pWindowAction;
				SetItemTextKey(&pDlgMain->m_wndList, pKey, i);
				break;
			case MouseWindowAction:
				pMouse = (CMouseWindowAction*) pWindowAction;
				SetItemTextMouse(&pDlgMain->m_wndList, pMouse, i);
				break;
			}
			break;//for loop
		}
	}
}

void CACSDoc::SetItemTextKey(CReportCtrl *m_wndList, CKeyWindowAction *pKey, int IDX)
{
//MYDEL   	//_T("STT, 30; Class caption, 100; Class Name, 100; Class Style, 50, Handle, 50; Action, 80; ; xy, 200"));
//MYDEL   	TCHAR temp[5];
//MYDEL   	CString strHwnd, strClassStyle, s;
//MYDEL   	m_wndList->SetItemText(IDX, 0, _itow(m_wndList->GetItemCount(), temp, 10));
//MYDEL   	m_wndList->SetItemText(IDX, 1, pKey->m_caption);
//MYDEL   	m_wndList->SetItemText(IDX, 2, pKey->m_className);
//MYDEL   	strClassStyle.Format(_T("%x"), pKey->m_calssStyle);
//MYDEL   	m_wndList->SetItemText(IDX, 3, strClassStyle);
//MYDEL   	strHwnd.Format(_T("%x"), pKey->m_hWnd);
//MYDEL   	m_wndList->SetItemText(IDX, 4, strHwnd);
//MYDEL   	m_wndList->SetItemText(IDX, 5, _T("Key press"));
//MYDEL   	//m_wndList->SetItemText(IDX, 6, CFunctionHelper.hex_string_to_long(pKey->m_hWnd));
//MYDEL   	CKeyPress* pKeyPress;
//MYDEL   	strHwnd.Empty();
//MYDEL   	s.Empty();
//MYDEL   //MYDEL	LPARAM myParam;
//MYDEL   //MYDEL	int scanCode;
//MYDEL   	TCHAR keyName[30];
//MYDEL   	for(int i = 0; i<pKey->m_listKeyPress.GetSize(); i++)
//MYDEL   	{
//MYDEL   		pKeyPress = pKey->m_listKeyPress.GetAt(i);
//MYDEL   		if(pKeyPress->m_eKindOfChar == NormalChar)
//MYDEL   		{
//MYDEL   			s = (TCHAR)(pKeyPress->m_asciiCode);
//MYDEL   		}
//MYDEL   		else
//MYDEL   		{
//MYDEL   			GetKeyNameText(pKeyPress->m_ks.lparam,keyName,sizeof(keyName));
//MYDEL   			s = keyName;
//MYDEL   		}
//MYDEL   		strHwnd += s + _T(";");
//MYDEL   	}
//MYDEL   	m_wndList->SetItemText(IDX, 6, strHwnd);

	//_T("STT, 30; Class caption, 100; Class Name, 100; Class Style, 50, Handle, 50; Action, 80; ; xy, 200"));	
	
	//TCHAR temp[5];
	CString strHwnd, strClassStyle, s;
	s.Format(_T("%d"), m_wndList->GetItemCount());
	//m_wndList->SetItemText(IDX, 0, _itow(m_wndList->GetItemCount(), temp, 10));
	m_wndList->SetItemText(IDX, 0, s);
//MYDEL   	m_wndList->SetItemText(IDX, 1, pKey->m_caption);
//MYDEL   	m_wndList->SetItemText(IDX, 2, pKey->m_className);
//MYDEL   	strClassStyle.Format(_T("%x"), pKey->m_calssStyle);
//MYDEL   	m_wndList->SetItemText(IDX, 3, strClassStyle);
//MYDEL   	strHwnd.Format(_T("%x"), pKey->m_hWnd);
//MYDEL   	m_wndList->SetItemText(IDX, 4, strHwnd);
	m_wndList->SetItemText(IDX, 1, _T("Key press"));
	//m_wndList->SetItemText(IDX, 6, CFunctionHelper.hex_string_to_long(pKey->m_hWnd));
	CKeyPress* pKeyPress;
	strHwnd.Empty();
	s.Empty();
//MYDEL	LPARAM myParam;
//MYDEL	int scanCode;
	TCHAR keyName[30];
	for(int i = 0; i<pKey->m_listKeyPress.GetSize(); i++)
	{
		pKeyPress = pKey->m_listKeyPress.GetAt(i);
		if(pKeyPress->m_eKindOfChar == NormalChar)
		{
			s = (TCHAR)(pKeyPress->m_asciiCode);
		}
		else
		{
			GetKeyNameText(pKeyPress->m_ks.lparam,keyName,sizeof(keyName));
			s = keyName;
		}
		strHwnd += s + _T(";");
	}
	m_wndList->SetItemText(IDX, 2, strHwnd);
}

void CACSDoc::SetItemTextMouse(CReportCtrl *m_wndList, CMouseWindowAction *pMouse, int IDX)
{
//MYDEL   	TCHAR temp[5];
//MYDEL   	CString strHwnd, strClassStyle, s;
//MYDEL   	m_wndList->SetItemText(IDX, 0, _itow(m_wndList->GetItemCount(), temp, 10));
//MYDEL   	m_wndList->SetItemText(IDX, 1, pMouse->m_caption);
//MYDEL   	m_wndList->SetItemText(IDX, 2, pMouse->m_className);
//MYDEL   	strClassStyle.Format(_T("%x"), pMouse->m_calssStyle);
//MYDEL   	m_wndList->SetItemText(IDX, 3, strClassStyle);
//MYDEL   	strHwnd.Format(_T("%x"), pMouse->m_hWnd);
//MYDEL   	m_wndList->SetItemText(IDX, 4, strHwnd);
//MYDEL   	s = CFunctionHelper::EnumToStringKindOfClick(pMouse->m_listMouseClick.GetAt(0)->m_eKindOfClick);
//MYDEL   	m_wndList->SetItemText(IDX, 5, s);
//MYDEL   
//MYDEL   	CMouseClick* mc;
//MYDEL   	strHwnd.Empty();
//MYDEL   	s.Empty();
//MYDEL   	for(int i = 0; i<pMouse->m_listMouseClick.GetSize(); i++)
//MYDEL   	{
//MYDEL   		mc = pMouse->m_listMouseClick.GetAt(i);
//MYDEL   		s.Format(_T("[%d %d]"), mc->m_poiterClick.x, mc->m_poiterClick.y);
//MYDEL   		strHwnd += s + _T(";");
//MYDEL   	}
//MYDEL   	m_wndList->SetItemText(IDX, 6, strHwnd);

	//TCHAR temp[5];
	CString strHwnd, strClassStyle, s;
	s.Format(_T("%d"), m_wndList->GetItemCount());
	//m_wndList->SetItemText(IDX, 0, _itow(m_wndList->GetItemCount(), temp, 10));	
	m_wndList->SetItemText(IDX, 0, s);
//MYDEL   	m_wndList->SetItemText(IDX, 1, pMouse->m_caption);
//MYDEL   	m_wndList->SetItemText(IDX, 2, pMouse->m_className);
//MYDEL   	strClassStyle.Format(_T("%x"), pMouse->m_calssStyle);
//MYDEL   	m_wndList->SetItemText(IDX, 3, strClassStyle);
//MYDEL   	strHwnd.Format(_T("%x"), pMouse->m_hWnd);
//MYDEL   	m_wndList->SetItemText(IDX, 4, strHwnd);
	//s = CFunctionHelper::EnumToStringKindOfClick(pMouse->m_listMouseClick.GetAt(0)->m_eKindOfClick);
	m_wndList->SetItemText(IDX, 1, _T("Mouse Click"));

	CMouseClick* mc;
	strHwnd.Empty();
	s.Empty();
	for(int i = 0; i<pMouse->m_listMouseClick.GetSize(); i++)
	{
		mc = pMouse->m_listMouseClick.GetAt(i);
		s.Format(_T("%s[%d %d]"), 
			CFunctionHelper::EnumToStringKindOfClick(pMouse->m_listMouseClick.GetAt(i)->m_eKindOfClick),
			mc->m_poiterClick.x, mc->m_poiterClick.y);
		strHwnd += s + _T(";");
	}
	m_wndList->SetItemText(IDX, 2, strHwnd);
}


HWND CACSDoc::GetLastHwnd()
{
	HWND hRs = 0;
	int idx = m_listWindowAction.GetUpperBound();
	if(idx >= 0)
		hRs = m_listWindowAction.GetAt(idx)->m_hWnd;

	return hRs;
}



CWindowAction* CACSDoc::GetLastWindowAction()
{
	CWindowAction* pWA = NULL;
	int idx = m_listWindowAction.GetUpperBound();
	if(idx >= 0)
		pWA = m_listWindowAction.GetAt(idx);

	return pWA;
}

void CACSDoc::RefreshThreadTimeChecking()
{

}
