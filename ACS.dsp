# Microsoft Developer Studio Project File - Name="ACS" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ACS - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ACS.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ACS.mak" CFG="ACS - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ACS - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "ACS - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ACS - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "ACS - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_UNICODE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ACS - Win32 Release"
# Name "ACS - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ACS.cpp
# End Source File
# Begin Source File

SOURCE=.\ACS.odl
# End Source File
# Begin Source File

SOURCE=.\ACS.rc
# End Source File
# Begin Source File

SOURCE=.\ACSDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ACSView.cpp
# End Source File
# Begin Source File

SOURCE=.\BsDocTemplate.cpp
# End Source File
# Begin Source File

SOURCE=.\DateTimeFormat.cpp
# End Source File
# Begin Source File

SOURCE=.\diskid32.cpp
# End Source File
# Begin Source File

SOURCE=.\FileVersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\HighTime.cpp
# End Source File
# Begin Source File

SOURCE=.\KeyManager.cpp
# End Source File
# Begin Source File

SOURCE=.\KeyWindowAction.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MainTab.cpp
# End Source File
# Begin Source File

SOURCE=.\md5.cpp
# End Source File
# Begin Source File

SOURCE=.\MouseManager.cpp
# End Source File
# Begin Source File

SOURCE=.\MouseWindowAction.cpp
# End Source File
# Begin Source File

SOURCE=.\MyTabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\OptionTab.cpp
# End Source File
# Begin Source File

SOURCE=.\RecordNoteDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ScriptManager.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\textfile.cpp
# End Source File
# Begin Source File

SOURCE=.\WindowAction.cpp
# End Source File
# Begin Source File

SOURCE=.\WinStartup.cpp
# End Source File
# Begin Source File

SOURCE=.\WndPosManager.cpp
# End Source File
# Begin Source File

SOURCE=.\XMLSettings.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ACS.h
# End Source File
# Begin Source File

SOURCE=.\ACSDoc.h
# End Source File
# Begin Source File

SOURCE=.\ACSView.h
# End Source File
# Begin Source File

SOURCE=.\BsDocTemplate.h
# End Source File
# Begin Source File

SOURCE=.\DateTimeFormat.h
# End Source File
# Begin Source File

SOURCE=.\diskid32.h
# End Source File
# Begin Source File

SOURCE=.\FileVersionInfo.h
# End Source File
# Begin Source File

SOURCE=.\FunctionHelper.h
# End Source File
# Begin Source File

SOURCE=.\HighTime.h
# End Source File
# Begin Source File

SOURCE=.\KeyManager.h
# End Source File
# Begin Source File

SOURCE=.\KeyWindowAction.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MainTab.h
# End Source File
# Begin Source File

SOURCE=.\md5.h
# End Source File
# Begin Source File

SOURCE=.\MouseManager.h
# End Source File
# Begin Source File

SOURCE=.\MouseWindowAction.h
# End Source File
# Begin Source File

SOURCE=.\MyTabCtrl.h
# End Source File
# Begin Source File

SOURCE=.\OptionTab.h
# End Source File
# Begin Source File

SOURCE=.\RecordNoteDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportCtrl.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ScriptManager.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\textfile.h
# End Source File
# Begin Source File

SOURCE=.\WindowAction.h
# End Source File
# Begin Source File

SOURCE=.\WinStartup.h
# End Source File
# Begin Source File

SOURCE=.\WndPosManager.h
# End Source File
# Begin Source File

SOURCE=.\XMLSettings.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\ACS.ico
# End Source File
# Begin Source File

SOURCE=.\res\ACS.rc2
# End Source File
# Begin Source File

SOURCE=.\res\ACSDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ACS.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section ACS : {0F62FEB2-98B7-40B0-8605-AE45560BF0BD}
# 	1:21:IDD_POPUP_RECORD_NOTE:135
# 	2:16:Resource Include:resource.h
# 	2:21:IDD_POPUP_RECORD_NOTE:IDD_POPUP_RECORD_NOTE
# 	2:17:RecordNoteDlg.cpp:RecordNoteDlg.cpp
# 	2:10:ENUM: enum:enum
# 	2:15:RecordNoteDlg.h:RecordNoteDlg.h
# 	2:21:CLASS: CRecordNoteDlg:CRecordNoteDlg
# 	2:19:Application Include:acs.h
# End Section
