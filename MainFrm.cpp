// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "ACS.h"
//#include <vld.h>
#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, baseCMainFrame)

BEGIN_MESSAGE_MAP(CMainFrame, baseCMainFrame)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_MYSTATUS, OnUpdateIndicatorMyStatus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_MYSTATUS,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	
	if (baseCMainFrame::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	//set mystatus pan width
	//m_wndStatusBar.SetPaneInfo(1,ID_INDICATOR_MYSTATUS, SBPS_NORMAL,200);
	m_wndStatusBar.SetPaneInfo(0,ID_SEPARATOR, SBPS_NORMAL,100); 
	m_wndStatusBar.SetPaneInfo(1,ID_INDICATOR_MYSTATUS, SBPS_STRETCH,100); 

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !baseCMainFrame::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	//cs.cx = cs.cy = 1200;

	//MYDEL   	// Sample 01: Hide the document title
//MYDEL   	cs.style = cs.style & (~ FWS_ADDTOTITLE );
//MYDEL   	
//MYDEL   	//Sample 02: To remove Maximize and Minimize box of the window
//MYDEL   	cs.style = cs.style & ( ~(WS_MAXIMIZEBOX | WS_MINIMIZEBOX ));

	//Sample 02: To remove Maximize box of the window
	cs.style = cs.style & ( ~WS_MAXIMIZEBOX);

	//Sample 03: Avoid resizing the document. Make it fixed size
	cs.style = cs.style & ~WS_THICKFRAME;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	baseCMainFrame::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	baseCMainFrame::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

//DEL LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
//DEL {
//DEL 	// TODO: Add your specialized code here and/or call the base class
//DEL 	// TODO: Add your specialized code here and/or call the base class
//DEL 	// Open window when double click to the Systray Icon
//DEL     if(message == MYWM_NOTIFYICON){
//DEL 		CACSView * pView = (CACSView *)CFunctionHelper::GetActiveView();
//DEL         switch (lParam){
//DEL 
//DEL             case WM_LBUTTONDBLCLK: 
//DEL                 switch (wParam) {
//DEL                     case IDR_MAINFRAME:
//DEL 						//m_hided = false;
//DEL                         //ShowWindow(SW_NORMAL);
//DEL 						pView->TrayMessage(NIM_DELETE);
//DEL 						pView->myShowDlg(true);
//DEL                         SetForegroundWindow();
//DEL                         SetFocus();
//DEL                         return TRUE; 
//DEL                         break; 
//DEL                 } 
//DEL                 break; //showContextNotMenu
//DEL 
//DEL 			case WM_RBUTTONUP:
//DEL                 switch (wParam) {
//DEL                     case IDR_MAINFRAME:
//DEL 						pView->myShowContextNotMenu();
//DEL                         return TRUE; 
//DEL                         break; 
//DEL                 } 
//DEL                 break; //showContextNotMenu
//DEL         } 
//DEL     }//MYWM_KEYPRESS
//DEL 	if(message == MYWM_KEYPRESS_F10)
//DEL 	{
//DEL 		//AfxMessageBox(_T("good"));
//DEL 	
//DEL 		CACSApp * pApp = (CACSApp*)AfxGetApp();
//DEL 		if(pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F10")) == _T("F10"))
//DEL 		{
//DEL 			//show
//DEL 			//MessageBox(_T("F10"));
//DEL 			if(!pView->m_visible)
//DEL 			{
//DEL 				pView->myShowDlg(true);
//DEL 			}
//DEL 		}
//DEL 	}
//DEL 	else
//DEL 	{
//DEL 		if(message == MYWM_KEYPRESS_F11)
//DEL 		{				
//DEL 			CACSApp * pApp = (CACSApp*)AfxGetApp();
//DEL 			if(pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F11")) == _T("F11"))
//DEL 			{
//DEL 				//show
//DEL 				//MessageBox(_T("F11"));
//DEL 				if(!pView->m_visible)
//DEL 				{
//DEL 					pView->myShowDlg(true);
//DEL 				}
//DEL 			}
//DEL 		}
//DEL 	}
//DEL 	return baseCMainFrame::WindowProc(message, wParam, lParam);
//DEL }

void CMainFrame::OnUpdateIndicatorMyStatus(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ();
}
