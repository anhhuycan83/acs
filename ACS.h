// ACS.h : main header file for the ACS application
//

#if !defined(AFX_ACS_H__B0AD862B_D92F_45EF_8EC2_76BAC2672069__INCLUDED_)
#define AFX_ACS_H__B0AD862B_D92F_45EF_8EC2_76BAC2672069__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "XMLSettings.h"	// Added by ClassView


//globle variables
extern	BOOL m_bIsRegistered;
extern	int m_maxNumRunAction;
extern	BOOL m_bIsRepeat;
extern 	BOOL m_isHaveCurrentTimeCheking;
extern 	COleDateTime m_timeChecking;

/////////////////////////////////////////////////////////////////////////////
// CACSApp:
// See ACS.cpp for the implementation of this class
//

class CACSApp : public CWinApp
{
private:
	HANDLE m_hMutex;
public:	
	CString GetFileVersion();
	CString m_activeCode;
	COleDateTime m_endDate;
	COleDateTime m_startDate;
	void CheckLicense(CString sLicense);
	CString m_licenseKey;
	CXMLSettings m_xmlSetting;
	CACSApp();
	~CACSApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CACSApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CACSApp)
	afx_msg void OnAppAbout();
	afx_msg void OnHelpIndex();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACS_H__B0AD862B_D92F_45EF_8EC2_76BAC2672069__INCLUDED_)
