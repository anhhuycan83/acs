// MainTab.cpp : implementation file
//

#include "stdafx.h"
#include "ACS.h"
#include "MainTab.h"
#include "FunctionHelper.h"
#include "ACSView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainTab dialog


CMainTab::CMainTab(CWnd* pParent /*=NULL*/)
	: CDialog(CMainTab::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMainTab)
	m_txtWAIFValue = _T("");
	//}}AFX_DATA_INIT
}


void CMainTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMainTab)
	DDX_Control(pDX, btnRun, m_btnRunCtrl);
	DDX_Control(pDX, btnStop, m_btnStopCtrl);
	DDX_Control(pDX, btnRecord, m_btnRecordCtrl);
	DDX_Control(pDX, IDC_LIST1, m_wndList);
	DDX_Text(pDX, txtWAIF, m_txtWAIFValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMainTab, CDialog)
	//{{AFX_MSG_MAP(CMainTab)
	ON_BN_CLICKED(btnAdd, OnbtnAdd)
	ON_BN_CLICKED(btnDelete, OnbtnDelete)
	ON_BN_CLICKED(btnEdit, OnbtnEdit)
	ON_BN_CLICKED(btnRecord, OnbtnRecord)
	ON_BN_CLICKED(btnStop, OnbtnStop)
	ON_BN_CLICKED(btnRun, OnbtnRun)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainTab message handlers

BOOL CMainTab::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	m_wndList.SetGridLines(TRUE); // SHow grid lines
	m_wndList.SetCheckboxeStyle(RC_CHKBOX_NONE); // Enable checkboxes
	m_wndList.SetEditable(FALSE); // Allow sub-text edit
	

	//m_wndList.SortItems(0, TRUE); // sort the 1st column, ascending
	//m_bSortable = m_wndList.IsSortable();
	//m_wndList.SetColumnHeader(_T("STT, 30; Class caption, 150; Action, 70; Handle, 70; Class Style, 50; xy, 50"));
	//m_wndList.SetColumnHeader(_T("STT, 30; Class caption, 100; Class Name, 100; Class Style, 100; Handle, 50; Action, 70; xy, 200"));
	m_wndList.SetColumnHeader(_T("STT, 35; Action, 70; xy, 500"));
	UpdateData(FALSE);
	
	//GetDlgItem(IDC_ALLOWSORT)->EnableWindow(m_wndList.HasColumnHeader());

	// now play some colorful stuff
	
	// Set the 3rd column background color to yellow, text color to blue
	m_wndList.SetItemTextColor(-1, 2, RGB(0, 0, 0));
	//m_wndList.SetItemBkColor(-1, 2, RGB(255, 255, 0));

	//m_pRecordNoteDlg = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CMainTab::OnbtnAdd() 
{
//	// TODO: Add your control notification handler code here
//	//m_dlgRecordNote.m_msgText = _T("khoi tao");
//
//	m_dlgRecordNote.m_isNew = true;//set to mode new
//	m_dlgRecordNote.m_msgText.Empty();
//	int nResponse = m_dlgRecordNote.DoModal();
//	if (nResponse == IDOK)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with OK
//		CACSDlg* pMainDlg = (CACSDlg*) AfxGetApp()->m_pMainWnd;
//		pMainDlg->MyReRunTimer();
//	}
//	else if (nResponse == IDCANCEL)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with Cancel
//	}

}

void CMainTab::OnbtnEdit() 
{
//	// TODO: Add your control notification handler code here
//	COleDateTime oleDT;
//
//	POSITION pos = m_wndList.GetFirstSelectedItemPosition();
//	int nItem = m_wndList.GetNextSelectedItem(pos);
//	//m_wndList.GetItemText(nItem,0);
//
//	
//	//DateTime_SetSystemtime(m_dlgRecordNote.m_dtpDateTime,
//	CString temp = m_wndList.GetItemText(nItem,1);	
//	//MessageBox(temp);
//	oleDT.ParseDateTime(temp);
//	SYSTEMTIME systime;
//	oleDT.GetAsSystemTime(systime);
//	//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,2);
//	m_dlgRecordNote.m_strFrequency = m_wndList.GetItemText(nItem,2);
//	m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,3);
//	m_dlgRecordNote.m_editSysTime = systime;	
//
//	//set config values
//	m_dlgRecordNote.m_isNew = false;//set to mode edit
//	m_dlgRecordNote.m_indexItemUpdating = nItem;
//
//	int nResponse = m_dlgRecordNote.DoModal();
//	
//
//	if (nResponse == IDOK)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with OK
//		CACSDlg* pMainDlg = (CACSDlg*) AfxGetApp()->m_pMainWnd;
//		pMainDlg->MyReRunTimer();
//	}
//	else if (nResponse == IDCANCEL)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with Cancel
//	}	
}

void CMainTab::OnbtnDelete() 
{
//	// TODO: Add your control notification handler code here
//	POSITION pos = m_wndList.GetFirstSelectedItemPosition();
//	int nItem = m_wndList.GetNextSelectedItem(pos);
//	//m_wndList.GetItemText(nItem,0);
//
//	//m_dlgRecordNote.m_msgText = m_wndList.GetItemText(nItem,1);
//	m_wndList.DeleteItem(nItem);
//	MyRefreshSTT();
//
//	//delete item in list record
//	CACSDlg* pMainDlg = (CACSDlg*) AfxGetApp()->m_pMainWnd;
//	pMainDlg->MyDeleteItemDLL(nItem);
//
//	pMainDlg->m_RecordNoteArray.RemoveAt(nItem);
//
////	CACSDlg* pMainDlg = (CACSDlg*) AfxGetApp()->m_pMainWnd;
//	pMainDlg->MyReRunTimer();
//
//	//int nResponse = m_dlgRecordNote.DoModal();
}

void CMainTab::MyRefreshSTT()
{
	CString s;
	for (int i = 0; i < m_wndList.GetItemCount(); i++)
	{
		s.Format(_T("%d"), i + 1);
		m_wndList.SetItemText(i,0,s);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMainTab message handlers

void CMainTab::OnbtnRecord() 
{
	// TODO: Add your control notification handler code here
	RecordAction();
}

void CMainTab::OnbtnStop() 
{
	// TODO: Add your control notification handler code here
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	//pView->m_bIsRecording = false;
	m_statusScript = PSTOP;
	pView->m_scriptManager.RefreshThread();	

	StopAction();
}


void CMainTab::RefreshStatusButton()
{
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();	
	switch(m_statusScript)
	{
	case PRUN:		
		m_btnRunCtrl.EnableWindow(false);
		m_btnStopCtrl.EnableWindow(true);		
		m_btnRecordCtrl.EnableWindow(false);
		//m_btnPauseCtrl.EnableWindow(true);
		break;
	case PSTOP:
		m_btnRunCtrl.EnableWindow(true);
		m_btnStopCtrl.EnableWindow(false);		
		m_btnRecordCtrl.EnableWindow(true);
		//m_btnPauseCtrl.EnableWindow(false);
		break;
	case PRECORD:
		m_btnRunCtrl.EnableWindow(false);
		m_btnStopCtrl.EnableWindow(true);		
		m_btnRecordCtrl.EnableWindow(false);
		//m_btnPauseCtrl.EnableWindow(true);
		break;
	case PPAUSE:
//		m_btnRunCtrl.EnableWindow(false);
//		m_btnStopCtrl.EnableWindow(true);		
//		m_btnRecordCtrl.EnableWindow(false);
//		//m_btnPauseCtrl.EnableWindow(true);
		break;		
	}
//	if(pView->m_bIsRecording)
//	{
//		m_btnRecordCtrl.EnableWindow(false);
//		m_btnStopCtrl.EnableWindow(true);
//	}
//	else
//	{
//		m_btnRecordCtrl.EnableWindow(true);
//		m_btnStopCtrl.EnableWindow(false);
//	}
}

void CMainTab::RecordAction()
{
	//CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	//pView->m_bIsRecording = true;
	m_statusScript = PRECORD;
	RefreshStatusButton();
//	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
//	pView->setHookKeyBoard();
}

void CMainTab::StopAction()
{
	RefreshStatusButton();
//	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
//	pView->setUnHookKeyBoard();
}

void CMainTab::OnbtnRun() 
{
	// TODO: Add your control notification handler code here
	RunAction();
	m_txtWAIFValue = _T("");
	UpdateData(false);
}

void CMainTab::RunAction()
{
	//reset default color to every row on list control
	for(int i = 0; i<m_wndList.GetItemCount(); i++)
	{
		m_wndList.SetItemBkColor(i, -1, RGB(255,255,255));
	}
	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
	//pView->m_bIsRecording = false;
	m_statusScript = PRUN;
	RefreshStatusButton();
	pView->RunScriptAll();
//	CACSView* pView = (CACSView*) CFunctionHelper::GetActiveView();
//	pView->setUnHookKeyBoard();
}
