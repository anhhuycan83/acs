// FunctionHelper.cpp: implementation of the CFunctionHelper class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ACS.h"
#include <math.h>//for convert hex to string
#include "FunctionHelper.h"
#include "diskid32.h"
#include "md5.h"
#include "DateTimeFormat.h"


//#include <afxcoll.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// define for extern variable
//////////////////////////////////////////////////////////////////////
enum eFrequency;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWindowInfor::CWindowInfor()
{
	m_hWnd = NULL;
	m_caption.Empty();//title
	m_className.Empty();
	long m_calssStyle = 0;
}

CWindowInfor::~CWindowInfor()
{

}

CFunctionHelper::CFunctionHelper()
{

}

CFunctionHelper::~CFunctionHelper()
{
	
}

CString CFunctionHelper::GetProgramDir()
{
	CString RtnVal;
    TCHAR    FileName[MAX_PATH];
	//CString FileName;
    GetModuleFileName(AfxGetInstanceHandle(), FileName, MAX_PATH);
    RtnVal = FileName;
    RtnVal = RtnVal.Left(RtnVal.ReverseFind('\\'));
    return RtnVal;
}

CView* CFunctionHelper::GetActiveView()
{	
	CFrameWnd * pFrame = (CFrameWnd *)(AfxGetApp()->m_pMainWnd);
    CView * pView = pFrame->GetActiveView();
	return pView;
}

CDocument* CFunctionHelper::GetActiveDocument()
{
	CFrameWnd * pFrame = (CFrameWnd *)(AfxGetApp()->m_pMainWnd);
     return (CDocument *) pFrame->GetActiveDocument();
}


__int64 CFunctionHelper::hex_string_to_long(CString hexStg)
{
  int n= 0;				 // position in string
  __int64 intValue = 0;  // integer value of hex string
  CUIntArray digit;		 // hold values to convert

  //tokenize all digits to an array containing their value
  //while (n < _tcsclen(hexStg))
  while (n < hexStg.GetLength())
  {
     if (hexStg[n]=='\0')
        break;
     if (hexStg[n] > 0x29 && hexStg[n] < 0x40 ) //if 0 to 9
        digit.Add(hexStg[n] & 0x0f);            //convert to int
     else if (hexStg[n] >='a' && hexStg[n] <= 'f') //if a to f
        digit.Add((hexStg[n] & 0x0f) + 9);      //convert to int
     else if (hexStg[n] >='A' && hexStg[n] <= 'F') //if A to F
        digit.Add((hexStg[n] & 0x0f) + 9);      //convert to int
     else break;
    n++;
  }
  
  n--;					//back one digit
  int p=0;				//power factor
  while(n >=0) 
  {
     intValue = intValue + (digit.GetAt(n)*(__int64)pow (2,p));
     n--;   // next digit to process
	 p+=4;	//everytime the power goes up in 4
  }
  
  return intValue;

}

//convert a long to a hex string
void CFunctionHelper::convert_long_to_hex(CString &str, __int64 num)
{
	//1) divide the number to 16
	//2) use the reminder to index it to get the digit in hex
	//3) divide the result from before (less the reminder) in 16 and so on

	CStringArray digits;//array of all hex digit we will index it in reverse later
	str=_T("");//reset the string
	__int64 rem=0;//reminder
	
	TCHAR hex[16][2]={_T("0"),_T("1"),_T("2"),_T("3"),_T("4"),_T("5"),_T("6"),_T("7"),_T("8"),_T("9"),_T("A"),_T("B"),_T("C"),_T("D"),_T("E"),_T("F")};

	
	do 
	{
	  rem=num%16;
		if (rem==0 && num==0)
			break;
	  digits.Add (hex[rem]);
	  num/=16;
	} while (num>=0);

	//combine to a string 

	for (int add=digits.GetSize()-1;add>=0;add--)
		str+=digits.GetAt(add);

}

CString CFunctionHelper::EnumToStringKindOfWindowAction(eKindOfWindowAction ins_enum)
{
	CString sRs;
	switch(ins_enum)
	{
	case KeyWindowAction:
		sRs = _T("KeyWindowAction");
		break;
	case MouseWindowAction:
		sRs = _T("MouseWindowAction");
		break;
	}
	return sRs;
}

eKindOfWindowAction CFunctionHelper::StringToEnumKindOfWindowAction(CString s)
{
	eKindOfWindowAction eRs;
	
	STR_SWITCH(s)
	{	
		STR_CASE(_T("KeyWindowAction"))
		{	
			eRs = KeyWindowAction;
			break;
		}		
		STR_CASE(_T("MouseWindowAction"))
		{
			eRs = MouseWindowAction;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = KeyWindowAction;
			break;
		}
	}
	STR_SWITCH_END()
	return eRs;
}

CString CFunctionHelper::EnumToStringKindOfClick(eKindOfClick ins_enum)
{
	CString sRs;
	switch(ins_enum)
	{
	case LeftClick:
		sRs = _T("LeftClick");
		break;
	case RightClick:
		sRs = _T("RightClick");
		break;
	case MiddleClick:
		sRs = _T("MiddleClick");
		break;
	case ScrollUp:
		sRs = _T("ScrollUp");
		break;
	case ScrollDown:
		sRs = _T("ScrollDown");
		break;
	default:
		sRs = _T("LeftClick");
		break;
	}
	return sRs;

}

eKindOfClick CFunctionHelper::StringToEnumKindOfClick(CString s)
{
	eKindOfClick eRs;
	
	STR_SWITCH(s)
	{	
		STR_CASE(_T("LeftClick"))
		{	
			eRs = LeftClick;
			break;
		}		
		STR_CASE(_T("RightClick"))
		{
			eRs = RightClick;
			break;
		}
		STR_CASE(_T("MiddleClick"))
		{
			eRs = MiddleClick;
			break;
		}
		STR_CASE(_T("ScrollUp"))
		{
			eRs = ScrollUp;
			break;
		}
		STR_CASE(_T("ScrollDown"))
		{
			eRs = ScrollDown;
			break;
		}		
		DEFAULT_CASE()
		{
			eRs = LeftClick;
			break;
		}
	}
	STR_SWITCH_END()
	return eRs;
}


CString CFunctionHelper::EnumToStringKindOfChar(eKindOfChar ins_enum)
{
	CString sRs;
	switch(ins_enum)
	{
	case NormalChar:
		sRs = _T("NormalChar");
		break;
	case FunctionChar:
		sRs = _T("FunctionChar");
		break;
	}
	return sRs;
}

eKindOfChar CFunctionHelper::StringToEnumKindOfChar(CString s)
{
	eKindOfChar eRs;
	
	STR_SWITCH(s)
	{	
		STR_CASE(_T("NormalChar"))
		{	
			eRs = NormalChar;
			break;
		}		
		STR_CASE(_T("FunctionChar"))
		{
			eRs = FunctionChar;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = NormalChar;
			break;
		}
	}
	STR_SWITCH_END()
	return eRs;
}

void CFunctionHelper::StringSplit(CString &str, CStringArray &arr, TCHAR chDelimitior)
{
	int nStart = 0, nEnd = 0;
	arr.RemoveAll();

	while (nEnd < str.GetLength())
	{
		// determine the paragraph ("xxx,xxx,xxx;")
		nEnd = str.Find(chDelimitior, nStart);
		if( nEnd == -1 )
		{
			// reached the end of string
			nEnd = str.GetLength();
		}

		CString s = str.Mid(nStart, nEnd - nStart);
		if (!s.IsEmpty())
			arr.Add(s);

		nStart = nEnd + 1;
	}
}



void CFunctionHelper::SetWindowInfor(CWindowInfor* wIF, HWND hWnd)
{
	TCHAR		szBuff[100];
	::GetClassName(hWnd, szBuff, sizeof (szBuff) - 1);
	wIF->m_className = szBuff;
	//Get the text (caption, title) of handle window. it can not copy text on control
	GetWindowText(hWnd, szBuff, sizeof (szBuff) - 1);
	wIF->m_caption = szBuff;
	//GWL_STYLE Get style of window
	wIF->m_calssStyle = GetWindowLong(hWnd, GWL_STYLE);
}

//function must be return false to stop enumeration
BOOL CALLBACK CFunctionHelper::EnumWindowsProc(HWND hWnd, LPARAM lParam)
{
	TCHAR		szBuff[100];
    if (IsWindowVisible(hWnd)) {
        ::GetClassName(hWnd, szBuff, sizeof (szBuff) - 1);        
    }
    return TRUE;
}

BOOL CALLBACK CFunctionHelper::EnumChildProc(HWND hwnd, LPARAM lParam)
{
	return TRUE; // must return TRUE; If return is FALSE it stops the recursion
}

void CFunctionHelper::MyGetLastError(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code
DWORD   dwLastError = ::GetLastError();
TCHAR   lpBuffer[256] = _T("?");
if(dwLastError != 0)    // Don't want to see a "operation done successfully" error ;-)
    ::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,                 // It�s a system error
                     NULL,                                      // No string to be formatted needed
                     dwLastError,                               // Hey Windows: Please explain this error!
                     MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),  // Do it in the standard language
                     lpBuffer,              // Put the message here
                     _tcsclen(lpBuffer)-1,                     // Number of bytes to store the message
                     NULL);
    MessageBox(NULL, lpBuffer, TEXT("Error"), MB_OK); 

    //ExitProcess(dw); 

}

//get the handle of child window from hwnd input that has a keyboard focus
HWND CFunctionHelper::GetHwndFocus(HWND hwnd)
{
	HWND hRs = 0;
	//begin sendkey
	DWORD g_threadTarget,g_threadCurrent ;
	
	g_threadTarget = GetWindowThreadProcessId(hwnd, NULL);
	g_threadCurrent = GetCurrentThreadId();
	if(g_threadTarget != g_threadCurrent)
	{
		AttachThreadInput(g_threadCurrent, g_threadTarget, TRUE);
		
		GUITHREADINFO i;
		i.cbSize = sizeof(GUITHREADINFO);
		if(::GetGUIThreadInfo(g_threadTarget,&i))
			hRs = i.hwndFocus;
		AttachThreadInput(g_threadCurrent, g_threadTarget, FALSE);				
	}
	return hRs;
}

PROCESS_INFORMATION    pi = {0};

int CFunctionHelper::InitConsoleWindow()
{
	STARTUPINFO si = {0};            // Initialize all members to zero
	si.cb = sizeof(STARTUPINFO);     // Set byte count
	
	AllocConsole();                  // Allocate console window
	freopen("CONOUT$", "a", stderr); // Redirect stderr to console
	
	// Display user message in console window.
	fprintf(stderr, "Application stderr output window\n");
	fprintf(stderr, "DO NOT CLOSE THIS WINDOW, ");
	fprintf(stderr, "it will terminate your application!\n");
	TCHAR szCmdline[10] = _T("cmd.exe");//_tcsdup(TEXT("C:\\Windows\\Notepad.exe"));

	
	return CreateProcess(NULL,// address of module name
		szCmdline,         // address of command line
		NULL,              // address of process security attributes
		NULL,              // address of thread security attributes
		TRUE,              // new process inherits handles
		CREATE_SUSPENDED,  // creation flags
		NULL,              // address of new environment block
		NULL,              // address of current directory name
		&si,               // address of STARTUPINFO
		&pi);              // address of PROCESS_INFORMATION
	
}

void CFunctionHelper::CloseConsole()
{
/* The following two lines need to be placed in the normal termination
	procedure for your application. */ 
	
	TerminateProcess(pi.hProcess, 0);
	CloseHandle(pi.hProcess);
	
}

unsigned int CFunctionHelper::ConcertStringToInt(CString strToConvert)
{
	unsigned int iRs= 0;
	for(int i = 0; i < strToConvert.GetLength(); i++)
	{
		iRs += (int)strToConvert.GetAt(i);
//MYDEL		int num; // Takes the result
//MYDEL		int nFields; // Receives the number of fields converted
//MYDEL		
//MYDEL		nFields = sscanf(ch, "%d", &num );
//MYDEL		
//MYDEL		if (nFields == 0 || nFields == EOF)
//MYDEL		{
//MYDEL			// Something went wrong if we get here
//MYDEL			...
//MYDEL		}
	}
	return iRs;
	
}

unsigned int CFunctionHelper::ConcertStringToIntEx(CString strToConvert, byte controlCode)
{
	unsigned int iRs= 0;
	for(int i = 0; i < strToConvert.GetLength(); i++)
	{
		switch(controlCode)
		{
		case 0://normal
			iRs += (int)strToConvert.GetAt(i);
			break;
		case 1://for hdd serial
			iRs += ((int)strToConvert.GetAt(i) + 15) * 83;
			break;
		case 2://for start date
			iRs += ((int)strToConvert.GetAt(i) + 25) * 4;
			break;
		case 3://for total day
			iRs += ((int)strToConvert.GetAt(i) + 5) * 8;
			break;
		case 4://for md5
			iRs += ((int)strToConvert.GetAt(i) + 8) * 3;
			break;
		}
	}
	return iRs;
}

//MYDEL   -- encode and decode for start date
//MYDEL   0 1 2 3 4 5 6 7 8 9
//MYDEL   Z Y X A B C M N O T
//MYDEL   EX: 15/12/1983
//MYDEL   YCYXYTOA
//MYDEL   OR 1983/15/12
//MYDEL   YTOAYCYX


CString CFunctionHelper::EncodeNumToChar(CString strNumToConvert)
{
	CString sRs = _T("");
	CString sTemp;
	int numItem;
	int nFields; // Receives the number of fields converted
	//MYDEL   	TCHAR ch;
	for(int i = 0; i < strNumToConvert.GetLength(); i++)
	{
		sTemp = strNumToConvert.GetAt(i);
		nFields = _stscanf(sTemp, _T("%d"), &numItem );
		if (nFields == 0 || nFields == EOF)
		{
			// Something went wrong if we get here
		}
		else
		{
			switch(numItem)
			{
			case 0://normal
				sTemp = _T("Z");
				break;
			case 1://normal
				sTemp = _T("Y");
				break;
			case 2://normal
				sTemp = _T("X");
				break;
			case 3://normal
				sTemp = _T("A");
				break;
			case 4://normal
				sTemp = _T("B");
				break;
			case 5://normal
				sTemp = _T("C");
				break;
			case 6://normal
				sTemp = _T("M");
				break;
			case 7://normal
				sTemp = _T("N");
				break;
			case 8://normal
				sTemp = _T("O");
				break;
			case 9://normal
				sTemp = _T("T");
				break;
			}
			sRs += sTemp;
		}
	}
	return sRs;
}

CString CFunctionHelper::DecodeCharToNum(CString strCharToConvert)
{
	CString sRs = _T("");
	CString sTemp;
	
	for(int i = 0; i < strCharToConvert.GetLength(); i++)
	{
		sTemp = strCharToConvert.GetAt(i);
		STR_SWITCH(sTemp)
		{	
			STR_CASE(_T("Z"))
			{	
				sTemp = _T("0");
				break;
			}		
			STR_CASE(_T("Y"))
			{	
				sTemp = _T("1");
				break;
			}		
			STR_CASE(_T("X"))
			{	
				sTemp = _T("2");
				break;
			}		
			STR_CASE(_T("A"))
			{	
				sTemp = _T("3");
				break;
			}		
			STR_CASE(_T("B"))
			{	
				sTemp = _T("4");
				break;
			}		
			STR_CASE(_T("C"))
			{	
				sTemp = _T("5");
				break;
			}		
			STR_CASE(_T("M"))
			{	
				sTemp = _T("6");
				break;
			}		
			STR_CASE(_T("N"))
			{	
				sTemp = _T("7");
				break;
			}		
			STR_CASE(_T("O"))
			{	
				sTemp = _T("8");
				break;
			}		
			STR_CASE(_T("T"))
			{	
				sTemp = _T("9");
				break;
			}		
			DEFAULT_CASE()
			{
				sTemp = _T("");
				break;
			}
		}
		STR_SWITCH_END()
		sRs += sTemp;
	}
	return sRs;
}
//return int of md5
unsigned int CFunctionHelper::ConvertMD5ToIntMD5(CString strMD5)
{
	unsigned int iRs= 0;
	for(int i = 0; i < strMD5.GetLength(); i++)
	{
		iRs += ((int)strMD5.GetAt(i) + 8) * 3;
		//iRs += ((int)strMD5.GetAt(i) + 83) * 4;
	}
	return iRs;
}

//DEL unsigned int CFunctionHelper::ActiveCodeFromIntMD5(unsigned int uMD5)
//DEL {
//DEL 	return (uMD5 + 84) * 25;
//DEL 
//DEL }

unsigned int CFunctionHelper::EncodeMD5Server(unsigned int uMD5Server)
{
	return (uMD5Server + 83) * 15;
}


CString CFunctionHelper::LicenseToActualMD5Server(CString sLicense)
{
	CString sRs = _T("");
	CString strMD5Encode, strActualMD5;
	unsigned int actualMD5, uLicense;
	strMD5Encode = CFunctionHelper::DecodeCharToNum(sLicense);
	int nFields;
	nFields = _stscanf(strMD5Encode, _T("%d"), &uLicense);
	if (nFields == 0 || nFields == EOF)
		return sRs;
	actualMD5 = (uLicense / 15) - 83;
	strActualMD5.Format(_T("%d"), actualMD5);
	sRs = EncodeNumToChar(strActualMD5);
	return sRs;
}

unsigned int CFunctionHelper::CreateMD5Server(CString sAC, CString sStartDate, CString sNumDay)
{
	unsigned int uMD5Server;
	md5 clsMD5;
	CString strTemp;
	strTemp.Format(_T("%s%s%s"), sAC, sStartDate, sNumDay);
//MYDEL   	TCHAR myChar[100]; // the number depends on your CString's length
//MYDEL   	//initialize your storage buffer
//MYDEL   	memset(myChar,0,sizeof(myChar));
//MYDEL   	_stprintf(myChar,_T("%s"),strTemp);

//MYDEL   	char ascstr[32];
//MYDEL   	USES_CONVERSION;
//MYDEL   	strcpy(ascstr, W2A(strTemp)); 

	USES_CONVERSION;
	LPSTR myChar;
	//tLicense = W2CT(strTemp.GetBuffer(100));
	myChar = T2A(strTemp.GetBuffer(100));
	
	//CStringA myChar(strTemp);
	strTemp = MD5String(myChar);
	uMD5Server = ConvertMD5ToIntMD5(strTemp);

	strTemp.ReleaseBuffer();
	return uMD5Server;
	
}

CString CFunctionHelper::CreateActiveCodeFromHddSerial()
{
	CString strHddSerial;
		CString sRs = _T("");
	unsigned int uMD5;
	
//MYDEL   	TCHAR myChar[100]; // the number depends on your CString's length
//MYDEL   	//initialize your storage buffer
//MYDEL   	memset(myChar,0,sizeof(myChar));
	
	diskid32 m_disk;
	CString hddSerial;
	if(m_disk.GetHddSerial(hddSerial))
	{
		//_stprintf(myChar,_T("%s"),strHddSerial);
		USES_CONVERSION;		
		LPSTR myChar;
		myChar = T2A(hddSerial.GetBuffer(100));
		sRs = MD5String(myChar);
		uMD5 = ConvertMD5ToIntMD5(sRs);
		sRs.Format(_T("%d"), uMD5);
		sRs = EncodeNumToChar(sRs);

		hddSerial.ReleaseBuffer();
//MYDEL		//trace
//MYDEL		CString sTrace;
//MYDEL		sTrace.Format(_T("hddserial: %s, md5int: %d, md5char: %s\n"), myChar, uMD5, sRs);
//MYDEL		TRACE(sTrace);
	}
	return sRs;
}

//MYDELCString CFunctionHelper::CreateLicenseToClient(CString ActiveCode, COleDateTime starDate, int numDay)
//MYDEL{
//MYDEL	CString sRs = _T("");
//MYDEL	//COleDateTime currentDT = COleDateTime::GetCurrentTime();
//MYDEL	CDateTimeFormat dtf;
//MYDEL	CString sNumDay, sStartDate, sMD5Server;
//MYDEL	unsigned int iMD5Server;
//MYDEL	sNumDay.Format(_T("%d"), numDay);
//MYDEL	sNumDay = CFunctionHelper::EncodeNumToChar(sNumDay);
//MYDEL	//get the startdate string
//MYDEL	dtf.SetDateTime(starDate);
//MYDEL	dtf.SetFormat(_T("MMddyy"));
//MYDEL	sStartDate = dtf.GetString();
//MYDEL	sStartDate = CFunctionHelper::EncodeNumToChar(sStartDate);
//MYDEL	iMD5Server = CFunctionHelper::CreateMD5Server(ActiveCode, sStartDate, sNumDay);
//MYDEL	//decode this md5
//MYDEL	iMD5Server = CFunctionHelper::EncodeMD5Server(iMD5Server);
//MYDEL	sMD5Server.Format(_T("%d"), iMD5Server);
//MYDEL	sMD5Server = CFunctionHelper::EncodeNumToChar(sMD5Server);
//MYDEL	sRs.Format(_T("%s-%s-%s"), sMD5Server, sStartDate, sNumDay);
//MYDEL	return sRs;
//MYDEL}

CString CFunctionHelper::CreateLicenseNotEncode(CString ActiveCode, COleDateTime starDate, int numDay)
{
	CString sRs = _T("");
	//COleDateTime currentDT = COleDateTime::GetCurrentTime();
	CDateTimeFormat dtf;
	CString sNumDay, sStartDate, sMD5Server;
	unsigned int iMD5Server;
	sNumDay.Format(_T("%d"), numDay);
	sNumDay = CFunctionHelper::EncodeNumToChar(sNumDay);
	//get the startdate string
	dtf.SetDateTime(starDate);
	dtf.SetFormat(_T("MMddyy"));
	sStartDate = dtf.GetString();
	sStartDate = CFunctionHelper::EncodeNumToChar(sStartDate);
	iMD5Server = CFunctionHelper::CreateMD5Server(ActiveCode, sStartDate, sNumDay);
	//decode this md5
	sMD5Server.Format(_T("%d"), iMD5Server);
	sMD5Server = CFunctionHelper::EncodeNumToChar(sMD5Server);
	sRs = sMD5Server;
	return sRs;
}
void CFunctionHelper::CheckLicense(BOOL& bRs, COleDateTime &dtStarDate, COleDateTime &dtEndDate, CString sLicense)
{
	
	bRs = FALSE;
	CStringArray sArr;
	CString sActiveCode, sStarDate, sNumDay, sMD5ServerEncoded, sMD5ServerActual, sTemp;
	//COleDateTime startDate, endDate;
	int numDay;
	CFunctionHelper fn;
	//trace
	CString sTrace;
	
	fn.StringSplit(sLicense, sArr, _T('-'));
	if(sArr.GetSize() == 3)
	{
		// md5server - startdate - numday
		sActiveCode = CFunctionHelper::CreateActiveCodeFromHddSerial();
		//trace
		sTrace.Format(_T("acvitecode: %s\n"), sActiveCode);
		TRACE(sTrace);

		sMD5ServerEncoded = sArr.GetAt(0);
		sStarDate = sArr.GetAt(1);
		sNumDay = sArr.GetAt(2);
		//trace
		sTrace.Format(_T("md5serverencode: %s, StartDate: %s, numdate: %s\n"), sMD5ServerEncoded, sStarDate, sNumDay);
		TRACE(sTrace);
 
    //get star date
    dtStarDate = GetDateFromDateEncodedString(sStarDate);
		//get numday
    numDay = GetNumDayEncodedString(sNumDay);		
		//endDate = startDate
		COleDateTimeSpan ts(numDay);
		dtEndDate = dtStarDate + ts;
		//trace
		CDateTimeFormat d;
		d.SetDateTime(dtEndDate);
		d.SetFormat(_T("MMddyy"));
		sTrace.Format(_T("numday: %d, endDate: %s\n"), numDay, d.GetString());
		TRACE(sTrace);

		//check md5server 
		sMD5ServerActual = CFunctionHelper::LicenseToActualMD5Server(sMD5ServerEncoded);
		//trace
		sTrace.Format(_T("md5actual from md5encod from serial: %s\n"), sMD5ServerActual);
		TRACE(sTrace);
		//now create md5 by inputs: activecode, startdate, numday to compare the actual md5server
		CString sLicenseClientCreate;
		sLicenseClientCreate = CFunctionHelper::CreateLicenseNotEncode(sActiveCode, dtStarDate, numDay);
		//trace
		sTrace.Format(_T("License(md5ClientActual) Client Create from activecode, start date and numDay: %s\n"), sLicenseClientCreate);
		TRACE(sTrace);
		//LicenseToMD5Server(
		UINT currentNumDay = (UINT)COleDateTime::GetCurrentTime().m_dt;
		if(sMD5ServerActual == sLicenseClientCreate)
		{
			if((UINT)dtStarDate.m_dt <= currentNumDay 
				&& currentNumDay <= (UINT)dtEndDate.m_dt)
				bRs = TRUE;
		}
	}
	
}

//output:
//c:\windows\system\
//Retrieves the path of the system directory. The system directory contains system files such as dynamic-link libraries and drivers.
CString CFunctionHelper::GetSystemDirectory()
{
	TCHAR szTemp[MAX_PATH];
	::GetSystemDirectory(szTemp,MAX_PATH);
	return szTemp;
	
}

//Retrieves the current directory for the current process.
CString CFunctionHelper::GetCurrentDirectory()
{
	TCHAR szTemp[MAX_PATH];
	::GetCurrentDirectory(MAX_PATH,szTemp);
	return szTemp;
}

//Retrieves the path of the Windows directory.
CString CFunctionHelper::GetWindowsDirectory()
{
	TCHAR szTemp[MAX_PATH];
	::GetSystemDirectory(szTemp,MAX_PATH);
	return szTemp;
}

COleDateTime CFunctionHelper::GetDateFromDateEncodedString(CString strDateEncoded)
{
	COleDateTime dRs;
	//dRs.SetStatus(COleDateTime::error);
	if(strDateEncoded.GetLength() == 6)
	{
		//ex: BCAXBA => 180512
		int nFields;		
		int month, day, year;		
			CString sTemp = CFunctionHelper::DecodeCharToNum(strDateEncoded);
			
			CString sTemp2;
			sTemp2 = sTemp.Left(2);
			nFields = _stscanf(sTemp2, _T("%02d"), &month);
			if (nFields == 0 || nFields == EOF)
				return dRs;
			sTemp2 = sTemp.Mid(2,2);
			nFields = _stscanf(sTemp2, _T("%02d"), &day);
			if (nFields == 0 || nFields == EOF)
				return dRs;
			sTemp2 = sTemp.Right(2);
			nFields = _stscanf(sTemp2, _T("%02d"), &year);
			if (nFields == 0 || nFields == EOF)
				return dRs;
			year += 2000;
			//set startdate
			dRs.SetDate(year, month, day);
			CDateTimeFormat d2;
			d2.SetDateTime(dRs);
			d2.SetFormat(_T("MMddyy"));
			//trace
			//sTrace.Format(_T("after parse datetime from serial input:\n month: %d, day: %d, year: %d, startdate: %s\n"), 
			//	month, day, year, d2.GetString());
			//TRACE(sTrace);
		dRs.SetDate(year, month, day);
		
	}
	return dRs;
}

int CFunctionHelper::GetNumDayEncodedString(CString strNumDayEncoded)
{
  int iRs = -1;
  //get numday
		CString sTemp = CFunctionHelper::DecodeCharToNum(strNumDayEncoded);
    int nFields = _stscanf(sTemp, _T("%d"), &iRs);
    if (nFields == 0 || nFields == EOF)
			return -1;
    return iRs;
}

bool CFunctionHelper::GetFileVersion(CString &sFileVersion)
{
//MYDEL   	string temp;
//MYDEL       bool bResult = false;
//MYDEL       DWORD size;
//MYDEL       DWORD dummy;
//MYDEL       char filename[ 130 ];
//MYDEL       unsigned int len;
//MYDEL       GetModuleFileName( NULL, filename, 128 );
//MYDEL       size = GetFileVersionInfoSize( filename, &dummy );
//MYDEL       if( size == 0 )
//MYDEL       {
//MYDEL           this->ver = "No Version Information!";
//MYDEL           return true;
//MYDEL       }
//MYDEL       char* buffer = new char[ size ];
//MYDEL       VS_FIXEDFILEINFO* data = NULL;
//MYDEL       if( buffer = NULL ){ return true; }
//MYDEL       bResult = GetFileVersionInfo( filename, 0, size, (void*)buffer );
//MYDEL       if( !bResult )
//MYDEL       {
//MYDEL           this->ver = STRLASTERROR; // STRLASTERROR is a custom macro
//MYDEL           return true;
//MYDEL       }
//MYDEL       bResult = VerQueryValue( buffer, "\\", (void**)&data, &len );
//MYDEL       if( !bResult || data == NULL || len != sizeof(VS_FIXEDFILEINFO) )
//MYDEL       {
//MYDEL           this->ver = "Could Not Retrieve Values!";
//MYDEL           return true;
//MYDEL       }
//MYDEL       // here I would extract the needed values
//MYDEL       delete[] buffer;
//MYDEL       this->valid = true;
    return false;

}

eFrequency CFunctionHelper::GetFrequencyEnum(CString strFre)
{
	eFrequency eRs = ONCE;
	//CString sCase("AAAA");
	STR_SWITCH(strFre)      //Start of switch
	{                        //Opening and closing braces
		//NOT MANDATORY for switch.
		STR_CASE(_T("ONCE"))
		{                      //MANDATORY for case.
			eRs = ONCE;
			break;               //break has to in braces of case
		}  
		STR_CASE(_T("DAILY"))
		{                      //MANDATORY for case.
			eRs = DAILY;
			break;               //break has to in braces of case
		}                      //Opening and closing braces
		//MANDATORY for case.
		
		STR_CASE(_T("WEEKLY"))
		{
			eRs = WEEKLY;
			break;
		}
		STR_CASE(_T("MONTHLY"))
		{
			eRs = MONTHLY;
			break;
		}
		DEFAULT_CASE()
		{
			eRs = ONCE;//Default handling if any
			break;
		}
	}                        //Opening and closing braces
	//NOT MANDATORY for switch
	STR_SWITCH_END()         //MANDATORY statement
		

	return eRs;
}

CString CFunctionHelper::GetFrequencyString(eFrequency eFre)
{
	CString sRs = "";
	switch(eFre)
	{
	case ONCE:
		sRs = "ONCE";
		break;
	case DAILY:
		sRs = "DAILY";
		break;
	case WEEKLY:
		sRs = "WEEKLY";
		break;
	case MONTHLY:
		sRs = "MONTHLY";
		break;
	default:
		sRs = "ONCE";
		break;
	}
	return sRs;
}

COleDateTime CFunctionHelper::GetOleDateTime(CDateTimeCtrl* dateTimeCtrl)
{
	SYSTEMTIME dt;	
	COleDateTime dtOle;
	DateTime_GetSystemtime(dateTimeCtrl->m_hWnd, &dt);
	dtOle = dt;
	return dtOle;
}

double CFunctionHelper::ConvertCStringToDouble(CString strDouble)
{
	double dTmp = 0;
	USES_CONVERSION;		
	LPSTR myChar;
	myChar = W2A(strDouble);
	dTmp = atof(myChar);
	return dTmp;
}

COleDateTime CFunctionHelper::GetPresentDT(COleDateTime dtInput, eFrequency eFre)
{
	COleDateTimeSpan ts;
	COleDateTime dtRs = dtInput;
	COleDateTime dtNow = COleDateTime::GetCurrentTime();
	int dayToAdd, dowInput, dowNow, dayInput, dayNow;		
	switch(eFre)
	{
	case ONCE:
		//dtRs = dtInput;
		break;
	case DAILY:
		//dtRs = dtInput;
		
		dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
			dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
		if(dtRs < dtNow)
		{
			//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 1, 
			//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

			ts.SetDateTimeSpan(1, 0, 0, 0);
			dtRs = dtRs + ts;
			//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
			//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			//Rs = dtRs + ts.SetDateTimeSpan(1, 0, 0, 0);
		}
		break;
	case WEEKLY:		
		dowInput = dtInput.GetDayOfWeek();
		dowNow = dtNow.GetDayOfWeek();
		if(dowInput == dowNow)
		{			
			//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay());
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			if(dtRs < dtNow)
			{
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtInput.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());		

				ts.SetDateTimeSpan(7, 0, 0, 0);
				dtRs = dtRs + ts;
			}
		}
		else //day of week of input != day of week of now
		{
			if(dowInput < dowNow)
			{
				dayToAdd = dowNow - dowInput;				
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd);

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + 7 - dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
					dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = dtRs + ts.SetDateTimeSpan(7 - dayToAdd, 0, 0, 0);

				ts.SetDateTimeSpan(7 - dayToAdd, 0, 0, 0);
				dtRs = dtRs + ts;
			}
			else
			{
				dayToAdd = dowInput - dowNow;
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + dayToAdd);

				dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay(), 
					dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtNow.GetDay() + dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				ts.SetDateTimeSpan(dayToAdd, 0, 0, 0);
				dtRs = dtRs + ts;				
			}
		}
		break;
	case MONTHLY:		
		dayInput = dtInput.GetDay(); // day of month
		dayNow = dtNow.GetDay();// day of month
		if(dayInput == dayNow)
		{			
			//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dayNow);
			dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dayNow, 
				dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
			if(dayInput < dtNow)
			{
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtNow.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				//dtRs = MySetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtNow.GetDay(), 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());

				//ts.SetDateTimeSpan(dayToAdd, 0, 0, 0);
				if(dtNow.GetMonth() < 12)
				{
					int daysInMonth = CFunctionHelper::MyGetDaysInMonth(dtNow.GetMonth() + 1, dtNow.GetYear());
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
				}
				else
				{
					int daysInMonth = MyGetDaysInMonth(1, dtNow.GetYear() + 1);
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
				}				
			}
		}
		else //day of input != day of now
		{
			if(dayInput < dayNow)
			{				
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth() + 1, dayInput);

				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dayInput, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				if(dtNow.GetMonth() < 12)
				{
					int daysInMonth = MyGetDaysInMonth(dtNow.GetMonth() + 1, dtNow.GetYear());
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth() + 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					
				}
				else
				{
					int daysInMonth = MyGetDaysInMonth(1, dtNow.GetYear() + 1);
					if(daysInMonth < dtInput.GetDay())
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, daysInMonth, 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}
					else
					{
						dtRs.SetDateTime(dtNow.GetYear() + 1, 1, dtInput.GetDay(), 
							dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
					}					
				}
			}
			else//input > now
			{
				//dayToAdd = dayInput - dayNow;
				//dtRs.SetDate(dtNow.GetYear(), dtNow.GetMonth(), dayNow + dayToAdd);
				    // Adjust time and frac time
				
				//dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dayNow + dayToAdd, 
				//	dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				int daysInMonth = MyGetDaysInMonth(dtNow.GetMonth(), dtNow.GetYear());
				if(dtInput.GetDay() >= daysInMonth)
				{
					dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), daysInMonth, 
						dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				}
				else
				{
					dtRs.SetDateTime(dtNow.GetYear(), dtNow.GetMonth(), dtInput.GetDay(), 
						dtInput.GetHour(), dtInput.GetMinute(), dtInput.GetSecond());
				}				
			}
		}
		break;
	default:		
		break;
	}
	
	return dtRs;
}

int CFunctionHelper::MyGetDaysInMonth(int month, int year)
{
	int dayNo = 28;
	
	switch(month)
	{
	case 1:		
	case 3:		
	case 5:		
	case 7:		
	case 8:		
	case 10:		
	case 12:
		dayNo = 31;
		break;		
	case 2:
		if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
			dayNo = 29;
		else
			dayNo = 28;
		break;		
	case 4:		
	case 6:		
	case 9:
	case 11:
		dayNo = 30;
		break;
	default:
		break;
	}
	return dayNo;
}
