// OptionTab.cpp : implementation file
//

#include "stdafx.h"
#include "ACS.h"
#include "OptionTab.h"
#include "WinStartup.h"
#include "textfile.h"
//#include "ACSView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionTab dialog


COptionTab::COptionTab(CWnd* pParent /*=NULL*/)
	: CDialog(COptionTab::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionTab)
	//}}AFX_DATA_INIT
}


void COptionTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionTab)
	DDX_Control(pDX, rbF11, m_rbF11Ctrl);
	DDX_Control(pDX, rbF10, m_rbF10Ctrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionTab, CDialog)
	//{{AFX_MSG_MAP(COptionTab)
	ON_BN_CLICKED(btnDefault, OnbtnDefault)
	ON_BN_CLICKED(btnSave, OnbtnSave)
	ON_BN_CLICKED(chbStartup, OnchbStartup)
	ON_BN_CLICKED(rbF10, OnrbF10)
	ON_BN_CLICKED(rbF11, OnrbF11)
	ON_BN_CLICKED(chbTopMost, OnchbTopMost)
	ON_BN_CLICKED(chbShowAtStart, OnchbShowAtStart)
	ON_BN_CLICKED(chbRepeat, OnchbRepeat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionTab message handlers

//MYDELvoid COptionTab::SetFileStorePath(CString ins_fullPath)
//MYDEL{
//MYDEL	//ins_fullPath = m_txtFileStore;
//MYDEL	UpdateData(false);	
//MYDEL}

//MYDELCString COptionTab::GetFileStorePath()
//MYDEL{
//MYDEL	UpdateData(true);
//MYDEL	return m_txtFileStore;
//MYDEL}

BOOL COptionTab::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	// TODO: Add extra initialization here
	//init check box start when system startup
	StartupUser user;
	user = AllUsers;
	//AfxGetApp()->m_hInstance, _T("Startup ArcKey"), user);
	if(CWinStartup::isAppAdded(AfxGetApp()->m_hInstance, ONCE_INSTANCE, user) == true)
	{
		//m_chbStartupCtrl.SetCheck(true);
		//UpdateData(false);
		CheckDlgButton(chbStartup, TRUE);
	}
	else
	{
		CheckDlgButton(chbStartup, FALSE);
	}
	
	//no longer use load config file because this function should use in main parent dialog for init read 
	// config file in parent dialog

	////load config file 
	//loadConfigValue();
	setConfigValue(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COptionTab::OnchbStartup() 
{
	// TODO: Add your control notification handler code here
	StartupUser user;
	user = AllUsers;

	if(IsDlgButtonChecked(chbStartup))
	{
		CWinStartup::AddApp(AfxGetApp()->m_hInstance, ONCE_INSTANCE, user);
	}
	else
	{
		CWinStartup::RemoveApp(ONCE_INSTANCE, user);
	}
}

//DEL void COptionTab::loadConfigValue()
//DEL {
//DEL 	CFileFind finder;	
//DEL 	CString FullPath;
//DEL 	FullPath = GetProgramDir();
//DEL 	FullPath += _T("\\config.ini");
//DEL 
//DEL 	free((void*)AfxGetApp()->m_pszProfileName);
//DEL 	AfxGetApp()->m_pszProfileName = _tcsdup(FullPath);
//DEL 
//DEL 	if(!finder.FindFile(FullPath))
//DEL 	{
//DEL 		createConfigFile(FullPath);		
//DEL 	}	
//DEL }

void COptionTab::createConfigFile()
{
//	//create file
//	CTextFileWrite fwrite(ins_FullPath, CTextFileWrite::UTF_8);
//	ASSERT(fwrite.IsOpen());
//	fwrite.Close();

	//write default value:
	//FILESTORE
	CString strTemp;
	CACSApp* pApp = (CACSApp*)AfxGetApp();

//MYDEL	strTemp = _T("C:\\myRecordNote.txt");
//MYDEL	pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("FILESOTER"), strTemp);	
//MYDEL	
//MYDEL	//FILESOUND
//MYDEL	strTemp = _T("C:\\soud.wav");
//MYDEL	pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("FILESOUND"), strTemp);	
	//HOTKEY
	strTemp = _T("F10");
	pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("HOTKEY"), strTemp);	
	//TOPMOST	
	pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), false);	
	//show at start
	pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), true);	

	pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("REPEAT"), false);	

	//::WritePrivateProfileString(_T("INITIAL"), _T("TOPMOST"), _T("FALSE"), ins_FullPath);
}

//DEL CString COptionTab::GetProgramDir()
//DEL {
//DEL 	CString RtnVal;
//DEL     wchar_t    FileName[MAX_PATH];
//DEL 	//CString FileName;
//DEL     GetModuleFileName(AfxGetInstanceHandle(), FileName, MAX_PATH);
//DEL     RtnVal = FileName;
//DEL     RtnVal = RtnVal.Left(RtnVal.ReverseFind('\\'));
//DEL     return RtnVal;
//DEL }

void COptionTab::setConfigValue(bool ins_getSet)
{
	CString strTemp;
	CACSApp* pApp = (CACSApp*)AfxGetApp();
	if(ins_getSet) //set to config file
	{		
//MYDEL		//FILESTORE
//MYDEL		GetDlgItemText(txtFileStore, strTemp);
//MYDEL		pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("FILESOTER"), strTemp);

//MYDEL		//FILESOUND
//MYDEL		GetDlgItemText(txtFileStore, strTemp);
//MYDEL		pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("FILESOUND"), strTemp);

		//HOTKEY
		if(IsDlgButtonChecked(rbF10))
		{
			strTemp = _T("F10");
		}
		else
		{
			strTemp = _T("F11");
		}
		pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("HOTKEY"), strTemp);	

		//TOPMOST
		if(IsDlgButtonChecked(chbTopMost))
		{
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), true);
		}
		else
		{
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), false);
		}

		//SHOW AT START
		if(IsDlgButtonChecked(chbShowAtStart))	
		{
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), true);
		}
		else
		{			
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), false);
		}

		//REPEAT
		if(IsDlgButtonChecked(chbRepeat))
		{
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("REPEAT"), true);
		}
		else
		{
			pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("REPEAT"), false);
		}
		
	}
	else
	{
		//for using xml config file
//MYDEL		//RECORD		
//MYDEL		strTemp = pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));
//MYDEL		SetDlgItemText(txtFileStore, strTemp);
//MYDEL		//SOUND
//MYDEL		strTemp = pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("FILESOUND"), _T("C:\\sound.wav"));
//MYDEL		SetDlgItemText(txtFileSound, strTemp);
		//HOTKEY		
		if(pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F10")) == _T("F10"))
		{
			m_rbF10Ctrl.SetCheck(true);
			m_rbF11Ctrl.SetCheck(false);
		}
		else
		{
			m_rbF11Ctrl.SetCheck(true);
			m_rbF10Ctrl.SetCheck(false);
		}
		//TOPMOST		
		if(pApp->m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("TOPMOST"), 0) == 1)
		{
			CheckDlgButton(chbTopMost, TRUE);
		}
		else
		{
			CheckDlgButton(chbTopMost, FALSE);
		}

		//SHOW AT START		
		if(pApp->m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), 1) == 1)
		{
			CheckDlgButton(chbShowAtStart, TRUE);
		}
		else
		{
			CheckDlgButton(chbShowAtStart, FALSE);
		}

		//TOPMOST		
		if(pApp->m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("REPEAT"), 0) == 1)
		{
			CheckDlgButton(chbRepeat, TRUE);
		}
		else
		{
			CheckDlgButton(chbRepeat, FALSE);
		}
	}
}

void COptionTab::OnbtnSave() 
{
	// TODO: Add your control notification handler code here
	setConfigValue(true);
}

void COptionTab::OnbtnDefault() 
{
	// TODO: Add your control notification handler code here
	//setConfigValue(false);
	createConfigFile();
	setConfigValue(false);
}

void COptionTab::OnrbF10() 
{
	// TODO: Add your control notification handler code here
	if(IsDlgButtonChecked(rbF10))	
	{
		CACSApp* pApp = (CACSApp*)AfxGetApp();
		pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F10"));		
	}	
}

void COptionTab::OnrbF11() 
{
	// TODO: Add your control notification handler code here
	if(IsDlgButtonChecked(rbF11))	
	{
		CACSApp* pApp = (CACSApp*)AfxGetApp();
		pApp->m_xmlSetting.SetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F11"));		
	}
}

void COptionTab::OnchbTopMost()
{
	// TODO: Add your control notification handler code here
	
	//CACView* pMainDlg = (CACView*) AfxGetApp()->m_pMainWnd;
	CACSApp* pApp = (CACSApp*)AfxGetApp();
	if(IsDlgButtonChecked(chbTopMost))	
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), 1);
		//SET TOPMOST		
		SetTopMost(AfxGetMainWnd()->m_hWnd, TRUE);
	}
	else
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), 0);
		SetTopMost(AfxGetMainWnd()->m_hWnd, FALSE);
	}
	
}

void COptionTab::SetTopMost( HWND hWnd, const BOOL TopMost)
{
	ASSERT( ::IsWindow( hWnd ));
	HWND hWndInsertAfter = (TopMost ? HWND_TOPMOST : HWND_NOTOPMOST );
	::SetWindowPos( hWnd, hWndInsertAfter, 0, 0 , 0 , 0, SWP_NOMOVE | SWP_NOSIZE );
}

void COptionTab::OnchbShowAtStart() 
{
	// TODO: Add your control notification handler code here
	CACSApp* pApp = (CACSApp*)AfxGetApp();
	if(IsDlgButtonChecked(chbShowAtStart))	
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), 1);
	}
	else
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), 0);
	}
	
}

void COptionTab::OnchbRepeat() 
{
	// TODO: Add your control notification handler code here
	CACSApp* pApp = (CACSApp*)AfxGetApp();
	if(IsDlgButtonChecked(chbRepeat))	
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("REPEAT"), 1);
		m_bIsRepeat = TRUE;
	}
	else
	{
		pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("REPEAT"), 0);
		m_bIsRepeat = FALSE;
	}
}
