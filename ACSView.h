// ACSView.h : interface of the CACSView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACSVIEW_H__2A41B1B2_9133_4006_A7DB_6B7EFBFC7B43__INCLUDED_)
#define AFX_ACSVIEW_H__2A41B1B2_9133_4006_A7DB_6B7EFBFC7B43__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MyTabCtrl.h"
#include "ACSDoc.h"
#include "BsDocTemplate.h"
#include "MouseManager.h"
#include "resource.h"       // main symbols
#include "MainTab.h"
#include "OptionTab.h"
#include "KeyManager.h"	// Added by ClassView
#include "FunctionHelper.h"	// Added by ClassView
#include "ScriptManager.h"	// Added by ClassView


class CRecordNote : public CObject
{
public:	
	//CString m_strTime;
	//CString m_strMessage;
	CString m_line;

public:
	bool m_bIsChecked;
	//CRecordNote(CString strTime, CString strMessage);
	CRecordNote(CString strLine);
};
//////////////////CView class
//globle function:

UINT ThreadRunningTimeChecking(PVOID pParam);
BOOL CheckTime();

// CDailyNoteDlg dialog
typedef bool (*MYPROCRECNOTE)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath);
typedef bool (*DELPROC)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, int index);
typedef bool (*ADDPROC)(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote, CString filePath, CString strLineAdd);


//hook key board
typedef bool (*MYKBPROC)(HHOOK hHook);
typedef bool (*MYKBPROC1)();
typedef bool (*MYKBPROC2)(bool);
typedef bool (*MYKBPROC3)(HWND ins_hWndRecMsg);//setHWNDRecMsg
//hook mouse
typedef bool (*MYMOUSEPROC)(HHOOK hHook);
typedef bool (*MYMOUSEPROC2)(HWND ins_hMouseHook);


class CACSView : public CFormView
{
protected: // create from serialization only
	CACSView();
	DECLARE_DYNCREATE(CACSView)

public:
	//{{AFX_DATA(CACSView)
	enum { IDD = IDD_ACS_FORM };
	CButton	m_chbMoveMouseBeforClickCtrl;
	CButton	m_chbSetActiveCtrl;
	MyTabCtrl	m_tbCtrl;
	//}}AFX_DATA

// Attributes
public:
	CACSDoc* GetDocument();	
	//MOUSE HOOK
	void setUnHookMouse();
	bool setHookMouse();
	//BsDocTemplate* GetDocument();
	void SetStringNotify(CString strMsg, CString strSequence);
	CString m_strMsgNotify;
	void setUnHookKeyBoard();
	bool setHookKeyBoard();
	//for systray
	void myShowContextNotMenu();
	int TrayMessage(DWORD dwMessage);
	int TrayMessage(DWORD dwMessage, CString strMsg);
	void myShowDlg(bool ins_isShow);
	bool m_visible;

	void SetItemBkColor(int indexRow, COLORREF colorToSet);
	void StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior);
	void MyDeleteItemDLL(int index);
	void MyAddItemDLL(CString strLineAdd);
	void MyClearMem();
	void MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote);
	void MyUnUseDLL();
	void MyUseDLL();
	void MyGetArrRecordNote();
	CTypedPtrArray<CObArray, CRecordNote*> m_RecordNoteArray;

	//status variable
	enum TimerStatus {
       MYSTOP = 0,
       MYRUN = 1,
       MYPAUSE = 2,
    };
//	TimerStatus m_myStatus;
	
	//thread timer
	static UINT MyTimer(LPVOID pParam);
	CWinThread *m_pThreadSriptRunning;

	void MyReRunTimer();
	void MyStartTimer();

	//thread checking time
	CWinThread *m_pThreadCheckingTime;



	CMouseManager m_mouseManager;
private:
	BOOL m_bIsResizeToFit;
	CFont m_font;

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CACSView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInitialUpdate(); // called first time after construct
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
public:
	void MySetCurrentCheckingTime();
	void ReSetThreadCheckingTime();
	void RefreshThread();
	void ResizeFrameToFit();
	void MyUnUseDllKMC();
	void MyUseDllKMC();
	CScriptManager m_scriptManager;
	bool RunScriptAll();
	bool RunScriptByIndex(int index);
	bool OnKeyboardProc(UINT message, WPARAM wParam, LPARAM lParam);
	bool OnMouseProc(UINT message, WPARAM wParam, LPARAM lParam);
	CKeyManager m_keyManager;
	bool m_bIsRecording;
	COptionTab* GetOptionTab();
	CMainTab* GetMainTab();
	void setStopRecordHook();
	void setRecordHook();
	virtual ~CACSView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	HICON m_hIcon;
	HINSTANCE hUpdateRecordDll;

	//KEYBOARD HOOK
	HHOOK oldKeyBoardHook;	
	HINSTANCE hmeDll;
	MYKBPROC pSetHHook;
	MYKBPROC1 pSetHHook1;
	MYKBPROC2 pSetHHook2;
	MYKBPROC3 pSetHWNDRectMsg;
	HMODULE hModuleKeyboard; //key
	//MOUSE HOOK
	HINSTANCE hMMsDLL;
	HHOOK oldMouseHook;
	MYMOUSEPROC pSetHMHook;
	MYMOUSEPROC2 pSetHWNDRectMsgMouse;
	HMODULE hModuleMouse; //mouse



// Generated message map functions
protected:
	//{{AFX_MSG(CACSView)
	afx_msg void OnbtnHide();
	afx_msg void OnpmMitShow();
	afx_msg void OnpmMitExit();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCheckTime(WPARAM, LPARAM);
	afx_msg void OnbtnExit();
	afx_msg LRESULT OnScriptFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnScriptRunningIndex(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnScriptRepeat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTimeRemain(WPARAM wParam, LPARAM lParam);
	afx_msg void OnToolSchedule();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ACSView.cpp
inline CACSDoc* CACSView::GetDocument()
   { return (CACSDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACSVIEW_H__2A41B1B2_9133_4006_A7DB_6B7EFBFC7B43__INCLUDED_)
