// WinStartup.cpp : implementation file
//

#include "stdafx.h"
//#include "ArcKey.h"
#include "WinStartup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWinStartup
const TCHAR KEY_STARTUP[] = { _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run") };

CWinStartup::CWinStartup()
{
}

CWinStartup::~CWinStartup()
{
}

/*
BEGIN_MESSAGE_MAP(CWinStartup, CWnd)
	//{{AFX_MSG_MAP(CWinStartup)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
*/


/////////////////////////////////////////////////////////////////////////////
// CWinStartup message handlers

bool CWinStartup::AddApp(HINSTANCE hInst, LPCTSTR lpszName, StartupUser user)
{
	TCHAR szPath[MAX_PATH];
	GetCurrentAppPath(hInst, szPath, sizeof(szPath));

	return AddApp(lpszName, szPath, user);
}

bool CWinStartup::AddApp(LPCTSTR lpszName, LPCTSTR lpszPath, StartupUser user)
{
	HKEY hRootKey;

	if (user == CurrentUser)
	{
		hRootKey = HKEY_CURRENT_USER;
	}
	else
	{
		hRootKey = HKEY_LOCAL_MACHINE;
	}

	HKEY hKey = 0;
	HRESULT hr = RegOpenKey(hRootKey, KEY_STARTUP, &hKey);

	if (hr == ERROR_SUCCESS)
	{
		hr = RegSetValueEx(hKey, lpszName, 0, REG_SZ, (const BYTE*)lpszPath, _tcsclen(lpszPath));

		RegCloseKey(hKey);
	}

	return (hr == ERROR_SUCCESS);
}

bool CWinStartup::RemoveApp(LPCTSTR lpszName, StartupUser user)
{
	HKEY hRootKey;

	if (user == CurrentUser)
	{
		hRootKey = HKEY_CURRENT_USER;
	}
	else
	{
		hRootKey = HKEY_LOCAL_MACHINE;
	}

	HKEY hKey = 0;
	HRESULT hr = RegOpenKey(hRootKey, KEY_STARTUP, &hKey);

	if (hr == ERROR_SUCCESS)
	{
		hr = RegDeleteValue(hKey, lpszName);

		RegCloseKey(hKey);
	}

	return (hr == ERROR_SUCCESS); 
}

LPCTSTR CWinStartup::GetCurrentAppPath(HINSTANCE hInst, LPTSTR lpszPath, int nSize)
{
	GetModuleFileName(hInst, lpszPath, nSize);

	return lpszPath;
}

bool CWinStartup::isAppAdded(HINSTANCE hInst, LPCTSTR lpszName, StartupUser user)
{
	TCHAR szPath[MAX_PATH];
//	int nSize;
	GetCurrentAppPath(hInst, szPath, sizeof(szPath));

	HKEY hRootKey;

	if (user == CurrentUser)
	{
		hRootKey = HKEY_CURRENT_USER;
	}
	else
	{
		hRootKey = HKEY_LOCAL_MACHINE;
	}

	HKEY hKey = 0;
	HRESULT hr = RegOpenKey(hRootKey, KEY_STARTUP, &hKey);
	if (hr == ERROR_SUCCESS)
	{
		//hr = RegQueryValueEx(hKey, lpszName, 0, REG_SZ, (BYTE*)szPath, &nSize);
		DWORD keytype;
		char data[200]="";
		DWORD bread=200;
		hr = RegQueryValueEx(hKey,lpszName,NULL,&keytype,(BYTE*)&data,&bread);
		//RegCloseKey(hkey);

		RegCloseKey(hKey);
	}
	return (hr == ERROR_SUCCESS);
}
