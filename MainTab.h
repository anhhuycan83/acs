#if !defined(AFX_MAINTAB_H__8010396A_690A_40EE_AE51_A48AE757A00A__INCLUDED_)
#define AFX_MAINTAB_H__8010396A_690A_40EE_AE51_A48AE757A00A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MainTab.h : header file
//

#include "ReportCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CMainTab dialog

class CMainTab : public CDialog
{
// Construction
public:
	CMainTab(CWnd* pParent = NULL);   // standard constructor
	void MyRefreshSTT();

public: //member variables
	void RunAction();
	void StopAction();
	void RecordAction();
	void RefreshStatusButton();

// Dialog Data
	//{{AFX_DATA(CMainTab)
	enum { IDD = IDD_DIALOG1 };
	CButton	m_btnRunCtrl;
	CButton	m_btnStopCtrl;
	CButton	m_btnRecordCtrl;
	CReportCtrl	m_wndList;
	CString	m_txtWAIFValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainTab)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMainTab)
	afx_msg void OnbtnAdd();
	afx_msg void OnbtnDelete();
	afx_msg void OnbtnEdit();
	virtual BOOL OnInitDialog();
	afx_msg void OnbtnRecord();
	afx_msg void OnbtnStop();
	afx_msg void OnbtnRun();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINTAB_H__8010396A_690A_40EE_AE51_A48AE757A00A__INCLUDED_)
