// KeyManager.h: interface for the CKeyManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KEYMANAGER_H__E51BFBC5_3FF2_4EC1_B427_92297675CD04__INCLUDED_)
#define AFX_KEYMANAGER_H__E51BFBC5_3FF2_4EC1_B427_92297675CD04__INCLUDED_

#include "HighTime.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ACSDoc.h"
#include "KeyWindowAction.h"	// Added by ClassView

class CKeyManager  
{
public:
	CKeyManager();

	virtual ~CKeyManager();

public:
	CKeyPress* GetKeyPress(WPARAM wParam, LPARAM lParam);
	void OnKey(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);//down or up
	CKeyWindowAction* m_pkeyWindowAction;
	void SetPoiterDocument(CACSDoc* pDoc);
	CACSDoc* m_pDoc;

private:
	CHighTime m_timePre;
	HWND m_hWndOld;
};

#endif // !defined(AFX_KEYMANAGER_H__E51BFBC5_3FF2_4EC1_B427_92297675CD04__INCLUDED_)
