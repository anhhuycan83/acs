// MouseManger.h: interface for the CMouseManger class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MOUSEMANGER_H__FF5B4664_08D3_49F5_9CFC_5B2F6B8BCB2E__INCLUDED_)
#define AFX_MOUSEMANGER_H__FF5B4664_08D3_49F5_9CFC_5B2F6B8BCB2E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "BsDocTemplate.h"
//#include "ACSView.h"

#define BULLSEYE_CENTER_X_OFFSET		15
#define BULLSEYE_CENTER_Y_OFFSET		18

class CMouseManger  
{
public:
	HINSTANCE	g_hInst;
	HWND		g_hwndMainWnd;
	HANDLE		g_hApplicationMutex;
	DWORD		g_dwLastError;
	BOOL		g_bStartSearchWindow;
	HCURSOR		g_hCursorSearchWindow;
	HCURSOR		g_hCursorPrevious;
//	HBITMAP		g_hBitmapFinderToolFilled;
//	HBITMAP		g_hBitmapFinderToolEmpty;
	HWND		g_hwndFoundWindow;
	HPEN		g_hRectanglePen;

	////all functions
	long StartSearchWindowDialog (HWND hwndMain);
	BOOL CheckWindowValidity (HWND hwndDialog, HWND hwndToCheck);
	long DoMouseMove 
(
  HWND hwndDialog, 
  UINT message, 
  WPARAM wParam, 
  LPARAM lParam
);
	long DoMouseUp
(
  HWND hwndDialog, 
  UINT message, 
  WPARAM wParam, 
  LPARAM lParam
);
	BOOL SetFinderToolImage (HWND hwndDialog, BOOL bSet);
	BOOL MoveCursorPositionToBullsEye (HWND hwndDialog);
	long SearchWindow (HWND hwndDialog);
	long DisplayInfoOnFoundWindow (HWND hwndDialog, HWND hwndFoundWindow);
	long RefreshWindow (HWND hwndWindowToBeRefreshed);
	long HighlightFoundWindow (HWND hwndDialog, HWND hwndFoundWindow);
public:
	CMouseManger();
	//CMouseManger(CACSView* , BsDocTemplate* );
	//CMouseManger(int x, int y);
	virtual ~CMouseManger();

private:
	//CACSView* m_pView;
//	BsDocTemplate* m_pDoc;
};

#endif // !defined(AFX_MOUSEMANGER_H__FF5B4664_08D3_49F5_9CFC_5B2F6B8BCB2E__INCLUDED_)
