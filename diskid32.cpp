// diskid32.cpp: implementation of the diskid32 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "acs.h"
#include "diskid32.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

diskid32::diskid32()
{

}

diskid32::~diskid32()
{

}

//  diskid32.cpp


//  for displaying the details of hard drives in a command window


//  06/11/00  Lynn McGuire  written with many contributions from others,
//                            IDE drives only under Windows NT/2K and 9X,
//                            maybe SCSI drives later
//  11/20/03  Lynn McGuire  added ReadPhysicalDriveInNTWithZeroRights
//  10/26/05  Lynn McGuire  fix the flipAndCodeBytes function
//  01/22/08  Lynn McGuire  incorporate changes from Gonzalo Diethelm,
//                             remove media serial number code since does 
//                             not work on USB hard drives or thumb drives
//  01/29/08  Lynn McGuire  add ReadPhysicalDriveInNTUsingSmart


#define PRINTING_TO_CONSOLE_ALLOWED




	//  special include from the MS DDK
//#include "c:\win2kddk\inc\ddk\ntddk.h"
//#include "c:\win2kddk\inc\ntddstor.h"


#define  TITLE   _T("DiskId32")


char HardDriveSerialNumber [1024];
char HardDriveModelNumber [1024];

int PRINT_DEBUG = true;


static void dump_buffer (const char* title,
			const unsigned char* buffer,
			int len);


void WriteConstantString (char *entry, char *string)
{
}





   // Define global buffers.
BYTE IdOutCmd [sizeof (SENDCMDOUTPARAMS) + IDENTIFY_BUFFER_SIZE - 1];


char *ConvertToString (DWORD diskdata [256],
		       int firstIndex,
		       int lastIndex,
		       char* buf);
//void PrintIdeInfo (int drive, DWORD diskdata [256]);
//BOOL DoIDENTIFY (HANDLE, PSENDCMDINPARAMS, PSENDCMDOUTPARAMS, BYTE, BYTE,
//                 PDWORD);


   //  Max number of drives assuming primary/secondary, master/slave topology
#define  MAX_IDE_DRIVES  16


int diskid32::ReadPhysicalDriveInNTWithAdminRights (void)
{
   int done = FALSE;
   int drive = 0;

   for (drive = 0; drive < MAX_IDE_DRIVES; drive++)
   {
      HANDLE hPhysicalDriveIOCTL = 0;

         //  Try to get a handle to PhysicalDrive IOCTL, report failure
         //  and exit if can't.
      TCHAR driveName [256];

      _stprintf(driveName, _T("\\\\.\\PhysicalDrive%d"), drive);

         //  Windows NT, Windows 2000, must have admin rights
      hPhysicalDriveIOCTL = CreateFile (driveName,
                               GENERIC_READ | GENERIC_WRITE, 
                               FILE_SHARE_READ | FILE_SHARE_WRITE , NULL,
                               OPEN_EXISTING, 0, NULL);
      // if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      //    fprintf (stderr, "Unable to open physical drive %d, error code: 0x%lX\n",
      //            drive, GetLastError ());

      if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      {
#ifdef PRINTING_TO_CONSOLE_ALLOWED
         if (PRINT_DEBUG) 
			 fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithAdminRights ERROR"
					 "\nCreateFile(%s) returned INVALID_HANDLE_VALUE\n",
		 			 __LINE__, driveName);
#endif
      }
      else
      {
         GETVERSIONOUTPARAMS VersionParams;
         DWORD               cbBytesReturned = 0;

            // Get the version, etc of PhysicalDrive IOCTL
         memset ((void*) &VersionParams, 0, sizeof(VersionParams));

         if ( ! DeviceIoControl (hPhysicalDriveIOCTL, DFP_GET_VERSION,
                   NULL, 
                   0,
                   &VersionParams,
                   sizeof(VersionParams),
                   &cbBytesReturned, NULL) )
         {         
#ifdef PRINTING_TO_CONSOLE_ALLOWED
            if (PRINT_DEBUG)
            {
	           DWORD err = GetLastError ();
	           fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithAdminRights ERROR"
		               "\nDeviceIoControl(%d, DFP_GET_VERSION) returned 0, error is %d\n",
		               __LINE__, (int) hPhysicalDriveIOCTL, (int) err);
		    }
#endif
         }

            // If there is a IDE device at number "i" issue commands
            // to the device
         if (VersionParams.bIDEDeviceMap <= 0)
         {
#ifdef PRINTING_TO_CONSOLE_ALLOWED
            if (PRINT_DEBUG)
	            fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithAdminRights ERROR"
		                "\nNo device found at position %d (%d)\n",
		                __LINE__, (int) drive, (int) VersionParams.bIDEDeviceMap);
#endif
         }
         else
         {
            BYTE             bIDCmd = 0;   // IDE or ATAPI IDENTIFY cmd
            SENDCMDINPARAMS  scip;
            //SENDCMDOUTPARAMS OutCmd;

			   // Now, get the ID sector for all IDE devices in the system.
               // If the device is ATAPI use the IDE_ATAPI_IDENTIFY command,
               // otherwise use the IDE_ATA_IDENTIFY command
            bIDCmd = (VersionParams.bIDEDeviceMap >> drive & 0x10) ? \
                      IDE_ATAPI_IDENTIFY : IDE_ATA_IDENTIFY;

            memset (&scip, 0, sizeof(scip));
            memset (IdOutCmd, 0, sizeof(IdOutCmd));

            if ( DoIDENTIFY (hPhysicalDriveIOCTL, 
                       &scip, 
                       (PSENDCMDOUTPARAMS)&IdOutCmd, 
                       (BYTE) bIDCmd,
                       (BYTE) drive,
                       &cbBytesReturned))
            {
               DWORD diskdata [256];
               int ijk = 0;
               USHORT *pIdSector = (USHORT *)
                             ((PSENDCMDOUTPARAMS) IdOutCmd) -> bBuffer;

               for (ijk = 0; ijk < 256; ijk++)
                  diskdata [ijk] = pIdSector [ijk];

               PrintIdeInfo (drive, diskdata);

               done = TRUE;
            }
	    }

         CloseHandle (hPhysicalDriveIOCTL);
      }
   }

   return done;
}




int diskid32::ReadPhysicalDriveInNTUsingSmart (void)
{
   int done = FALSE;
   int drive = 0;

   for (drive = 0; drive < MAX_IDE_DRIVES; drive++)
   {
      HANDLE hPhysicalDriveIOCTL = 0;

         //  Try to get a handle to PhysicalDrive IOCTL, report failure
         //  and exit if can't.
      TCHAR driveName [256];

      _stprintf(driveName, _T("\\\\.\\PhysicalDrive%d"), drive);

         //  Windows NT, Windows 2000, Windows Server 2003, Vista
      hPhysicalDriveIOCTL = CreateFile (driveName,
                               GENERIC_READ | GENERIC_WRITE, 
                               FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, 
							   NULL, OPEN_EXISTING, 0, NULL);
      // if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      //    fprintf (stderr, "Unable to open physical drive %d, error code: 0x%lX\n",
      //            drive, GetLastError ());

      if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      {
#ifdef PRINTING_TO_CONSOLE_ALLOWED
         if (PRINT_DEBUG) 
			 fprintf (stderr, "\n%d ReadPhysicalDriveInNTUsingSmart ERROR"
					 "\nCreateFile(%s) returned INVALID_HANDLE_VALUE\n"
					 "Error Code %d\n",
		 			 __LINE__, driveName, GetLastError ());
#endif
      }
      else
      {
         GETVERSIONINPARAMS GetVersionParams;
         DWORD cbBytesReturned = 0;

            // Get the version, etc of PhysicalDrive IOCTL
         memset ((void*) & GetVersionParams, 0, sizeof(GetVersionParams));

         if ( ! DeviceIoControl (hPhysicalDriveIOCTL, SMART_GET_VERSION,
                   NULL, 
                   0,
     			   &GetVersionParams, sizeof (GETVERSIONINPARAMS),
				   &cbBytesReturned, NULL) )
         {         
#ifdef PRINTING_TO_CONSOLE_ALLOWED
            if (PRINT_DEBUG)
            {
	           DWORD err = GetLastError ();
	           fprintf (stderr, "\n%d ReadPhysicalDriveInNTUsingSmart ERROR"
		               "\nDeviceIoControl(%d, SMART_GET_VERSION) returned 0, error is %d\n",
		               __LINE__, (int) hPhysicalDriveIOCTL, (int) err);
		    }
#endif
         }
         else
         {
			 	// Print the SMART version
           	// PrintVersion (& GetVersionParams);
	           // Allocate the command buffer
			ULONG CommandSize = sizeof(SENDCMDINPARAMS) + IDENTIFY_BUFFER_SIZE;
        	PSENDCMDINPARAMS Command = (PSENDCMDINPARAMS) malloc (CommandSize);
	           // Retrieve the IDENTIFY data
	           // Prepare the command
#define ID_CMD          0xEC            // Returns ID sector for ATA
			Command -> irDriveRegs.bCommandReg = ID_CMD;
			DWORD BytesReturned = 0;
	        if ( ! DeviceIoControl (hPhysicalDriveIOCTL, 
				                    SMART_RCV_DRIVE_DATA, Command, sizeof(SENDCMDINPARAMS),
									Command, CommandSize,
									&BytesReturned, NULL) )
            {
		           // Print the error
		        //PrintError ("SMART_RCV_DRIVE_DATA IOCTL", GetLastError());
	        } 
			else
			{
        	       // Print the IDENTIFY data
                DWORD diskdata [256];
                USHORT *pIdSector = (USHORT *)
                             (PIDENTIFY_DATA) ((PSENDCMDOUTPARAMS) Command) -> bBuffer;

                for (int ijk = 0; ijk < 256; ijk++)
                   diskdata [ijk] = pIdSector [ijk];

                PrintIdeInfo (drive, diskdata);
                done = TRUE;
			}
	           // Done
            CloseHandle (hPhysicalDriveIOCTL);
			free (Command);
		 }
      }
   }

   return done;
}



	//  function to decode the serial numbers of IDE hard drives
	//  using the IOCTL_STORAGE_QUERY_PROPERTY command 
char * diskid32::flipAndCodeBytes (const char * str,
			 int pos,
			 int flip,
			 char * buf)
{
   int i;
   int j = 0;
   int k = 0;

   buf [0] = '\0';
   if (pos <= 0)
      return buf;

   if ( ! j)
   {
      char p = 0;

      // First try to gather all characters representing hex digits only.
      j = 1;
      k = 0;
      buf[k] = 0;
      for (i = pos; j && str[i] != '\0'; ++i)
      {
	 char c = tolower(str[i]);

	 if (isspace(c))
	    c = '0';

	 ++p;
	 buf[k] <<= 4;

	 if (c >= '0' && c <= '9')
	    buf[k] |= (unsigned char) (c - '0');
	 else if (c >= 'a' && c <= 'f')
	    buf[k] |= (unsigned char) (c - 'a' + 10);
	 else
	 {
	    j = 0;
	    break;
	 }

	 if (p == 2)
	 {
	    if (buf[k] != '\0' && ! isprint(buf[k]))
	    {
	       j = 0;
	       break;
	    }
	    ++k;
	    p = 0;
	    buf[k] = 0;
	 }

      }
   }

   if ( ! j)
   {
      // There are non-digit characters, gather them as is.
      j = 1;
      k = 0;
      for (i = pos; j && str[i] != '\0'; ++i)
      {
	     char c = str[i];

	     if ( ! isprint(c))
	     {
	        j = 0;
	        break;
	     }

	     buf[k++] = c;
      }
   }

   if ( ! j)
   {
      // The characters are not there or are not printable.
      k = 0;
   }

   buf[k] = '\0';

   if (flip)
      // Flip adjacent characters
      for (j = 0; j < k; j += 2)
      {
	     char t = buf[j];
	     buf[j] = buf[j + 1];
	     buf[j + 1] = t;
      }

   // Trim any beginning and end space
   i = j = -1;
   for (k = 0; buf[k] != '\0'; ++k)
   {
      if (! isspace(buf[k]))
      {
	     if (i < 0)
	        i = k;
	     j = k;
      }
   }

   if ((i >= 0) && (j >= 0))
   {
      for (k = i; (k <= j) && (buf[k] != '\0'); ++k)
         buf[k - i] = buf[k];
      buf[k - i] = '\0';
   }

   return buf;
}


int diskid32::ReadPhysicalDriveInNTWithZeroRights (void)
{
	//CLEAR SAVED DATE FOR SERIAL 
	strcpy (HardDriveSerialNumber, "");
	strcpy (HardDriveModelNumber, "");

   int done = FALSE;
   int drive = 0;
   
   for (drive = 0; drive < MAX_IDE_DRIVES; drive++)
   {
      HANDLE hPhysicalDriveIOCTL = 0;

         //  Try to get a handle to PhysicalDrive IOCTL, report failure
         //  and exit if can't.
      TCHAR driveName [256];

      _stprintf(driveName, _T("\\\\.\\PhysicalDrive%d"), drive);

         //  Windows NT, Windows 2000, Windows XP - admin rights not required
      hPhysicalDriveIOCTL = CreateFile (driveName, 0,
                               FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                               OPEN_EXISTING, 0, NULL);
      if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      {
#ifdef PRINTING_TO_CONSOLE_ALLOWED
         if (PRINT_DEBUG)
             fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithZeroRights ERROR"
		             "\nCreateFile(%s) returned INVALID_HANDLE_VALUE\n",
		             __LINE__, driveName);
#endif
      }
      else
      {
		 STORAGE_PROPERTY_QUERY query;
         DWORD cbBytesReturned = 0;
		 char buffer [10000];

         memset ((void *) & query, 0, sizeof (query));
		 query.PropertyId = StorageDeviceProperty;
		 query.QueryType = PropertyStandardQuery;

		 memset (buffer, 0, sizeof (buffer));

         if ( DeviceIoControl (hPhysicalDriveIOCTL, IOCTL_STORAGE_QUERY_PROPERTY,
                   & query,
                   sizeof (query),
				   & buffer,
				   sizeof (buffer),
                   & cbBytesReturned, NULL) )
         {         
			 STORAGE_DEVICE_DESCRIPTOR * descrip = (STORAGE_DEVICE_DESCRIPTOR *) & buffer;
			 char serialNumber [1000];
			 char modelNumber [1000];
             char vendorId [1000];
	         char productRevision [1000];

#ifdef PRINTING_TO_CONSOLE_ALLOWED
             if (PRINT_DEBUG)
             {
                 fprintf (stderr, "\n%d STORAGE_DEVICE_DESCRIPTOR contents for drive %d\n"
		                 "                Version: %ld\n"
		                 "                   Size: %ld\n"
		                 "             DeviceType: %02x\n"
		                 "     DeviceTypeModifier: %02x\n"
		                 "         RemovableMedia: %d\n"
		                 "        CommandQueueing: %d\n"
		                 "         VendorIdOffset: %4ld (0x%02lx)\n"
		                 "        ProductIdOffset: %4ld (0x%02lx)\n"
		                 "  ProductRevisionOffset: %4ld (0x%02lx)\n"
		                 "     SerialNumberOffset: %4ld (0x%02lx)\n"
		                 "                BusType: %d\n"
		                 "    RawPropertiesLength: %ld\n",
		                 __LINE__, drive,
			             (unsigned long) descrip->Version,
		                 (unsigned long) descrip->Size,
		                 (int) descrip->DeviceType,
		                 (int) descrip->DeviceTypeModifier,
		                 (int) descrip->RemovableMedia,
		                 (int) descrip->CommandQueueing,
		                 (unsigned long) descrip->VendorIdOffset,
		                 (unsigned long) descrip->VendorIdOffset,
		                 (unsigned long) descrip->ProductIdOffset,
		                 (unsigned long) descrip->ProductIdOffset,
		                 (unsigned long) descrip->ProductRevisionOffset,
		                 (unsigned long) descrip->ProductRevisionOffset,
		                 (unsigned long) descrip->SerialNumberOffset,
		                 (unsigned long) descrip->SerialNumberOffset,
		                 (int) descrip->BusType,
		                 (unsigned long) descrip->RawPropertiesLength);

	            dump_buffer ("Contents of RawDeviceProperties",
			                 (unsigned char*) descrip->RawDeviceProperties,
			                 descrip->RawPropertiesLength);

	            dump_buffer ("Contents of first 256 bytes in buffer",
			                 (unsigned char*) buffer, 256);
			 }
#endif
             flipAndCodeBytes (buffer,
                               descrip -> VendorIdOffset,
			                   0, vendorId );
	         flipAndCodeBytes (buffer,
			                   descrip -> ProductIdOffset,
			                   0, modelNumber );
	         flipAndCodeBytes (buffer,
			                   descrip -> ProductRevisionOffset,
			                   0, productRevision );
	         flipAndCodeBytes (buffer,
			                   descrip -> SerialNumberOffset,
			                   1, serialNumber );

			 if (0 == HardDriveSerialNumber [0] &&
						//  serial number must be alphanumeric
			            //  (but there can be leading spaces on IBM drives)
				   (isalnum (serialNumber [0]) || isalnum (serialNumber [19])))
			 {
				strcpy (HardDriveSerialNumber, serialNumber);
				strcpy (HardDriveModelNumber, modelNumber);
				done = TRUE;
			 }
#ifdef PRINTING_TO_CONSOLE_ALLOWED
             fprintf (stderr, "\n**** STORAGE_DEVICE_DESCRIPTOR for drive %d ****\n"
		             "Vendor Id = [%s]\n"
		             "Product Id = [%s]\n"
		             "Product Revision = [%s]\n"
		             "Serial Number = [%s]\n",
		             drive,
		             vendorId,
		             modelNumber,
		             productRevision,
		             serialNumber);
#endif
	           // Get the disk drive geometry.
	         memset (buffer, 0, sizeof(buffer));
	         if ( ! DeviceIoControl (hPhysicalDriveIOCTL,
			          IOCTL_DISK_GET_DRIVE_GEOMETRY_EX,
			          NULL,
			          0,
			          &buffer,
			          sizeof(buffer),
			          &cbBytesReturned,
			          NULL))
	         {
#ifdef PRINTING_TO_CONSOLE_ALLOWED
                if (PRINT_DEBUG)
	               fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithZeroRights ERROR"
		                   "|nDeviceIoControl(%s, IOCTL_DISK_GET_DRIVE_GEOMETRY_EX) returned 0",
		                   driveName);
#endif
          	 }
	         else
	         {         
	            DISK_GEOMETRY_EX* geom = (DISK_GEOMETRY_EX*) &buffer;
	            int fixed = (geom->Geometry.MediaType == FixedMedia);
	            __int64 size = geom->DiskSize.QuadPart;
				     
#ifdef PRINTING_TO_CONSOLE_ALLOWED
	            fprintf (stderr, "\n**** DISK_GEOMETRY_EX for drive %d ****\n"
		                "Disk is%s fixed\n"
		                "DiskSize = %I64d\n",
		                drive,
		                fixed ? "" : " NOT",
		                size);
#endif
	        }
         }
		 else
		 {
			 DWORD err = GetLastError ();
#ifdef PRINTING_TO_CONSOLE_ALLOWED
			 fprintf (stderr, "\nDeviceIOControl IOCTL_STORAGE_QUERY_PROPERTY error = %d\n", err);
#endif
		 }

         CloseHandle (hPhysicalDriveIOCTL);
      }
   }

   return done;
}


   // DoIDENTIFY
   // FUNCTION: Send an IDENTIFY command to the drive
   // bDriveNum = 0-3
   // bIDCmd = IDE_ATA_IDENTIFY or IDE_ATAPI_IDENTIFY
BOOL diskid32::DoIDENTIFY (HANDLE hPhysicalDriveIOCTL, PSENDCMDINPARAMS pSCIP,
                 PSENDCMDOUTPARAMS pSCOP, BYTE bIDCmd, BYTE bDriveNum,
                 PDWORD lpcbBytesReturned)
{
      // Set up data structures for IDENTIFY command.
   pSCIP -> cBufferSize = IDENTIFY_BUFFER_SIZE;
   pSCIP -> irDriveRegs.bFeaturesReg = 0;
   pSCIP -> irDriveRegs.bSectorCountReg = 1;
   //pSCIP -> irDriveRegs.bSectorNumberReg = 1;
   pSCIP -> irDriveRegs.bCylLowReg = 0;
   pSCIP -> irDriveRegs.bCylHighReg = 0;

      // Compute the drive number.
   pSCIP -> irDriveRegs.bDriveHeadReg = 0xA0 | ((bDriveNum & 1) << 4);

      // The command can either be IDE identify or ATAPI identify.
   pSCIP -> irDriveRegs.bCommandReg = bIDCmd;
   pSCIP -> bDriveNumber = bDriveNum;
   pSCIP -> cBufferSize = IDENTIFY_BUFFER_SIZE;

   return ( DeviceIoControl (hPhysicalDriveIOCTL, DFP_RECEIVE_DRIVE_DATA,
               (LPVOID) pSCIP,
               sizeof(SENDCMDINPARAMS) - 1,
               (LPVOID) pSCOP,
               sizeof(SENDCMDOUTPARAMS) + IDENTIFY_BUFFER_SIZE - 1,
               lpcbBytesReturned, NULL) );
}



int diskid32::ReadDrivePortsInWin9X (void)
{
   int done = FALSE;
   unsigned long int i = 0;

   HANDLE VxDHandle = 0;
   pt_IdeDInfo pOutBufVxD = 0;
   DWORD lpBytesReturned = 0;

		//  set the thread priority high so that we get exclusive access to the disk
   BOOL status =
		// SetThreadPriority (GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
		SetPriorityClass (GetCurrentProcess (), REALTIME_PRIORITY_CLASS);
		// SetPriorityClass (GetCurrentProcess (), HIGH_PRIORITY_CLASS);

#ifdef PRINTING_TO_CONSOLE_ALLOWED

   if (0 == status) 
	   // fprintf (stderr, "\nERROR: Could not SetThreadPriority, LastError: %d\n", GetLastError ());
	   fprintf (stderr, "\nERROR: Could not SetPriorityClass, LastError: %d\n", GetLastError ());

#endif

      // 1. Make an output buffer for the VxD
   rt_IdeDInfo info;
   pOutBufVxD = &info;

      // *****************
      // KLUDGE WARNING!!!
      // HAVE to zero out the buffer space for the IDE information!
      // If this is NOT done then garbage could be in the memory
      // locations indicating if a disk exists or not.
   ZeroMemory (&info, sizeof(info));

      // 1. Try to load the VxD
       //  must use the short file name path to open a VXD file
   //char StartupDirectory [2048];
   //char shortFileNamePath [2048];
   //char *p = NULL;
   //char vxd [2048];
      //  get the directory that the exe was started from
   //GetModuleFileName (hInst, (LPSTR) StartupDirectory, sizeof (StartupDirectory));
      //  cut the exe name from string
   //p = &(StartupDirectory [strlen (StartupDirectory) - 1]);
   //while (p >= StartupDirectory && *p && '\\' != *p) p--;
   //*p = '\0';   
   //GetShortPathName (StartupDirectory, shortFileNamePath, 2048);
   //sprintf (vxd, "\\\\.\\%s\\IDE21201.VXD", shortFileNamePath);
   //VxDHandle = CreateFile (vxd, 0, 0, 0,
   //               0, FILE_FLAG_DELETE_ON_CLOSE, 0);   
   VxDHandle = CreateFile (_T("\\\\.\\IDE21201.VXD"), 0, 0, 0,
							0, FILE_FLAG_DELETE_ON_CLOSE, 0);

   if (VxDHandle != INVALID_HANDLE_VALUE)
   {
         // 2. Run VxD function
      DeviceIoControl (VxDHandle, m_cVxDFunctionIdesDInfo,
					0, 0, pOutBufVxD, sizeof(pt_IdeDInfo), &lpBytesReturned, 0);

         // 3. Unload VxD
      CloseHandle (VxDHandle);
   }
   else
		MessageBox (NULL, _T("ERROR: Could not open IDE21201.VXD file"), 
					TITLE, MB_ICONSTOP);

      // 4. Translate and store data
   for (i=0; i<8; i++)
   {
      if((pOutBufVxD->DiskExists[i]) && (pOutBufVxD->IDEExists[i/2]))
      {
			DWORD diskinfo [256];
			for (int j = 0; j < 256; j++) 
				diskinfo [j] = pOutBufVxD -> DisksRawInfo [i * 256 + j];

            // process the information for this buffer
		   PrintIdeInfo (i, diskinfo);
			done = TRUE;
      }
   }

		//  reset the thread priority back to normal
   // SetThreadPriority (GetCurrentThread(), THREAD_PRIORITY_NORMAL);
   SetPriorityClass (GetCurrentProcess (), NORMAL_PRIORITY_CLASS);

   return done;
}





int diskid32::ReadIdeDriveAsScsiDriveInNT (void)
{
   int done = FALSE;
   int controller = 0;

   for (controller = 0; controller < 16; controller++)
   {
      HANDLE hScsiDriveIOCTL = 0;
      TCHAR driveName [256];

         //  Try to get a handle to PhysicalDrive IOCTL, report failure
         //  and exit if can't.
      _stprintf (driveName, _T("\\\\.\\Scsi%d:"), controller);

         //  Windows NT, Windows 2000, any rights should do
      hScsiDriveIOCTL = CreateFile (driveName,
                               GENERIC_READ | GENERIC_WRITE, 
                               FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                               OPEN_EXISTING, 0, NULL);
      // if (hScsiDriveIOCTL == INVALID_HANDLE_VALUE)
      //    fprintf (stderr, "Unable to open SCSI controller %d, error code: 0x%lX\n",
      //            controller, GetLastError ());

      if (hScsiDriveIOCTL != INVALID_HANDLE_VALUE)
      {
         int drive = 0;

         for (drive = 0; drive < 2; drive++)
         {
            char buffer [sizeof (SRB_IO_CONTROL) + SENDIDLENGTH];
            SRB_IO_CONTROL *p = (SRB_IO_CONTROL *) buffer;
            SENDCMDINPARAMS *pin =
                   (SENDCMDINPARAMS *) (buffer + sizeof (SRB_IO_CONTROL));
            DWORD dummy;
   
            memset (buffer, 0, sizeof (buffer));
            p -> HeaderLength = sizeof (SRB_IO_CONTROL);
            p -> Timeout = 10000;
            p -> Length = SENDIDLENGTH;
            p -> ControlCode = IOCTL_SCSI_MINIPORT_IDENTIFY;
            strncpy ((char *) p -> Signature, "SCSIDISK", 8);
  
            pin -> irDriveRegs.bCommandReg = IDE_ATA_IDENTIFY;
            pin -> bDriveNumber = drive;

            if (DeviceIoControl (hScsiDriveIOCTL, IOCTL_SCSI_MINIPORT, 
                                 buffer,
                                 sizeof (SRB_IO_CONTROL) +
                                         sizeof (SENDCMDINPARAMS) - 1,
                                 buffer,
                                 sizeof (SRB_IO_CONTROL) + SENDIDLENGTH,
                                 &dummy, NULL))
            {
               SENDCMDOUTPARAMS *pOut =
                    (SENDCMDOUTPARAMS *) (buffer + sizeof (SRB_IO_CONTROL));
               IDSECTOR *pId = (IDSECTOR *) (pOut -> bBuffer);
               if (pId -> sModelNumber [0])
               {
                  DWORD diskdata [256];
                  int ijk = 0;
                  USHORT *pIdSector = (USHORT *) pId;
          
                  for (ijk = 0; ijk < 256; ijk++)
                     diskdata [ijk] = pIdSector [ijk];

                  PrintIdeInfo (controller * 2 + drive, diskdata);

                  done = TRUE;
               }
            }
         }
         CloseHandle (hScsiDriveIOCTL);
      }
   }

   return done;
}



void diskid32::PrintIdeInfo (int drive, DWORD diskdata [256])
{

   char serialNumber [1024];
   char modelNumber [1024];
   char revisionNumber [1024];
   char bufferSize [32];

   __int64 sectors = 0;
   __int64 bytes = 0;

      //  copy the hard drive serial number to the buffer
   ConvertToString (diskdata, 10, 19, serialNumber);
   ConvertToString (diskdata, 27, 46, modelNumber);
   ConvertToString (diskdata, 23, 26, revisionNumber);
   sprintf (bufferSize, "%u", diskdata [21] * 512);

   if (0 == HardDriveSerialNumber [0] &&
       //  serial number must be alphanumeric
       //  (but there can be leading spaces on IBM drives)
       (isalnum (serialNumber [0]) || isalnum (serialNumber [19])))
   {
      strcpy (HardDriveSerialNumber, serialNumber);
      strcpy (HardDriveModelNumber, modelNumber);
   }

#ifdef PRINTING_TO_CONSOLE_ALLOWED

   fprintf (stderr, "\nDrive %d - ", drive);

   switch (drive / 2)
   {
      case 0: fprintf (stderr, "Primary Controller - ");
              break;
      case 1: fprintf (stderr, "Secondary Controller - ");
              break;
      case 2: fprintf (stderr, "Tertiary Controller - ");
              break;
      case 3: fprintf (stderr, "Quaternary Controller - ");
              break;
   }

   switch (drive % 2)
   {
      case 0: fprintf (stderr, " - Master drive\n\n");
              break;
      case 1: fprintf (stderr, " - Slave drive\n\n");
              break;
   }

   fprintf (stderr, "Drive Model Number________________: [%s]\n",
           modelNumber);
   fprintf (stderr, "Drive Serial Number_______________: [%s]\n",
           serialNumber);
   fprintf (stderr, "Drive Controller Revision Number__: [%s]\n",
           revisionNumber);

   fprintf (stderr, "Controller Buffer Size on Drive___: %s bytes\n",
           bufferSize);

   fprintf (stderr, "Drive Type________________________: ");
   if (diskdata [0] & 0x0080)
      fprintf (stderr, "Removable\n");
   else if (diskdata [0] & 0x0040)
      fprintf (stderr, "Fixed\n");
   else fprintf (stderr, "Unknown\n");
           
		//  calculate size based on 28 bit or 48 bit addressing
		//  48 bit addressing is reflected by bit 10 of word 83
	if (diskdata [83] & 0x400) 
		sectors = diskdata [103] * 65536I64 * 65536I64 * 65536I64 + 
					diskdata [102] * 65536I64 * 65536I64 + 
					diskdata [101] * 65536I64 + 
					diskdata [100];
	else
		sectors = diskdata [61] * 65536 + diskdata [60];
		//  there are 512 bytes in a sector
	bytes = sectors * 512;
	fprintf (stderr, "Drive Size________________________: %I64d bytes\n",
			bytes);

#endif  // PRINTING_TO_CONSOLE_ALLOWED

   char string1 [1000];
   sprintf (string1, "Drive%dModelNumber", drive);
   WriteConstantString (string1, modelNumber);

   sprintf (string1, "Drive%dSerialNumber", drive);
   WriteConstantString (string1, serialNumber);

   sprintf (string1, "Drive%dControllerRevisionNumber", drive);
   WriteConstantString (string1, revisionNumber);

   sprintf (string1, "Drive%dControllerBufferSize", drive);
   WriteConstantString (string1, bufferSize);

   sprintf (string1, "Drive%dType", drive);
   if (diskdata [0] & 0x0080)
      WriteConstantString (string1, "Removable");
   else if (diskdata [0] & 0x0040)
      WriteConstantString (string1, "Fixed");
   else
      WriteConstantString (string1, "Unknown");


}



char *diskid32::ConvertToString (DWORD diskdata [256],
		       int firstIndex,
		       int lastIndex,
		       char* buf)
{
   int index = 0;
   int position = 0;

      //  each integer has two characters stored in it backwards
   for (index = firstIndex; index <= lastIndex; index++)
   {
         //  get high byte for 1st character
      buf [position++] = (char) (diskdata [index] / 256);

         //  get low byte for 2nd character
      buf [position++] = (char) (diskdata [index] % 256);
   }

      //  end the string 
   buf[position] = '\0';

      //  cut off the trailing blanks
   for (index = position - 1; index > 0 && isspace(buf [index]); index--)
      buf [index] = '\0';

   return buf;
}



long diskid32::getHardDriveComputerID ()
{

   int done = FALSE;
   // char string [1024];
   __int64 id = 0;
   OSVERSIONINFO version;

   strcpy (HardDriveSerialNumber, "");

   memset (&version, 0, sizeof (version));
   version.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
   GetVersionEx (&version);
   if (version.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		  //  this works under WinNT4 or Win2K if you have admin rights
#ifdef PRINTING_TO_CONSOLE_ALLOWED
		fprintf (stderr, "\nTrying to read the drive IDs using physical access with admin rights\n");
#endif
		done = ReadPhysicalDriveInNTWithAdminRights ();

			//  this should work in WinNT or Win2K if previous did not work
			//  this is kind of a backdoor via the SCSI mini port driver into
			//     the IDE drives
#ifdef PRINTING_TO_CONSOLE_ALLOWED
		fprintf (stderr, "\nTrying to read the drive IDs using the SCSI back door\n");
#endif
		// if ( ! done) 
			done = ReadIdeDriveAsScsiDriveInNT ();

		  //  this works under WinNT4 or Win2K or WinXP if you have any rights
#ifdef PRINTING_TO_CONSOLE_ALLOWED
		fprintf (stderr, "\nTrying to read the drive IDs using physical access with zero rights\n");
#endif
		//if ( ! done)
			done = ReadPhysicalDriveInNTWithZeroRights ();

		  //  this works under WinNT4 or Win2K or WinXP or Windows Server 2003 or Vista if you have any rights
#ifdef PRINTING_TO_CONSOLE_ALLOWED
		fprintf (stderr, "\nTrying to read the drive IDs using Smart\n");
#endif
		//if ( ! done)
			done = ReadPhysicalDriveInNTUsingSmart ();
   }
   else
   {
         //  this works under Win9X and calls a VXD
      int attempt = 0;

         //  try this up to 10 times to get a hard drive serial number
      for (attempt = 0;
           attempt < 10 && ! done && 0 == HardDriveSerialNumber [0];
           attempt++)
         done = ReadDrivePortsInWin9X ();
   }

   if (HardDriveSerialNumber [0] > 0)
   {
      char *p = HardDriveSerialNumber;

      WriteConstantString ("HardDriveSerialNumber", HardDriveSerialNumber);

         //  ignore first 5 characters from western digital hard drives if
         //  the first four characters are WD-W
      if ( ! strncmp (HardDriveSerialNumber, "WD-W", 4)) 
         p += 5;
      for ( ; p && *p; p++)
      {
         if ('-' == *p) 
            continue;
         id *= 10;
         switch (*p)
         {
            case '0': id += 0; break;
            case '1': id += 1; break;
            case '2': id += 2; break;
            case '3': id += 3; break;
            case '4': id += 4; break;
            case '5': id += 5; break;
            case '6': id += 6; break;
            case '7': id += 7; break;
            case '8': id += 8; break;
            case '9': id += 9; break;
            case 'a': case 'A': id += 10; break;
            case 'b': case 'B': id += 11; break;
            case 'c': case 'C': id += 12; break;
            case 'd': case 'D': id += 13; break;
            case 'e': case 'E': id += 14; break;
            case 'f': case 'F': id += 15; break;
            case 'g': case 'G': id += 16; break;
            case 'h': case 'H': id += 17; break;
            case 'i': case 'I': id += 18; break;
            case 'j': case 'J': id += 19; break;
            case 'k': case 'K': id += 20; break;
            case 'l': case 'L': id += 21; break;
            case 'm': case 'M': id += 22; break;
            case 'n': case 'N': id += 23; break;
            case 'o': case 'O': id += 24; break;
            case 'p': case 'P': id += 25; break;
            case 'q': case 'Q': id += 26; break;
            case 'r': case 'R': id += 27; break;
            case 's': case 'S': id += 28; break;
            case 't': case 'T': id += 29; break;
            case 'u': case 'U': id += 30; break;
            case 'v': case 'V': id += 31; break;
            case 'w': case 'W': id += 32; break;
            case 'x': case 'X': id += 33; break;
            case 'y': case 'Y': id += 34; break;
            case 'z': case 'Z': id += 35; break;
         }                            
      }
   }

   id %= 100000000;
   if (strstr (HardDriveModelNumber, "IBM-"))
      id += 300000000;
   else if (strstr (HardDriveModelNumber, "MAXTOR") ||
            strstr (HardDriveModelNumber, "Maxtor"))
      id += 400000000;
   else if (strstr (HardDriveModelNumber, "WDC "))
      id += 500000000;
   else
      id += 600000000;

#ifdef PRINTING_TO_CONSOLE_ALLOWED

   CString s, s1, s2;
   s1 =HardDriveSerialNumber;
   s2 = HardDriveModelNumber;
   s.Format(_T("\nHard Drive Serial Number__________: %s\n\nHard Drive Model Number___________: %s\n\nComputer ID_______________________: %I64d\n"),
	   s1, s2, id);
   AfxMessageBox(s);
   fprintf (stderr, "\nHard Drive Serial Number__________: %s\n", 
           HardDriveSerialNumber);
   fprintf (stderr, "\nHard Drive Model Number___________: %s\n", 
           HardDriveModelNumber);
   fprintf (stderr, "\nComputer ID_______________________: %I64d\n", id);

#endif

   return (long) id;
} 


// GetMACAdapters.cpp : Defines the entry point for the console application.
//
// Author:	Khalid Shaikh [Shake@ShakeNet.com]
// Date:	April 5th, 2002
//
// This program fetches the MAC address of the localhost by fetching the 
// information through GetAdapatersInfo.  It does not rely on the NETBIOS
// protocol and the ethernet adapter need not be connect to a network.
//
// Supported in Windows NT/2000/XP
// Supported in Windows 95/98/Me
//
// Supports multiple NIC cards on a PC.




// Prints the MAC address stored in a 6 byte array to stdout
static void PrintMACaddress(unsigned char MACData[])
{

#ifdef PRINTING_TO_CONSOLE_ALLOWED

	fprintf (stderr, "\nMAC Address: %02X-%02X-%02X-%02X-%02X-%02X\n", 
		MACData[0], MACData[1], MACData[2], MACData[3], MACData[4], MACData[5]);

#endif

   char string [256];
   sprintf (string, "%02X-%02X-%02X-%02X-%02X-%02X", MACData[0], MACData[1], 
			   MACData[2], MACData[3], MACData[4], MACData[5]);
   WriteConstantString ("MACaddress", string);
}



// Fetches the MAC address and prints it
DWORD diskid32::GetMACaddress(void)
{
  DWORD MACaddress = 0;
  IP_ADAPTER_INFO AdapterInfo[16];       // Allocate information
                                         // for up to 16 NICs
  DWORD dwBufLen = sizeof(AdapterInfo);  // Save memory size of buffer

  DWORD dwStatus = GetAdaptersInfo(      // Call GetAdapterInfo
			AdapterInfo,                 // [out] buffer to receive data
			&dwBufLen);                  // [in] size of receive data buffer
  assert(dwStatus == ERROR_SUCCESS);  // Verify return value is
                                      // valid, no buffer overflow

  PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo; // Contains pointer to
                                               // current adapter info
  do {
	if (MACaddress == 0)
		MACaddress = pAdapterInfo->Address [5] + pAdapterInfo->Address [4] * 256 + 
					pAdapterInfo->Address [3] * 256 * 256 + 
					pAdapterInfo->Address [2] * 256 * 256 * 256;
	switch(pAdapterInfo->Type)
	{
	case MIB_IF_TYPE_OTHER:
		fprintf (stderr, "\nSome other type of network interface: ");
		break;
			case MIB_IF_TYPE_ETHERNET:
		fprintf (stderr, "\nAn Ethernet network interface: ");
		break;
	case 9://IF_TYPE_ISO88025_TOKENRING
		fprintf (stderr, "\nMIB_IF_TYPE_TOKENRING: ");
		break;
	case MIB_IF_TYPE_PPP:
		fprintf (stderr, "\nA PPP network interface: ");
		break;
	case MIB_IF_TYPE_LOOPBACK:
		fprintf (stderr, "\nA software loopback network interface: ");
		break;
	case MIB_IF_TYPE_SLIP:
		fprintf (stderr, "\nAn ATM network interface: ");
		break;
	case 71://IF_TYPE_IEEE80211
//MYDEL		Note  This adapter type is returned on Windows Vista and later. 
//MYDEL		On Windows Server 2003 and Windows XP , 
//MYDEL		an IEEE 802.11 wireless network interface returns an adapter type of MIB_IF_TYPE_ETHERNET.
		fprintf (stderr, "\nAn IEEE 802.11 wireless network interface: ");
		break;

	}
    PrintMACaddress(pAdapterInfo->Address); // Print MAC address
    pAdapterInfo = pAdapterInfo->Next;    // Progress through linked list
  }
  while(pAdapterInfo);                    // Terminate if last adapter
  
  return MACaddress;
}



static void dump_buffer (const char* title,
			const unsigned char* buffer,
			int len)
{
   int i = 0;
   int j;

   fprintf (stderr, "\n-- %s --\n", title);
   if (len > 0)
   {
      fprintf (stderr, "%8.8s ", " ");
      for (j = 0; j < 16; ++j)
      {
	    fprintf (stderr, " %2X", j);
      }
      fprintf (stderr, "  ");
      for (j = 0; j < 16; ++j)
      {
	    fprintf (stderr, "%1X", j);
      }
      fprintf (stderr, "\n");
   }
   while (i < len)
   {
      printf("%08x ", i);
      for (j = 0; j < 16; ++j)
      {
	 if ((i + j) < len)
	    fprintf (stderr, " %02x", (int) buffer[i +j]);
	 else
	    fprintf (stderr, "   ");
      }
      fprintf (stderr, "  ");
      for (j = 0; j < 16; ++j)
      {
	 if ((i + j) < len)
	    fprintf (stderr, "%c", isprint (buffer[i + j]) ? buffer [i + j] : '.');
	 else
	    fprintf (stderr, " ");
      }
      fprintf (stderr, "\n");
      i += 16;
   }
   fprintf (stderr, "-- DONE --\n");
}
  
//MYDELint main (int argc, char * argv [])
//MYDEL{
//MYDEL   fprintf (stderr, "To get all details use \"diskid32 /d\"\n");
//MYDEL
//MYDEL   if (argc > 1 && strstr (argv [1], "/d"))
//MYDEL      PRINT_DEBUG = true;
//MYDEL   else
//MYDEL      PRINT_DEBUG = false;
//MYDEL      
//MYDEL   long id = getHardDriveComputerID ();
//MYDEL
//MYDEL   GetMACaddress ();
//MYDEL
//MYDEL   return 0;
//MYDEL} 
//MYDEL
//MYDELchar* PrintMD5(uchar md5Digest[16])
//MYDEL{
//MYDEL	char chBuffer[256];
//MYDEL	char chEach[10];
//MYDEL	int nCount;
//MYDEL
//MYDEL	memset(chBuffer,0,256);
//MYDEL	memset(chEach, 0, 10);
//MYDEL
//MYDEL	for (nCount = 0; nCount < 16; nCount++)
//MYDEL	{
//MYDEL		sprintf(chEach, "%02x", md5Digest[nCount]);
//MYDEL		strncat(chBuffer, chEach, sizeof(chEach));
//MYDEL	}
//MYDEL
//MYDEL
//MYDEL	return chBuffer;// this memory need to be returned
//MYDEL}
BOOL diskid32::GetHddSerial(CString& hddSerial)
{
	//CLEAR SAVED DATE FOR SERIAL 
	strcpy (HardDriveSerialNumber, "");
	strcpy (HardDriveModelNumber, "");

	BOOL done = FALSE;
   int drive = 0;

   for (drive = 0; drive < MAX_IDE_DRIVES; drive++)
   {
      HANDLE hPhysicalDriveIOCTL = 0;

         //  Try to get a handle to PhysicalDrive IOCTL, report failure
         //  and exit if can't.
      TCHAR driveName [256];

      _stprintf(driveName, _T("\\\\.\\PhysicalDrive%d"), drive);

         //  Windows NT, Windows 2000, Windows XP - admin rights not required
      hPhysicalDriveIOCTL = CreateFile (driveName, 0,
                               FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                               OPEN_EXISTING, 0, NULL);
      if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
      {
//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL         if (PRINT_DEBUG)
//MYDEL             fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithZeroRights ERROR"
//MYDEL		             "\nCreateFile(%s) returned INVALID_HANDLE_VALUE\n",
//MYDEL		             __LINE__, driveName);
//MYDEL#endif
      }
      else
      {
		 STORAGE_PROPERTY_QUERY query;
         DWORD cbBytesReturned = 0;
		 char buffer [10000];

         memset ((void *) & query, 0, sizeof (query));
		 query.PropertyId = StorageDeviceProperty;
		 query.QueryType = PropertyStandardQuery;

		 memset (buffer, 0, sizeof (buffer));

         if ( DeviceIoControl (hPhysicalDriveIOCTL, IOCTL_STORAGE_QUERY_PROPERTY,
                   & query,
                   sizeof (query),
				   & buffer,
				   sizeof (buffer),
                   & cbBytesReturned, NULL) )
         {         
			 STORAGE_DEVICE_DESCRIPTOR * descrip = (STORAGE_DEVICE_DESCRIPTOR *) & buffer;
			 char serialNumber [1000];
			 char modelNumber [1000];
             char vendorId [1000];
	         char productRevision [1000];

//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL             if (PRINT_DEBUG)
//MYDEL             {
//MYDEL                 fprintf (stderr, "\n%d STORAGE_DEVICE_DESCRIPTOR contents for drive %d\n"
//MYDEL		                 "                Version: %ld\n"
//MYDEL		                 "                   Size: %ld\n"
//MYDEL		                 "             DeviceType: %02x\n"
//MYDEL		                 "     DeviceTypeModifier: %02x\n"
//MYDEL		                 "         RemovableMedia: %d\n"
//MYDEL		                 "        CommandQueueing: %d\n"
//MYDEL		                 "         VendorIdOffset: %4ld (0x%02lx)\n"
//MYDEL		                 "        ProductIdOffset: %4ld (0x%02lx)\n"
//MYDEL		                 "  ProductRevisionOffset: %4ld (0x%02lx)\n"
//MYDEL		                 "     SerialNumberOffset: %4ld (0x%02lx)\n"
//MYDEL		                 "                BusType: %d\n"
//MYDEL		                 "    RawPropertiesLength: %ld\n",
//MYDEL		                 __LINE__, drive,
//MYDEL			             (unsigned long) descrip->Version,
//MYDEL		                 (unsigned long) descrip->Size,
//MYDEL		                 (int) descrip->DeviceType,
//MYDEL		                 (int) descrip->DeviceTypeModifier,
//MYDEL		                 (int) descrip->RemovableMedia,
//MYDEL		                 (int) descrip->CommandQueueing,
//MYDEL		                 (unsigned long) descrip->VendorIdOffset,
//MYDEL		                 (unsigned long) descrip->VendorIdOffset,
//MYDEL		                 (unsigned long) descrip->ProductIdOffset,
//MYDEL		                 (unsigned long) descrip->ProductIdOffset,
//MYDEL		                 (unsigned long) descrip->ProductRevisionOffset,
//MYDEL		                 (unsigned long) descrip->ProductRevisionOffset,
//MYDEL		                 (unsigned long) descrip->SerialNumberOffset,
//MYDEL		                 (unsigned long) descrip->SerialNumberOffset,
//MYDEL		                 (int) descrip->BusType,
//MYDEL		                 (unsigned long) descrip->RawPropertiesLength);
//MYDEL
//MYDEL	            dump_buffer ("Contents of RawDeviceProperties",
//MYDEL			                 (unsigned char*) descrip->RawDeviceProperties,
//MYDEL			                 descrip->RawPropertiesLength);
//MYDEL
//MYDEL	            dump_buffer ("Contents of first 256 bytes in buffer",
//MYDEL			                 (unsigned char*) buffer, 256);
//MYDEL			 }
//MYDEL#endif
             flipAndCodeBytes (buffer,
                               descrip -> VendorIdOffset,
			                   0, vendorId );
	         flipAndCodeBytes (buffer,
			                   descrip -> ProductIdOffset,
			                   0, modelNumber );
	         flipAndCodeBytes (buffer,
			                   descrip -> ProductRevisionOffset,
			                   0, productRevision );
	         flipAndCodeBytes (buffer,
			                   descrip -> SerialNumberOffset,
			                   1, serialNumber );

			 if (0 == HardDriveSerialNumber [0] &&
						//  serial number must be alphanumeric
			            //  (but there can be leading spaces on IBM drives)
				   (isalnum (serialNumber [0]) || isalnum (serialNumber [19])))
			 {
				strcpy (HardDriveSerialNumber, serialNumber);
				strcpy (HardDriveModelNumber, modelNumber);
				hddSerial = serialNumber;
				done = TRUE;
			 }
			 
//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL             fprintf (stderr, "\n**** STORAGE_DEVICE_DESCRIPTOR for drive %d ****\n"
//MYDEL		             "Vendor Id = [%s]\n"
//MYDEL		             "Product Id = [%s]\n"
//MYDEL		             "Product Revision = [%s]\n"
//MYDEL		             "Serial Number = [%s]\n",
//MYDEL		             drive,
//MYDEL		             vendorId,
//MYDEL		             modelNumber,
//MYDEL		             productRevision,
//MYDEL		             serialNumber);
//MYDEL#endif
//MYDEL	           // Get the disk drive geometry.
//MYDEL	         memset (buffer, 0, sizeof(buffer));
//MYDEL	         if ( ! DeviceIoControl (hPhysicalDriveIOCTL,
//MYDEL			          IOCTL_DISK_GET_DRIVE_GEOMETRY_EX,
//MYDEL			          NULL,
//MYDEL			          0,
//MYDEL			          &buffer,
//MYDEL			          sizeof(buffer),
//MYDEL			          &cbBytesReturned,
//MYDEL			          NULL))
//MYDEL	         {
//MYDEL//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL//MYDEL                if (PRINT_DEBUG)
//MYDEL//MYDEL	               fprintf (stderr, "\n%d ReadPhysicalDriveInNTWithZeroRights ERROR"
//MYDEL//MYDEL		                   "|nDeviceIoControl(%s, IOCTL_DISK_GET_DRIVE_GEOMETRY_EX) returned 0",
//MYDEL//MYDEL		                   driveName);
//MYDEL//MYDEL#endif
//MYDEL          	 }
//MYDEL	         else
//MYDEL	         {         
//MYDEL//MYDEL	            DISK_GEOMETRY_EX* geom = (DISK_GEOMETRY_EX*) &buffer;
//MYDEL//MYDEL	            int fixed = (geom->Geometry.MediaType == FixedMedia);
//MYDEL//MYDEL	            __int64 size = geom->DiskSize.QuadPart;
//MYDEL//MYDEL				     
//MYDEL//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL//MYDEL	            fprintf (stderr, "\n**** DISK_GEOMETRY_EX for drive %d ****\n"
//MYDEL//MYDEL		                "Disk is%s fixed\n"
//MYDEL//MYDEL		                "DiskSize = %I64d\n",
//MYDEL//MYDEL		                drive,
//MYDEL//MYDEL		                fixed ? "" : " NOT",
//MYDEL//MYDEL		                size);
//MYDEL//MYDEL#endif
//MYDEL	        }
         }
//MYDEL		 else
//MYDEL		 {
//MYDEL			 DWORD err = GetLastError ();
//MYDEL#ifdef PRINTING_TO_CONSOLE_ALLOWED
//MYDEL			 fprintf (stderr, "\nDeviceIOControl IOCTL_STORAGE_QUERY_PROPERTY error = %d\n", err);
//MYDEL#endif
//MYDEL		 }

         CloseHandle (hPhysicalDriveIOCTL);
      }
   }

   return done;
}
