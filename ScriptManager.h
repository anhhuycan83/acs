// ScriptManager.h: interface for the CScriptManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCRIPTMANAGER_H__8801F2A8_0F4A_44A4_AEF2_F652B895238F__INCLUDED_)
#define AFX_SCRIPTMANAGER_H__8801F2A8_0F4A_44A4_AEF2_F652B895238F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ACSDoc.h"
#include "WindowAction.h"
#include "MouseWindowAction.h"
#include "KeyWindowAction.h"

//declare to use what function from dll
//BOOL ClickHandle(HWND hwnd, int x, int y)
typedef bool (*MYKMCPROCMOUSECLICK)(HWND hwnd, int x, int y);
typedef bool (*MYKMCSENDKEY)(HWND ins_hMouseHook);

//KMC dll
typedef void (*MYPostMessageExPROC)(HWND hWnd, int messageDown, int messageUp,
									  WPARAM wParamDown, LPARAM lParamDown,
									  WPARAM wParaUp, LPARAM lParamUp,
									  bool bIsMoveMouse, bool bIsReturnOldPoint, bool bIsMouseMoveBetween);

typedef void (*MYSetForegroundWindowInternalPROC)(HWND hWnd);
//send key in KMC.dll
typedef void (*MYSendKeyUpDownToHwndPROC)(HWND hwnd, TCHAR ch, BOOL bIsShiftDown);
//SendKeyUpDownToHwnd
//void SendVKToHwnd(HWND hwnd, short vkKey, BOOL bIsSysKey)
typedef void (*MYSendVKToHwndPROC)(HWND hwnd, short vkKey, BOOL bIsSysKey);

//globle function:

UINT ThreadRunningSript(PVOID pParam);
bool KeyPress(CKeyWindowAction *kwa, BOOL bIsSetActive);
void GenerateChar(CKeyPress* kp, int time);
bool SetForegroundWindowInternal(HWND hWnd);
bool ReSetWindownHandle(CWindowAction* wa);
//mouse
bool MouseClick(CMouseWindowAction* mwa, BOOL bIsSetActive, BOOL bIsMoveMouseBeforeClick);
bool MyPostMessageEx(HWND hWnd, int messageDown, int messageUp,
					 WPARAM wParamDown, LPARAM lParamDown,
					 WPARAM wParaUp, LPARAM lParamUp,
					 bool bIsMoveMouse, bool bIsReturnOldPoint, bool bIsMouseMoveBetween);



extern HINSTANCE hKMCDll;
extern MYKMCPROCMOUSECLICK m_pMouseClick;
extern MYKMCSENDKEY m_pSendKey;
extern HMODULE hModuleKMC;
extern MYPostMessageExPROC pfMyPostMessageEx;
extern MYSetForegroundWindowInternalPROC pfSetForegroundWindowInternal;
extern MYSendKeyUpDownToHwndPROC pfSendKeyUpDownToHwnd;
extern MYSendVKToHwndPROC pfSendVKToHwnd;

class CScriptManager  
{
public:
	CScriptManager();
	virtual ~CScriptManager();
public: //implement
	void RefreshThread();
	
	void GenerateKey(int ch, BOOL bExtended);
	bool SendNormalChar(HWND hwnd, byte asciiCode);
	void SendVKToHwnd(HWND hwnd, short vkKey, BOOL bIsSysKey);
	bool SendKeyUpDownToHwnd(HWND hwnd, TCHAR ch, BOOL bIsShiftDown);	
	void SetTxtWAIF(CWindowAction* pWA, bool bIsFound, int exitCode);
	
	
	bool RunScriptAll();
	
	int m_currentIndex;
	bool MyUseDLL();
	void MyUnUseDLL();
	bool RunScriptByIndex(int index);
	void SetPoiterDocument(CACSDoc* pDoc);
	CACSDoc* m_pDoc;
private:

};

	BOOL CALLBACK EnumWindowsParentWA(HWND hWnd, LPARAM lParam);
	BOOL CALLBACK EnumChildWAProc(HWND hwnd, LPARAM lParam);
	BOOL CALLBACK EnumWindowsTopWA(HWND hWnd, LPARAM lParam);

#endif // !defined(AFX_SCRIPTMANAGER_H__8801F2A8_0F4A_44A4_AEF2_F652B895238F__INCLUDED_)
