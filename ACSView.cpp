// ACSView.cpp : implementation of the CACSView class
//

#include "stdafx.h"
#include "ACS.h"

#include "ACSDoc.h"
#include "ACSView.h"
#include "textfile.h"
#include "MainTab.h"
#include "OptionTab.h"
#include "MainFrm.h"
#include "diskid32.h"
#include "RecordNoteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CACSView

IMPLEMENT_DYNCREATE(CACSView, CFormView)

BEGIN_MESSAGE_MAP(CACSView, CFormView)
	//{{AFX_MSG_MAP(CACSView)
	ON_BN_CLICKED(IDC_btnHide, OnbtnHide)
	ON_COMMAND(pmMitShow, OnpmMitShow)
	ON_COMMAND(pmMitExit, OnpmMitExit)
	ON_WM_DESTROY()
	ON_MESSAGE(MYWM_CHECKTIME, OnCheckTime)
	ON_BN_CLICKED(IDC_btnExit, OnbtnExit)
	ON_MESSAGE(MYWM_SCRIPT_FINISHED, OnScriptFinished)
	ON_MESSAGE(MYWM_SCRIPT_RUNNING_INDEX, OnScriptRunningIndex)
	ON_MESSAGE(MYWM_SCRIPT_REPEAT, OnScriptRepeat)
	ON_MESSAGE(MYWM_TIME_REMAIN, OnTimeRemain)
	ON_COMMAND(ID_TOOL_SCHEDULE, OnToolSchedule)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CACSView construction/destruction

CACSView::CACSView()
	: CFormView(CACSView::IDD)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_visible = false;// when first run is move to tray icon
	m_bIsResizeToFit = FALSE;//for resize main frame to fit cview once times when frame was showed the first time

	m_pThreadSriptRunning = 0;
	//m_bIsRecording = false;
	m_statusScript = PSTOP;
	hMMsDLL = NULL;
	hmeDll = NULL;
	hModuleKeyboard = NULL;
	hModuleMouse = NULL;
	hUpdateRecordDll = NULL;
	//checking time thread
	m_pThreadCheckingTime = 0;
	m_statusTimer = PSTOP;
	
	//AfxGetApp()->m_xmlSetting.
	//{{AFX_DATA_INIT(CACSView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	//OnInitialUpdate();
	//m_pMouseManager = new CMouseManager(this, GetDocument());
}

CACSView::~CACSView()
{
	//CFunctionHelper::CloseConsole();
}

void CACSView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CACSView)
	DDX_Control(pDX, chbMoveMouseBeforClick, m_chbMoveMouseBeforClickCtrl);
	DDX_Control(pDX, chbSetActive, m_chbSetActiveCtrl);
	DDX_Control(pDX, IDC_TABCTRL1, m_tbCtrl);
	//}}AFX_DATA_MAP
}

BOOL CACSView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CACSView::OnInitialUpdate()
{
//MYDEL	CFunctionHelper::InitConsoleWindow();

	CFormView::OnInitialUpdate();
	

	//now active tab control
	m_tbCtrl.InitDialogs(); 
	m_tbCtrl.InsertItem(0,_T("Main"));
	m_tbCtrl.InsertItem(1,_T("Option"));

	m_tbCtrl.ActivateTabDialogs();

	MyUseDLL();
	//MyGetArrRecordNote();
	//MyRefreshListReport(m_RecordNoteArray);

	//set key hook
	setHookKeyBoard();
	//set mouse hook
	setHookMouse();

	//set button record of main tab
	GetMainTab()->StopAction();

	m_statusTimer = PSTOP;	
	MyStartTimer();
	
//	//SET TOPMOST	
	CACSApp* pApp = (CACSApp*)AfxGetApp();	
//	if(pApp->m_xmlSetting.SetSettingLong(_T("INITIAL"), _T("TOPMOST"), 0) == 1)
//	{
//		pOptionTab->SetTopMost(m_hWnd, TRUE);
//	}

	//SHOW AT START	
	if(pApp->m_xmlSetting.GetSettingLong(_T("INITIAL"), _T("SHOWATSTART"), true))
	{
		m_visible = true;
		//myShowDlg(true);
	}
	else
	{
		m_visible = false;
		//myShowDlg(false);
	}
	//now set KeyManager to poiter a pDoc for constructor
	//m_keyManager.SetPoiterDocument(GetDocument());
	
	CDocument* pDoc = CFunctionHelper::GetActiveDocument();
	if (pDoc->IsKindOf( RUNTIME_CLASS(CACSDoc) ) )
	{
         //AfxMessageBox(_T("not bs doc"));
		m_keyManager.SetPoiterDocument((CACSDoc*)pDoc);
		m_mouseManager.SetPoiterDocument((CACSDoc*)pDoc);
		m_scriptManager.SetPoiterDocument((CACSDoc*)pDoc);
//		 CACSDoc* p = (CACSDoc*)pDoc;
//		 CRect r;
//		 GetClientRect(&r);
//		 p->AddWindowAction(AfxGetMainWnd()->m_hWnd, _T(""),_T(""),0,r,0, KeyWindowAction);
		 
		 //p->m_listWindowAction.RemoveAll();
	}
	
//   printf ("To get all details use \"diskid32 /d\"\n");

//MYDEL   if (argc > 1 && strstr (argv [1], "/d"))
//MYDEL      PRINT_DEBUG = true;
//MYDEL   else
//MYDEL      PRINT_DEBUG = false;
//MYDEL      diskid32 d;
//MYDEL      CString strHddSerial;
//MYDEL      BOOL bRs = d.GetHddSerial(strHddSerial);
//MYDEL      if(bRs)
//MYDEL   		AfxMessageBox(strHddSerial);

   //d.GetMACaddress ();
   
//MYDEL   GetMACaddress ();
	m_chbMoveMouseBeforClickCtrl.SetCheck(true);
	m_chbSetActiveCtrl.SetCheck(true);
	ReSetThreadCheckingTime();

}

/////////////////////////////////////////////////////////////////////////////
// CACSView diagnostics

#ifdef _DEBUG
void CACSView::AssertValid() const
{
	CFormView::AssertValid();
}

void CACSView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CACSDoc* CACSView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CACSDoc)));
	return (CACSDoc*)m_pDocument;
}

//BsDocTemplate* CACSView::GetDocument() // non-debug version is inline
//{
//	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CACSDoc)));
//	return (BsDocTemplate*)m_pDocument;
//}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CACSView message handlers

BOOL CACSView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL bRs = FALSE;
	if(pMsg->message==WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			bRs = TRUE;
			break;
		case VK_F1:
			OnHelpIndex();
			bRs = TRUE;
			break;
		}
		if(bRs)
			return bRs;
	}
	return CFormView::PreTranslateMessage(pMsg);
}

bool CACSView::setHookKeyBoard()
{
	hmeDll = LoadLibrary((LPCTSTR) _T("HME.dll"));
	//hMMsDLL = LoadLibrary((LPCTSTR) "MMsg.dll");
	if(hmeDll == NULL)
	{
		MessageBox(_T("Can't load HME.dll"));
		return false;
	}
	
	hModuleKeyboard = GetModuleHandle(_T("HME"));
	
	if(hModuleKeyboard == NULL)
	{
		return false;
	}
	//GetModuleHandle("TestDLL")
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/

	oldKeyBoardHook = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)GetProcAddress(hModuleKeyboard,"myKeyBoardHook"), hModuleKeyboard, 0);
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/
	//set hhook to dll
	
	pSetHHook = (MYKBPROC) GetProcAddress(hModuleKeyboard, "setHKeyBoardHook");
	//test pSetHHook if nessesary
	(pSetHHook)(oldKeyBoardHook);

	//set hWnd to get message
	pSetHWNDRectMsg = (MYKBPROC3) GetProcAddress(hModuleKeyboard, "setHWNDRecMsg");
	//CFrameWnd * pFrame = (CFrameWnd *)(AfxGetApp()->m_pMainWnd);
	//(pSetHWNDRectMsg)(AfxGetMainWnd()->m_hWnd);	
	//set hanle view to hook dll so that it can send message direct to cview winproc function
	(pSetHWNDRectMsg)(m_hWnd);
	return true;
}

void CACSView::setUnHookKeyBoard()
{
	//free dll	
	if(hmeDll)
	{
		FreeLibrary(hmeDll);
		hmeDll = 0;	
	}	
	//UnhookWindowsHookEx return != if success else return  0
	if ((oldKeyBoardHook != 0) && (UnhookWindowsHookEx(oldKeyBoardHook) != 0))
	{
		//unhook keyboard
		oldKeyBoardHook = NULL;
		/*
		if ((oldMouseHook != 0) && (UnhookWindowsHookEx(oldMouseHook) != 0))
		{		
			//unhook mouse
			oldMouseHook = NULL;
			return true;
		}
		*/
	}	
}

bool CACSView::setHookMouse()
{
	//hmeDll = LoadLibrary((LPCTSTR) _T("HME.dll"));
	
	hMMsDLL = LoadLibrary((LPCTSTR) _T("MMsg.dll"));
	if(hMMsDLL == NULL)
	{
		MessageBox(_T("Can't load MMsg.dll"));
		return false;
	}
	
	hModuleMouse = GetModuleHandle(_T("MMsg"));
	
	if(hModuleMouse == NULL)
	{
		return false;
	}
	//GetModuleHandle("TestDLL")
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/

	oldMouseHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)GetProcAddress(hModuleMouse,"myLLMouseHook"), hModuleMouse, 0);	
	/*
	msg.Format("exe file: last error code: %d", GetLastError());
	MessageBox(msg);
	*/
	//set hhook to dll
	//MYPROC pSetHHook;
	//MYMOUSEPROC2 pSetMouseHHook;
	
	//pSetHHook = (MYPROC) GetProcAddress(hModuleKeyboard, "setHKeyBoardHook");
	pSetHMHook = (MYMOUSEPROC) GetProcAddress(hModuleMouse, "setHMouseHook");
	//test pSetHHook if nessesary
	(pSetHMHook)(oldMouseHook);

	//set hWnd to get message
	pSetHWNDRectMsgMouse = (MYMOUSEPROC2) GetProcAddress(hModuleMouse, "setHWNDRecMsg");
	//CFrameWnd * pFrame = (CFrameWnd *)(AfxGetApp()->m_pMainWnd);
	//(pSetHWNDRectMsg)(AfxGetMainWnd()->m_hWnd);	
	(pSetHWNDRectMsgMouse)(m_hWnd);
	return true;
}

void CACSView::setUnHookMouse()
{
	//free dll	
	if(hMMsDLL)
	{
		FreeLibrary(hMMsDLL);
		hmeDll = 0;	
	}	
	//UnhookWindowsHookEx return != if success else return  0
	if ((oldMouseHook != 0) && (UnhookWindowsHookEx(oldMouseHook) != 0))
	{
		//unhook keyboard
		oldMouseHook = NULL;
		/*
		if ((oldMouseHook != 0) && (UnhookWindowsHookEx(oldMouseHook) != 0))
		{		
			//unhook mouse
			oldMouseHook = NULL;
			return true;
		}
		*/
	}	
}


//for systray
void CACSView::myShowDlg(bool ins_isShow)
{
	if(ins_isShow == true)
	{	
		ResizeFrameToFit();
		m_visible = true;
		TrayMessage(NIM_DELETE);
		AfxGetMainWnd()->ShowWindow(SW_RESTORE);//SW_NORMAL  SW_HIDE
		
		//move from tray area to desktop
	}
	else
	{
		m_visible = false;
		TrayMessage(NIM_ADD);
		//myResetSystrayIconStatus(isRunning);
		AfxGetMainWnd()->ShowWindow(SW_HIDE);//SW_NORMAL
		//move from desktop to tray area
	}
}

int CACSView::TrayMessage(DWORD dwMessage)
{
	CString sTip(_T("Note Message"));
	//sTip += MYVERSION;

    NOTIFYICONDATA tnd;

    tnd.cbSize = sizeof(NOTIFYICONDATA);
    tnd.hWnd = m_hWnd;
    tnd.uID = IDR_MAINFRAME;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    tnd.uCallbackMessage = MYWM_NOTIFYICON;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
    VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDR_MAINFRAME)) );
    lstrcpyn(tnd.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));

    return Shell_NotifyIcon(dwMessage, &tnd);
}

int CACSView::TrayMessage(DWORD dwMessage, CString sTip)//set the tray icon with strMsg input
{	
	sTip.Format(_T("%s\n%s"), sTip, MYVERSION); 
	//sTip += _T("\n");
	 //sTip += MYVERSION;

    NOTIFYICONDATA tnd;

    tnd.cbSize = sizeof(NOTIFYICONDATA);
    tnd.hWnd = m_hWnd;
    tnd.uID = IDR_MAINFRAME;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON;
    tnd.uCallbackMessage = MYWM_NOTIFYICON;
    tnd.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
    VERIFY( tnd.hIcon = LoadIcon(AfxGetInstanceHandle(), 
		MAKEINTRESOURCE (IDR_MAINFRAME)) );
    //lstrcpyn(tnd.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));
	lstrcpyn(tnd.szTip, (LPCTSTR)sTip, sizeof(tnd.szTip));

    return Shell_NotifyIcon(dwMessage, &tnd);
}

void CACSView::OnbtnHide() 
{
	// TODO: Add your control notification handler code here
	myShowDlg(false);	
}


void CACSView::myShowContextNotMenu()
{
	CMenu menu ;
   // Load and Verify Menu

   VERIFY(menu.LoadMenu(IDR_ContextNoMenu));
   CMenu* pPopup = menu.GetSubMenu (0);
   //my code begin to check if the mit is enable or disable
   /*
   if(m_btRunObject.IsWindowEnabled() == false)
   {	   
	   menu.EnableMenuItem(mitStart, MF_GRAYED);
   }
   if(m_btStopObject.IsWindowEnabled() == false)
   {
	   menu.EnableMenuItem(mitStop, MF_GRAYED);
   }
   */



   ASSERT(pPopup != NULL);

   // Get the cursor position

   POINT pt ;
   GetCursorPos (&pt) ;

   // Fix Microsofts' BUG!!!!

   SetForegroundWindow();

   ///////////////////////////////////

   // Display The Menu

   pPopup->TrackPopupMenu(TPM_LEFTALIGN |
   TPM_RIGHTBUTTON,pt.x, pt.y, AfxGetMainWnd());
   SetForegroundWindow();
}

void CACSView::MyAddItemDLL(CString strLineAdd)
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	ADDPROC pAddRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pAddRecordNote = (ADDPROC) GetProcAddress(hModuleDataRecordNote, "AddRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;	
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	//MyClearMem();
	//(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
	(pAddRecordNote)(m_RecordNoteArray, filePath, strLineAdd);
}

void CACSView::MyDeleteItemDLL(int index)
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	DELPROC pDelRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pDelRecordNote = (DELPROC) GetProcAddress(hModuleDataRecordNote, "DelRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;	
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	//MyClearMem();
	//(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
	(pDelRecordNote)(m_RecordNoteArray, filePath, index);

}

void CACSView::StringSplit(const CString &str, CStringArray &arr, TCHAR chDelimitior)
{
	int nStart = 0, nEnd = 0;
	arr.RemoveAll();

	while (nEnd < str.GetLength())
	{
		// determine the paragraph ("xxx,xxx,xxx;")
		nEnd = str.Find(chDelimitior, nStart);
		if( nEnd == -1 )
		{
			// reached the end of string
			nEnd = str.GetLength();
		}

		CString s = str.Mid(nStart, nEnd - nStart);
		if (!s.IsEmpty())
			arr.Add(s);

		nStart = nEnd + 1;
	}
}

void CACSView::MyClearMem()
{
	for(int i = 0; i < m_RecordNoteArray.GetSize(); i++)
	{
		delete (CRecordNote *)(m_RecordNoteArray.GetAt(i));
	}
	m_RecordNoteArray.RemoveAll();
}

void CACSView::MyRefreshListReport(CTypedPtrArray<CObArray, CRecordNote*> &arrayRecordNote)
{
	CStringArray arrStr;

	CMainTab *pDlgMain = (CMainTab *)(m_tbCtrl.m_Dialog[0]);
	//pDlgMain->m_wndList.ins
	//pDlgMain->m_wndList.SetColumnHeader(_T("STT, 50; Date time, 200; Frequency, 50; Message, 250, 2"));
	pDlgMain->m_wndList.SetColumnHeader(_T("STT, 30; Class caption, 150; Action, 70; Handle, 70; Class Style, 100"));

	CRecordNote *pRectNote;
	//TCHAR temp[5];
	CString s;
	pDlgMain->m_wndList.SetSortable(false);
	for (int i = 0; i < arrayRecordNote.GetSize(); i++)
	{
		pRectNote = arrayRecordNote.GetAt(i);
		// arrStr
		StringSplit(pRectNote->m_line, arrStr, _T(';'));
		const int IDX = pDlgMain->m_wndList.InsertItem(i, _T(""));
		s.Format(_T("%d"), i);
		//pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(i, temp, 10));
		pDlgMain->m_wndList.SetItemText(IDX, 0, s);
		pDlgMain->m_wndList.SetItemText(IDX, 1, arrStr.GetAt(0));
		pDlgMain->m_wndList.SetItemText(IDX, 2, arrStr.GetAt(1));
		pDlgMain->m_wndList.SetItemText(IDX, 3, arrStr.GetAt(2));
	}
	//pDlgMain->m_wndList.SortItems(0, TRUE);	
}

void CACSView::MyUseDLL()
{
	hUpdateRecordDll = LoadLibrary(_T("DataRecordNote.dll"));//(LPCTSTR) 
	if(hUpdateRecordDll == NULL)
	{
		MessageBox(_T("Can't load DataRecordNote.dll"));
		return;
	}
}

void CACSView::MyUnUseDLL()
{
	if(hUpdateRecordDll)
	{
		FreeLibrary(hUpdateRecordDll);
		hUpdateRecordDll = 0;
		//FreeLibrary(hMMsgDLL);
	}
}


void CACSView::MyUseDllKMC()
{
	hUpdateRecordDll = LoadLibrary(_T("DataRecordNote.dll"));//(LPCTSTR) 
	if(hUpdateRecordDll == NULL)
	{
		MessageBox(_T("Can't load DataRecordNote.dll"));
		return;
	}
}

void CACSView::MyUnUseDllKMC()
{
	if(hUpdateRecordDll)
	{
		FreeLibrary(hUpdateRecordDll);
		hUpdateRecordDll = 0;
		//FreeLibrary(hMMsgDLL);
	}
}

void CACSView::MyGetArrRecordNote()
{
	HMODULE hModuleDataRecordNote; //hmodule
	hModuleDataRecordNote = GetModuleHandle(_T("DataRecordNote"));
	if(hModuleDataRecordNote == NULL)
	{
		return;
	}

	MYPROCRECNOTE pGetArrRecordNote;
	//MYMOUSEPROC pSetMouseHHook;

	pGetArrRecordNote = (MYPROCRECNOTE) GetProcAddress(hModuleDataRecordNote, "GetListRecordNote");
	//test pSetHHook if nessesary
	CString filePath = AfxGetApp()->GetProfileString(_T("INITIAL"), _T("FILESOTER"), _T("C:\\myRecordNote.txt"));

	CFileFind finder;	
	
	if(!finder.FindFile(filePath))
	{
		CTextFileWrite myfile(filePath, 
		CTextFileWrite::UTF_8);
		myfile.Close();
	}
	//MessageBox(filePath);
	MyClearMem();
	(pGetArrRecordNote)(m_RecordNoteArray, filePath);	
}

void CACSView::MyReRunTimer()
{
//MYDEL	m_myStatus = MYSTOP;
//MYDEL//	m_isHaveCurrentTimeCheking = false;
//MYDEL//	//clear backgroud of current selected record
//MYDEL//	if(m_currentIndexWaitingToShow != -1)	
//MYDEL//		SetItemBkColor(m_currentIndexWaitingToShow,RGB(255,255,255)); //white
//MYDEL//	
//MYDEL//	m_currentIndexWaitingToShow = -1;
//MYDEL	MyStartTimer();
}

void CACSView::MyStartTimer()
{
//MYDEL	switch(m_myStatus)
//MYDEL	{
//MYDEL		case MYSTOP:
//MYDEL//			if(m_RecordNoteArray.GetSize() > 0)
//MYDEL//			{
//MYDEL//				MySetCurrentCheckingTime();
//MYDEL//				if(m_isHaveCurrentTimeCheking)
//MYDEL//				{
//MYDEL//					m_pTimerThread = AfxBeginThread(MyTimer, this, THREAD_PRIORITY_NORMAL, 0,
//MYDEL//						CREATE_SUSPENDED);
//MYDEL//					m_pTimerThread->m_bAutoDelete = false;				
//MYDEL//					m_myStatus = MYRUN;
//MYDEL//									
//MYDEL//					m_pTimerThread->ResumeThread();	
//MYDEL//				}
//MYDEL//			}
//MYDEL
//MYDEL			break;
//MYDEL		case MYPAUSE:	
//MYDEL			m_myStatus = MYRUN;
//MYDEL			break;
//MYDEL		case MYRUN:
//MYDEL			m_myStatus = MYPAUSE;			
//MYDEL			break;
//MYDEL		default:
//MYDEL			break;
//MYDEL	}
}

UINT CACSView::MyTimer(LPVOID pParam)
{
/*
	enum TimerStatus {
       MYSTOP = 0,
       MYRUN = 1,
       MYPAUSE = 2,
    };

*/
	//CHighTimeSpan tsTemp;
	//CSubtitleDlg *pSubDlg = (CSubtitleDlg *)(pParam);

	//tsTemp = CHighTimeSpan(0,0,0,0,100,0,0);
	//CSubtitleDlg *pSubDlg = (CSubtitleDlg *) CWnd::FromHandle (*(HWND *)pParam);

//MYDEL	CACSView *pSubDlg = (CACSView*)pParam;
//MYDEL	//pSubDlg->m_tsTimer = CHighTimeSpan(0,0,0,0,0,0,0);	
//MYDEL	while(1)
//MYDEL	{
//MYDEL		switch(pSubDlg->m_myStatus)
//MYDEL		{
//MYDEL			case MYSTOP:
//MYDEL				return 0;
//MYDEL				break;
//MYDEL			case MYPAUSE:				
//MYDEL				break;
//MYDEL			case MYRUN:
//MYDEL				//pSubDlg->m_tsTimer += tsTemp;
//MYDEL				//_ultow(pSubDlg->m_tsTimer.GetTotalMilliSeconds(), m_txtSecondValue1, 10);
//MYDEL				//pSubDlg->MySetTxtSecond1();
//MYDEL				pSubDlg->PostMessage(MYWM_CHECKTIME, 0, 0);				
//MYDEL				
//MYDEL				//pSubDlg->UpdateData(false);
//MYDEL				break;
//MYDEL			default:
//MYDEL				break;
//MYDEL		}
//MYDEL		::Sleep(1000);
//MYDEL	}

	return 0;
}
LRESULT CACSView::OnCheckTime(WPARAM, LPARAM)
{
//	COleDateTime timeNow = COleDateTime::GetCurrentTime();
//
//	//minute
//	if(m_timeChecking.GetMinute() == timeNow.GetMinute())
//	{
//		//hour
//		if(m_timeChecking.GetHour() == timeNow.GetHour())
//		{
//			//second
//			if(m_timeChecking.GetSecond() == timeNow.GetSecond())
//			{
//				//play a sound file
//				//MessageBox(_T("helo"));
//				CMainTab *pDlgMain = (CMainTab *)(m_tbCtrl.m_Dialog[0]);
//				m_myStatus = MYPAUSE;
//				//AfxMessageBox(pDlgMain->m_wndList.GetItemText(m_currentIndexWaitingToShow, 2));
//				//on top message
//				::MessageBox(NULL,
//					pDlgMain->m_wndList.GetItemText(m_currentIndexWaitingToShow, 3),
//					_T("Alert"),
//					MB_ICONINFORMATION | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);
//				MyReRunTimer();
//			}
//			
//			//day
//			/*
//			if(m_timeChecking.GetDay() == timeNow.GetDay())
//			{
//				//month
//				if(m_timeChecking.GetMonth() == timeNow.GetMonth())
//				{
//					//year
//					if(m_timeChecking.GetYear() == timeNow.GetYear())
//					{
//						
//					}
//				}
//			}
//			*/
//		}
//	}	
	
	return 1;
}

void CACSView::SetItemBkColor(int indexRow, COLORREF colorToSet)
{
//CMainTab *pDlgMain = (CMainTab *)(m_tbCtrl.m_Dialog[0]);
//		//COLORREF colorCurrentActiveRecord;
//		//colorCurrentActiveRecord = RGB(255,0,0); // red
//		//color = RGB(0,255,0); // green
//		//colorCurrentActiveRecord = RGB(0,0,255); // blue
//		//EF,EB,DE
//		pDlgMain->m_wndList.SetItemBkColor(m_currentIndexWaitingToShow, -1, colorToSet, true);
}



LRESULT CACSView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	// Open window when double click to the Systray Icon
	
	//The main part. HC_ACTION is called whenever 
	//there is a mouse or keyboard event.
	//lparam is a pointer to EVENTMSG struct, 
	//which contains the necessary information
	//to replay the message.
	//CMainFrame* pMainFrame;
	CACSApp * pApp;
	
//	int nVirtKey;
	switch(message)
	{
//	case WM_CHAR:		
//		{
//			switch (wParam) 
//			{ 
//			case 0x08:  // backspace 
//			case 0x0A:  // linefeed 
//			case 0x1B:  // escape 
//				MessageBeep((UINT) -1); 
//				//return 0; 
//				break;
//				
//			case 0x09:  // tab 
//				
//				// Convert tabs to four consecutive spaces.
//				
//				//				for (i = 0; i < 4; i++) 
//				//					SendMessage(hwndMain, WM_CHAR, 0x20, 0); 
//				//				return 0; 
//				break;				
//			case 0x0D:  // carriage return 
//				
//				// Record the carriage return and position the 
//				// caret at the beginning of the new line.
//				
//				//				pchInputBuf[cch++] = 0x0D; 
//				//				nCaretPosX = 0; 
//				//				nCaretPosY += 1; 
//				break; 
//				
//			default:    // displayable character 
//				pMainFrame = (CMainFrame*) AfxGetMainWnd();
//				s = (TCHAR) wParam;
//				pMainFrame->m_wndStatusBar.SetPaneText(1,s);
//				//				HideCaret(hwndMain); 
//				break;
//			}
//		}
//		break;
//		//AfxMessageBox(_T("t3"));
		
	//hanle F1 for help index => open chm help file

	case HC_ACTION://Keyboar message
		TRACE(_T("Begin mouse\n"));
		if(!OnMouseProc(message, wParam, lParam))//if not mouse action
		{
			TRACE(_T("Not mouse message\n"));
			if(wParam > 0)
			{
				TRACE(_T("Begin key\n"));
				if(!OnKeyboardProc(message, wParam, lParam))//if not keyboard action
				{
					TRACE(_T("Not key message\n"));
				}
			}
		}		
		break;
		
	case MYWM_NOTIFYICON:
		{
			switch (lParam)
			{				
			case WM_LBUTTONDBLCLK: 
				switch (wParam) 
				{
				case IDR_MAINFRAME:
					//m_hided = false;
					//ShowWindow(SW_NORMAL);
					TrayMessage(NIM_DELETE);
					myShowDlg(true);
					SetForegroundWindow();
					SetFocus();
					return TRUE;
					break; 
				} 
				break; //showContextNotMenu
				
				case WM_RBUTTONUP:
					{
						switch (wParam) 
						{
						case IDR_MAINFRAME:
							myShowContextNotMenu();
							return TRUE; 
							break; 
						} 
					}
					break; //showContextNotMenu
					
			}
		}
		break;//end case notify
	case MYWM_KEYPRESS_F10:
		//AfxMessageBox(_T("good"));
		
		pApp = (CACSApp*)AfxGetApp();		
		if(pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F10")) == _T("F10"))
		{
			//show
			//MessageBox(_T("F10"));
			if(!m_visible)
			{
				myShowDlg(true);
			}
		}
		break;
	case MYWM_KEYPRESS_F11:
		pApp = (CACSApp*)AfxGetApp();
		if(pApp->m_xmlSetting.GetSettingString(_T("INITIAL"), _T("HOTKEY"), _T("F11")) == _T("F11"))
		{
			//show
			//MessageBox(_T("F11"));
			if(!m_visible)
			{
				myShowDlg(true);
			}
		}
		break;
	default:
		break;
	}
	return CFormView::WindowProc(message, wParam, lParam);
}

bool CACSView::OnMouseProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	bool bRs = false;
//MYDEL   	CMainFrame* pMainFrame;
	CString s;
	//int nVirtKey;

	switch(wParam)
	{
		//MOUSE MESSAGE
	case WM_LBUTTONDOWN:
		//case WM_LBUTTONUP: 
		//case WM_LBUTTONDBLCLK: 
		//button = MouseButtons.Left;
		/*
		c.Format("mouse LB click: %d", wParam);
		AfxMessageBox(c);
		return 1;
		*/
		
	case WM_RBUTTONDOWN:
		bRs = true;
		//case WM_RBUTTONUP: 
		//case WM_RBUTTONDBLCLK: 
		//                button = MouseButtons.Right;
		
//		if(!m_bIsRecording)
//			break;

		HWND hGA_ROOTOWNER, hWnd;
		POINT		screenpoint2;
		GetCursorPos (&screenpoint2);
		hWnd=::WindowFromPoint(screenpoint2);
		hGA_ROOTOWNER = ::GetAncestor(hWnd, GA_ROOTOWNER);

//MYDEL   		HWND hParent, hGA_PARENT, hGA_ROOT, hGA_ROOTOWNER, hWnd, hParentRoot, hDesktop;
//MYDEL   		POINT		screenpoint2;
//MYDEL   		GetCursorPos (&screenpoint2);
//MYDEL   				hWnd=::WindowFromPoint(screenpoint2);
//MYDEL   		hParent = ::GetParent(hWnd);
//MYDEL   		hGA_PARENT = ::GetAncestor(hWnd, GA_PARENT);
//MYDEL   		hGA_ROOT = ::GetAncestor(hWnd, GA_ROOT);
//MYDEL   		hGA_ROOTOWNER = ::GetAncestor(hWnd, GA_ROOTOWNER);
//MYDEL   		hParentRoot = ::GetAncestor(hGA_ROOTOWNER, GA_PARENT);
//MYDEL   		hDesktop = ::GetDesktopWindow();
//MYDEL   		
//MYDEL   		s.Format(_T("%5x, parent:%5x, gaParent:%5x, root:%5x, Rowner%5x, parentRo: %x, desktop: %x"), hWnd, hParent, hGA_PARENT, hGA_ROOT, hGA_ROOTOWNER, hParentRoot, hDesktop);
//MYDEL   		pMainFrame = (CMainFrame*) AfxGetMainWnd();
//MYDEL   			//s = _T("Mouse Action");
//MYDEL   			pMainFrame->m_wndStatusBar.SetPaneText(1,s);

		if(m_statusScript == PRECORD)
		{						
			if(hGA_ROOTOWNER != AfxGetMainWnd()->m_hWnd)
			{
				//			pMainFrame = (CMainFrame*) AfxGetMainWnd();
				//			s = _T("Mouse Action");
				//			pMainFrame->m_wndStatusBar.SetPaneText(1,s);
				//			POINT		screenpoint;
				//			GetCursorPos (&screenpoint);
				//			m_mouseManager.OnMouse(::WindowFromPoint(screenpoint), wParam, lParam);
				m_mouseManager.OnMouse(hWnd, wParam, lParam);
			}
		}
		break;
	case WM_MOUSEWHEEL:
		//If the message is WM_MOUSEWHEEL, the high-order word of mouseData member is the wheel delta. 
		//One wheel click is defined as WHEEL_DELTA, which is 120. 
		//(value >> 16) & 0xffff; retrieves the high-order word from the given 32-bit value
		//mouseDelta = (short)((pMouseHookStruct->mouseData >> 16) & 0xffff);
		//mouseDelta = HIWORD(pMouseHookStruct->mouseData);
		//mouseDelta /= 2;
		//TODO: X BUTTONS (I havent them so was unable to test)
		//If the message is WM_XBUTTONDOWN, WM_XBUTTONUP, WM_XBUTTONDBLCLK, WM_NCXBUTTONDOWN, WM_NCXBUTTONUP, 
		//or WM_NCXBUTTONDBLCLK, the high-order word specifies which X button was pressed or released, 
		//and the low-order word is reserved. This value can be one or more of the following values. 
		//Otherwise, mouseData is not used. 				
		//c.Format("mouse wheel: %d", mouseDelta);
		//AfxMessageBox(c);
		
		break;
	default:
		break;
		
		
	}
	return bRs;
}

bool CACSView::OnKeyboardProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	bool bRs = true;
	CMainFrame* pMainFrame;
	CString s;
	int nVirtKey;

	TRACE(_T("OnKeyProc, wparam: %d\n"), wParam);
	switch(wParam)
	{			
	case  VK_F10: // F10 - start record
		//CString s;
		pMainFrame = (CMainFrame*) AfxGetMainWnd();
		nVirtKey = GetKeyState(wParam);
		if (nVirtKey & 0x8000) //key press down
		{
			s = _T("Status: Recording:...");
			pMainFrame->m_wndStatusBar.SetPaneText(1,s); //1 is the index of pane wich you want to update text
			GetMainTab()->RecordAction();
			return TRUE;//handle this message and do not call back to any handle
		}
		else
		{
		}
		break;
	case  VK_F11: // F11 - stop record
		//CString s;
		pMainFrame = (CMainFrame*) AfxGetMainWnd();
		nVirtKey = GetKeyState(wParam);
		if (nVirtKey & 0x8000) //key press down
		{
			s = _T("Status: Stoped");
			pMainFrame->m_wndStatusBar.SetPaneText(1,s);
			GetMainTab()->StopAction();
			return TRUE;//handle this message and do not call back to any handle
		}
		break;
		
		//KEYBOAR MESSAGE
	default:
		//		if(!m_bIsRecording)
		//			break;
		switch(m_statusScript)
		{
		case PRECORD:
			nVirtKey = GetKeyState(wParam);
			if ((nVirtKey & 0x8000)) //key press down
			{
				TRACE(_T("key pressed down\n"));
				HWND hGA_ROOTOWNER, hWnd;
				hWnd = ::GetForegroundWindow();
				hGA_ROOTOWNER = ::GetAncestor(hWnd, GA_ROOTOWNER);
				if(hGA_ROOTOWNER != AfxGetMainWnd()->m_hWnd)
				{
					//			pMainFrame = (CMainFrame*) AfxGetMainWnd();
					//			s = _T("Mouse Action");
					//			pMainFrame->m_wndStatusBar.SetPaneText(1,s);
					//			POINT		screenpoint;
					//			GetCursorPos (&screenpoint);
					//			m_mouseManager.OnMouse(::WindowFromPoint(screenpoint), wParam, lParam);
					
					//check cap lock and shift key
					//MYDEL							if(!GetAsyncKeyState(VK_SHIFT))//VK_LSHIFT|VK_RSHIFT|VK_CAPITAL
					//MYDEL							{
					//MYDEL								wParam = wParam + 32;
					//MYDEL							}
					//MYDEL							pMainFrame = (CMainFrame*) AfxGetMainWnd();
					//MYDEL							s = (TCHAR) wParam;
					//MYDEL							pMainFrame->m_wndStatusBar.SetPaneText(1,s);
					m_keyManager.OnKey(hWnd, message, wParam, lParam);
					//				HideCaret(hwndMain); 
				}
			}
			break;
		case PSTOP:
			break;

		}//end switch status
		break;
	}
	return bRs;
}


void CACSView::OnpmMitShow() 
{
	// TODO: Add your command handler code here
	myShowDlg(true);
}

void CACSView::OnpmMitExit() 
{
	// TODO: Add your command handler code here
	AfxGetMainWnd()->SendMessage(WM_CLOSE);
}


void CACSView::OnDestroy() 
{
	CFormView::OnDestroy();
	
	// TODO: Add your message handler code here
	MyClearMem();
	// TODO: Add your message handler code here
	MyUnUseDLL();
	if(m_visible == false) // this program is in tray area
	{
		TrayMessage(NIM_DELETE);
	}
	setUnHookKeyBoard();
	setUnHookMouse();
	
	//delete font variable
	//When you no longer need the font, call the DeleteObject function to delete the font.
	//especially if you have a sub dialog
	m_font.DeleteObject();
	
	//remove thread
	//delete m_pTimerThread;
	DWORD exitCode;
	::GetExitCodeThread(m_pThreadSriptRunning, &exitCode);
	if(exitCode == STILL_ACTIVE)
	{
		//m_pTimerThread->end
		m_statusScript = PSTOP;
		WaitForSingleObject(m_pThreadSriptRunning, INFINITE);
	}
	
	if(m_pThreadSriptRunning)
	{
		delete m_pThreadSriptRunning;
		m_pThreadSriptRunning = 0;
	}

	//remove thread TIME CHECKING
	::GetExitCodeThread(m_pThreadCheckingTime, &exitCode);
	if(exitCode == STILL_ACTIVE)
	{
		//m_pTimerThread->end
		m_statusTimer = PSTOP;
		WaitForSingleObject(m_pThreadCheckingTime, INFINITE);
	}
	
	if(m_pThreadCheckingTime)
	{
		delete m_pThreadCheckingTime;
		m_pThreadCheckingTime = 0;
	}
}

void CACSView::OnbtnExit() 
{
	// TODO: Add your control notification handler code here
	AfxGetMainWnd()->SendMessage(WM_CLOSE);
}

void CACSView::setRecordHook()
{
	GetMainTab()->RecordAction();
	CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
	pMainFrame->m_wndStatusBar.SetPaneText(1,_T(""));
}

void CACSView::setStopRecordHook()
{
	GetMainTab()->StopAction();
}

CMainTab* CACSView::GetMainTab()
{
	CMainTab* pTab = (CMainTab*) m_tbCtrl.m_Dialog[0];
	return pTab;
}

COptionTab* CACSView::GetOptionTab()
{
	COptionTab* pTab = (COptionTab*) m_tbCtrl.m_Dialog[1];
	return pTab;
}

void CACSView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//RefreshListReport();
}

//void CACSView::RefreshListReport()
//{
//	CMainTab* pDlgMain = GetMainTab();
//	pDlgMain->m_wndList.DeleteAllItems();
//	CACSDoc* pDoc = (CACSDoc*)CFunctionHelper::GetActiveDocument();
//	TCHAR temp[5];
//	for(int i = 0; i <	pDoc->m_listWindowAction.GetSize(); i++)
//	{
//		CWindowAction* pWindowAction = pDoc->m_listWindowAction.GetAt(i);
//		switch(pWindowAction->m_eKindOfWindowAction)
//		{
//		case KeyWindowAction:
//			CKeyWindowAction* pKey = (CKeyWindowAction*) pWindowAction;
//			const int IDX = pDlgMain->m_wndList.InsertItem(pDlgMain->m_wndList.GetItemCount(), _T(""));
//			pDlgMain->m_wndList.SetItemText(IDX, 0, _itow(pDlgMain->m_wndList.GetItemCount() - 1, temp, 10));
//			pDlgMain->m_wndList.SetItemText(IDX, 1, pKey->m_caption);
//			pDlgMain->m_wndList.SetItemText(IDX, 2, pKey->m_strToEnter);
//			break;
//		}
//	}
//
//	
//	
//
//}



bool CACSView::RunScriptByIndex(int index)
{
	bool bRs = false;
	//bRs = m_scriptManager.RunScriptByIndex(index);
//MYDEL	bool bRs = false;
//MYDEL	CWindowAction* wa = m_pDoc->m_listWindowAction.GetAt(index);
	

	return bRs;

}

bool CACSView::RunScriptAll()
{
	bool bRs = false;
	bRs = m_scriptManager.RunScriptAll();
	return bRs;
}

LRESULT CACSView::OnScriptFinished(WPARAM wParam, LPARAM lParam)
{
	m_statusScript = PSTOP;
	int exitCode = wParam;
	CWindowAction* wa = (CWindowAction*)lParam;
	CMainTab* pMainTab = GetMainTab();
	switch(wParam)
	{
	case 0://exit normal
		//m_scriptManager.SetTxtWAIF(wa, true, exitCode);
		
		pMainTab->StopAction();
		pMainTab->m_txtWAIFValue.Format(_T("Running script completely!"));
		break;
	case 1://handle not fuount
		//m_scriptManager.SetTxtWAIF(wa, false, exitCode);
		pMainTab->m_txtWAIFValue.Format(_T("Window was not found: classname: %s, style: %5x"), 
			wa->m_className, wa->m_calssStyle);
		pMainTab->StopAction();
		break;
	case 2://list of windowaction is null
		pMainTab->m_txtWAIFValue.Format(_T("You must record actions before running script!"));
		pMainTab->StopAction();
		break;
	case 3://user stop thread
		pMainTab->m_txtWAIFValue.Format(_T("User stoped!"));
		break;
	case 4://in trial mode and get max num action
		//m_scriptManager.SetTxtWAIF(wa, false, exitCode);
		pMainTab->m_txtWAIFValue.Format(_T("In trial mode you can only run max: %d actions!"), m_maxNumRunAction);
		pMainTab->StopAction();
		break;
	}
	pMainTab->UpdateData(false);
	m_scriptManager.MyUnUseDLL();
	return 1;
}

LRESULT CACSView::OnScriptRunningIndex(WPARAM wParam, LPARAM lParam)
{
	//wparam is index of current index running
	int index = (int)wParam;
	CReportCtrl* rc = &(GetMainTab()->m_wndList);
//MYDEL   	int numLineIndex = rc->GetItemCount() - 1 - (index + 3);
//MYDEL   	if(numLineIndex >= 0)
//MYDEL   		numLineIndex = numLineIndex + 3;
//MYDEL   	else//numLineIndex is negative
//MYDEL   		numLineIndex = index + (3 + numLineIndex);
	int topVisible;
	int currentVisibleRowIndex;
	
	topVisible = rc->GetTopIndex();
//MYDEL   	bottomVisible = topVisible + rc->GetCountPerPage();
//MYDEL   	if(bottomVisible > rc->GetItemCount() - 1)
//MYDEL   			bottomVisible = rc->GetItemCount() - 1;

	if(index < topVisible)
		currentVisibleRowIndex = index - 3;
	else
		currentVisibleRowIndex = index + 3;

	if(currentVisibleRowIndex > rc->GetItemCount() - 1)
		currentVisibleRowIndex = rc->GetItemCount() - 1;
	else
	{
		if(currentVisibleRowIndex < 0)
			currentVisibleRowIndex = 0;
	}

	rc->EnsureVisible(currentVisibleRowIndex, FALSE);
	TRACE(_T("currentVisibleRowIndex: %d\n"), currentVisibleRowIndex);
	//GetMainTab()->m_wndList.EnsureVisible(
	rc->SetItemBkColor(index, -1, RGB(0xC6,0xC6,0xFF));
	//set task pane
	CString s;
	CACSDoc* pDoc = (CACSDoc*)CFunctionHelper::GetActiveDocument();
	s.Format(_T("row: %d/%d"), index + 1, pDoc->m_listWindowAction.GetSize());
	CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd();
	pMainFrame->m_wndStatusBar.SetPaneText(1,s);

	return 1;
}

void CACSView::ResizeFrameToFit()
{
	if(!m_bIsResizeToFit)
	{
		if(!IsWindowVisible() && m_bIsResizeToFit)
			return;
		m_bIsResizeToFit = TRUE;		
//MYDEL      		CString s1,s2;
//MYDEL      		CRect rectTemplate, r2;
//MYDEL      		GetWindowRect(rectTemplate);
//MYDEL      		GetParentFrame()->GetWindowRect(r2);
//MYDEL      		//SetScrollSizes(MM_TEXT, rectTemplate.Size());
//MYDEL      		s1.Format(_T("view: %d-%d, frame: %d-%d\n"), rectTemplate.bottom, rectTemplate.right, r2.bottom, r2.right);
//MYDEL      		TRACE(s1);
//MYDEL      		AfxMessageBox(s1);

		GetParentFrame()->RecalcLayout();
		ResizeParentToFit(FALSE);
//MYDEL   		GetParentFrame()->CenterWindow();

//MYDEL      		GetWindowRect(rectTemplate);
//MYDEL      		GetParentFrame()->GetWindowRect(r2);
//MYDEL      		s2.Format(_T("after resize: view: %d-%d, frame: %d-%d\n"), rectTemplate.bottom, rectTemplate.right, r2.bottom, r2.right);
//MYDEL      		TRACE(s2);
//MYDEL      		AfxMessageBox(s2);
	}
	
}

LRESULT CACSView::OnScriptRepeat(WPARAM wParam, LPARAM lParam)
{
	CReportCtrl* rc = &(GetMainTab()->m_wndList);
	rc->SetItemBkColor(-1, -1, RGB(0xFF,0xFF,0xFF));
	return 1;
}

void CACSView::RefreshThread()
{
	//CACSView* pV = (CACSView*)CFunctionHelper::GetActiveView();
	CACSDoc* m_pDoc = (CACSDoc*)CFunctionHelper::GetActiveDocument();
	switch(m_statusTimer)
	{
	case PSTOP:
		DWORD exitCode;
		::GetExitCodeThread(m_pThreadCheckingTime, &exitCode);
		if(exitCode == STILL_ACTIVE)
		{
			WaitForSingleObject(m_pThreadCheckingTime, INFINITE);
		}

//MYDEL		MyUnUseDLL();
		break;
	case PPAUSE:	
		m_pThreadCheckingTime->SuspendThread();
		break;
	case PRUN:
		if(!m_pThreadCheckingTime)//NOT CREATED
		{
//MYDEL			MyUseDLL();
			if(m_pDoc->m_pInfoCheckingTime)
			{
				delete m_pDoc->m_pInfoCheckingTime;
				m_pDoc->m_pInfoCheckingTime = 0;
			}
			m_pDoc->m_pInfoCheckingTime = new THREADINFOTIMECHECKING;
			m_pDoc->m_pInfoCheckingTime->hWndToSendMsg = m_hWnd;			
				
			m_pThreadCheckingTime = AfxBeginThread(ThreadRunningTimeChecking, (PVOID)m_pDoc->m_pInfoCheckingTime, 
				THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
			m_pThreadCheckingTime->m_bAutoDelete = false;
			m_pThreadCheckingTime->ResumeThread();
		}
		else
		{
			DWORD dwExitCode;
			::GetExitCodeThread (m_pThreadCheckingTime->m_hThread, &dwExitCode);
			if (dwExitCode == STILL_ACTIVE) 
			{
				m_pThreadCheckingTime->ResumeThread();
			}
			else
			{
				delete m_pThreadCheckingTime;
				m_pThreadCheckingTime = 0;
//MYDEL				MyUseDLL();
				if(m_pDoc->m_pInfoCheckingTime)
				{
					delete m_pDoc->m_pInfoCheckingTime;
					m_pDoc->m_pInfoCheckingTime = 0;
				}
				m_pDoc->m_pInfoCheckingTime= new THREADINFOTIMECHECKING;
				m_pDoc->m_pInfoCheckingTime->hWndToSendMsg = m_hWnd;
							
				m_pThreadCheckingTime = AfxBeginThread(ThreadRunningTimeChecking, (PVOID)m_pDoc->m_pInfoCheckingTime, 
					THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
				m_pThreadCheckingTime->m_bAutoDelete = false;				
				m_pThreadCheckingTime->ResumeThread();
			}
		}
		
		break;
	default:
		break;
	}
	return;
}



BOOL CheckTime()
{
	BOOL bRs = FALSE;
	COleDateTime timeNow = COleDateTime::GetCurrentTime();

//MYDEL   	if((ULONG) m_timeChecking.m_dt != (ULONG) timeNow.m_dt)
//MYDEL   		return bRs;
//MYDEL   	//minute
//MYDEL   	if(m_timeChecking.GetMinute() == timeNow.GetMinute())
//MYDEL   	{
//MYDEL   		//hour
//MYDEL   		if(m_timeChecking.GetHour() == timeNow.GetHour())
//MYDEL   		{
//MYDEL   			//second
//MYDEL   			if(m_timeChecking.GetSecond() == timeNow.GetSecond())
//MYDEL   			{
//MYDEL   				bRs = TRUE;
//MYDEL   			}
//MYDEL   		}
//MYDEL   	}
	//Note: COleDateTime does not store the milisecond => if you want => use custom CHighTime class
	
	COleDateTimeSpan ts = m_timeChecking.m_dt - COleDateTime::GetCurrentTime().m_dt;
//MYDEL   	CString s;
//MYDEL   		s.Format(_T("%f, %f, %f\n"),ts.GetTotalSeconds(), m_timeChecking.m_dt, COleDateTime::GetCurrentTime().m_dt);
//MYDEL   		TRACE(s);
	if(ts.GetTotalSeconds() == 0)
	{
		
		bRs = TRUE;
	}
	return bRs;
}

void CACSView::MySetCurrentCheckingTime()
{
	//int iCurrentRecAlartBackup = m_currentIndexWaitingToShow;
	COleDateTime oleDT, oleTempDT;
	COleDateTime oleMinDT;
	COleDateTime oleDTNow = COleDateTime::GetCurrentTime();
	//COleDateTime oleMinDT;
	//MYDEL	CRecordNote *pRectNote;
	//MYDEL	CStringArray arrStr;
	bool isFirstItem = true;	
	eFrequency eFreItem;
	CACSApp* pApp = (CACSApp*) AfxGetApp();
	//get enable setting
	m_isHaveCurrentTimeCheking = FALSE;	
	if(!pApp->m_xmlSetting.GetSettingLong(_T("SCHEDULE"), _T("ENABLE"), 0))
		return;
	//get datetime setting
	CString sDt = pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("DATETIME"), _T(""));
	if(sDt.GetLength() == 0)
		sDt.Format(_T("%f"), COleDateTime::GetCurrentTime().m_dt);
	
	oleDT.m_dt = CFunctionHelper::ConvertCStringToDouble(sDt);
	//get frequency setting
	eFreItem = CFunctionHelper::GetFrequencyEnum(pApp->m_xmlSetting.GetSettingString(_T("SCHEDULE"), _T("FREQUENCY"), _T("ONCE")));
	//now get actual time checking
	oleDT = CFunctionHelper::GetPresentDT(oleDT, eFreItem);

	switch(eFreItem)
	{
	case ONCE:
		if(oleDT > oleDTNow)
		{
			if(isFirstItem)
			{
				oleMinDT = oleDT;
				m_isHaveCurrentTimeCheking = TRUE;
				isFirstItem = false;
			}
			else
			{					
				if(oleDT < oleMinDT)
				{
					oleMinDT = oleDT;
				}
			}
		}
		break;
	case DAILY:			
	case WEEKLY:			
	case MONTHLY:
		if(isFirstItem)
		{
			oleMinDT = oleDT;
			m_isHaveCurrentTimeCheking = TRUE;
			isFirstItem = false;
		}
		else
		{
			if(oleDT < oleMinDT)
			{
				oleMinDT = oleDT;
			}
		}
		break;
	default:			
		break;
	}//end switch
	if(m_isHaveCurrentTimeCheking)
		m_timeChecking = oleMinDT;
	return;
}

void CACSView::ReSetThreadCheckingTime()
{
	CACSApp* pApp = (CACSApp*) AfxGetApp();
	if(!pApp->m_xmlSetting.GetSettingLong(_T("SCHEDULE"), _T("ENABLE"), 0))
	{
		m_statusTimer = PSTOP;
	}
	else
	{
		MySetCurrentCheckingTime();
		if(!m_isHaveCurrentTimeCheking)
		{
			m_statusTimer = PSTOP;
		}
		else
		{
			m_statusTimer = PRUN;
		}
	}
	RefreshThread();
}

//exitcode 0: enable = false
//exitcode 1: enable = true and waiting for a schedule time
UINT ThreadRunningTimeChecking(PVOID pParam)
{
	int exitCode = 0;
	THREADINFOTIMECHECKING *pInfo = reinterpret_cast<THREADINFOTIMECHECKING*> (pParam);

	//MySetCurrentCheckingTime();
	if(!m_isHaveCurrentTimeCheking)
		return exitCode;

	while(m_statusTimer == PRUN && m_isHaveCurrentTimeCheking == TRUE)
	{
		if(m_statusTimer == PSTOP)
		{
			exitCode = 2;
			break;
		}

		if(!m_isHaveCurrentTimeCheking)
		{
			exitCode = 3;
			break;
		}

		if(CheckTime())
		{
			//on time
			exitCode = 1;
			m_statusTimer = PSTOP;
			break;
		}
		PostMessage(pInfo->hWndToSendMsg, MYWM_TIME_REMAIN, 4, 0);
		Sleep(1000);
	}
	PostMessage(pInfo->hWndToSendMsg, MYWM_TIME_REMAIN, exitCode, 0);
	return exitCode;
}

LRESULT CACSView::OnTimeRemain(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case 0://not go to while loop
		break;
	case 1://on time
		break;
	case 2://status timer = PSTOP
		break;
	case 3://is_HaveCurrentChecking = false
		break;
	case 4://receive for every each seconds
		COleDateTimeSpan ts;
		CString sTimeRemain;
		ts = m_timeChecking - COleDateTime::GetCurrentTime();
		sTimeRemain.Format(_T("D: %d, h: %d, m: %d, s: %d"), 
			ts.GetDays(), ts.GetHours(), ts.GetMinutes(), ts.GetSeconds());
		SetDlgItemText(lblViewTimeRemain, sTimeRemain);
		break;
	}
	return 1;
}

void CACSView::OnToolSchedule() 
{
	// TODO: Add your command handler code here
	CRecordNoteDlg recDlg;
	recDlg.DoModal();
	ReSetThreadCheckingTime();
}
