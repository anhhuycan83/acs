//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ACS.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             102
#define IDD_ACS_FORM                    103
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_ACSTYPE                     129
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     131
#define IDR_ContextNoMenu               132
#define IDB_BITMAP1                     134
#define IDD_POPUP_RECORD_NOTE           135
#define IDC_TABCTRL1                    1000
#define chbSetActive                    1002
#define chbMoveMouseBeforClick          1003
#define txtWAIF                         1004
#define IDC_btnHide                     1005
#define txtActiveCode                   1006
#define IDC_btnExit                     1007
#define txtSerial                       1008
#define txtNewLicense                   1009
#define IDC_LIST1                       1010
#define txtStartDate                    1011
#define chbStartup                      1012
#define txtEndDate                      1013
#define IDC_DATETIMEPICKER1             1013
#define dtpSchedule                     1013
#define txtFileStore                    1014
#define txtMsg                          1014
#define txtCurrentLicense               1015
#define rbDaily                         1015
#define rbScheduleDaily                 1015
#define btnSave                         1016
#define rbWeekly                        1016
#define rbScheduleWeekly                1016
#define lblRegisterMsg                  1017
#define rbMonthly                       1017
#define rbScheduleMonthly               1017
#define btnDefault                      1018
#define rbOnce                          1018
#define rbScheduleOnce                  1018
#define txtContact                      1019
#define btnAdd                          1020
#define chbTopMost                      1021
#define txtFileSound                    1022
#define btnRecord                       1023
#define IDC_btnRegister                 1024
#define btnEdit                         1025
#define chbShowAtStart                  1026
#define btnRegister                     1027
#define btnDelete                       1028
#define rbF10                           1029
#define btnStop                         1030
#define rbF11                           1031
#define btnRun                          1032
#define IDC_OK                          1033
#define txtTemp                         1034
#define chbRepeat                       1035
#define IDC_CHECK1                      1036
#define chbSchedueEnable                1036
#define chbScheduleEnable               1036
#define btnScheduleSave                 1037
#define btnScheduleClose                1038
#define lblViewTimeRemain               1038
#define lblVersion                      1039
#define pmMitExit                       32771
#define pmMitShow                       32772
#define ID_TOOLS_SAVE                   32773
#define ID_TOOLS_OPEN                   32774
#define mitSave                         32775
#define mitOpen                         32776
#define ID_BUTTON32778                  32778
#define ID_TOOL_SCHEDULE                32778
#define ID_INDICATOR_MYSTATUS           59142

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           136
#endif
#endif
