#if !defined(AFX_MYTABCTRL_H__91D62A09_9F58_4FD3_B0D5_BD5C3EF11D24__INCLUDED_)
#define AFX_MYTABCTRL_H__91D62A09_9F58_4FD3_B0D5_BD5C3EF11D24__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyTabCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MyTabCtrl window

class MyTabCtrl : public CTabCtrl
{
// Construction
public:
	MyTabCtrl();

// Attributes
public:
	int m_nPageCount;

	//Array to hold the list of dialog boxes/tab pages for CTabCtrl
	int m_DialogID[2];
	//CDialog Array Variable to hold the dialogs 
	CDialog *m_Dialog[2];
	//Function to Create the dialog boxes during startup
	void InitDialogs();
	
	//Function to activate the tab dialog boxes 
	void ActivateTabDialogs();
	// Operations

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MyTabCtrl)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~MyTabCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(MyTabCtrl)
	afx_msg void OnSelchange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTABCTRL_H__91D62A09_9F58_4FD3_B0D5_BD5C3EF11D24__INCLUDED_)
