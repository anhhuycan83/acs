// MouseManager.h: interface for the CMouseManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MOUSEMANAGER_H__07C28B19_0AC9_4CEE_B8B9_C8277B56C4C5__INCLUDED_)
#define AFX_MOUSEMANAGER_H__07C28B19_0AC9_4CEE_B8B9_C8277B56C4C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ACSDoc.h"
#include "MouseWindowAction.h"
#include "HighTime.h"	// Added by ClassView

//#define BULLSEYE_CENTER_X_OFFSET		15
//#define BULLSEYE_CENTER_Y_OFFSET		18
//


class CMouseManager  
{
public:
	
	CMouseManager();
	//CMouseManager(CACSView* pView, BsDocTemplate* pDoc);
	//CMouseManger(int x, int y);
	virtual ~CMouseManager();
public:
	void OnMouse(HWND hWnd, WPARAM wParam, LPARAM lParam);
	void SetPoiterDocument(CACSDoc* pDoc);
	CACSDoc* m_pDoc;
	CMouseWindowAction* m_pMouseWindowAction;

	HINSTANCE	g_hInst;
	HWND		g_hwndMainWnd;
	HANDLE		g_hApplicationMutex;
	DWORD		g_dwLastError;
	BOOL		g_bStartSearchWindow;
	HCURSOR		g_hCursorSearchWindow;
	HCURSOR		g_hCursorPrevious;
//	HBITMAP		g_hBitmapFinderToolFilled;
//	HBITMAP		g_hBitmapFinderToolEmpty;
	HWND		g_hwndFoundWindow;
	HPEN		g_hRectanglePen;

	////all functions
public:
	long StartSearchWindowDialog (HWND hwndMain);
	BOOL CheckWindowValidity (HWND hwndDialog, HWND hwndToCheck);
	long DoMouseMove 
 (
   HWND hwndDialog, 
   UINT message, 
   WPARAM wParam, 
   LPARAM lParam
 );
	long DoMouseUp
 (
   HWND hwndDialog, 
   UINT message, 
   WPARAM wParam, 
   LPARAM lParam
 );
	BOOL SetFinderToolImage (HWND hwndDialog, BOOL bSet);
	BOOL MoveCursorPositionToBullsEye (HWND hwndDialog);
	long SearchWindow (HWND hwndDialog);
	long DisplayInfoOnFoundWindow (HWND hwndDialog, HWND hwndFoundWindow);
	long RefreshWindow (HWND hwndWindowToBeRefreshed);
	long HighlightFoundWindow (HWND hwndDialog, HWND hwndFoundWindow);

private:
	CHighTime m_timePre;
	HWND m_hWndOld;
//private:
//	CACSView* m_pView;
//	BsDocTemplate* m_pDoc;
};


#endif // !defined(AFX_MOUSEMANAGER_H__07C28B19_0AC9_4CEE_B8B9_C8277B56C4C5__INCLUDED_)
