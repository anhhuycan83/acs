; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "acs.h"
LastPage=0

ClassCount=12
Class1=CACSApp
Class2=CAboutDlg
Class3=CACSDoc
Class4=CACSView
Class5=BsDocTemplate
Class6=CMainFrame
Class7=CMainTab
Class8=MyTabCtrl
Class9=COptionTab
Class10=CRecordNoteDlg
Class11=CReportCtrl
Class12=CWinStartup

ResourceCount=14
Resource1=IDD_ACS_FORM
Resource2=IDR_ContextNoMenu
Resource3=IDD_ABOUTBOX
Resource4=IDD_DIALOG2
Resource5=IDR_MAINFRAME
Resource6=IDD_ABOUTBOX (English (U.S.))
Resource7=IDD_DIALOG1
Resource8=IDR_ContextNoMenu (English (U.S.))
Resource9=IDR_MAINFRAME (English (U.S.))
Resource10=IDD_ACS_FORM (English (U.S.))
Resource11=IDD_POPUP_RECORD_NOTE (English (U.S.))
Resource12=IDD_DIALOG1 (English (U.S.))
Resource13=IDD_DIALOG2 (English (U.S.))
Resource14=IDD_POPUP_RECORD_NOTE

[CLS:CACSApp]
Type=0
BaseClass=CWinApp
HeaderFile=ACS.h
ImplementationFile=ACS.cpp
LastObject=CACSApp
Filter=N
VirtualFilter=AC

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=ACS.cpp
ImplementationFile=ACS.cpp
LastObject=CAboutDlg
Filter=D
VirtualFilter=dWC

[CLS:CACSDoc]
Type=0
BaseClass=CDocument
HeaderFile=ACSDoc.h
ImplementationFile=ACSDoc.cpp
LastObject=CACSDoc

[CLS:CACSView]
Type=0
BaseClass=CFormView
HeaderFile=ACSView.h
ImplementationFile=ACSView.cpp
Filter=D
VirtualFilter=VWC
LastObject=CACSView

[CLS:BsDocTemplate]
Type=0
BaseClass=CSingleDocTemplate
HeaderFile=BsDocTemplate.h
ImplementationFile=BsDocTemplate.cpp
LastObject=BsDocTemplate

[CLS:CMainFrame]
Type=0
BaseClass=baseCMainFrame
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CMainTab]
Type=0
BaseClass=CDialog
HeaderFile=MainTab.h
ImplementationFile=MainTab.cpp

[CLS:MyTabCtrl]
Type=0
BaseClass=CTabCtrl
HeaderFile=MyTabCtrl.h
ImplementationFile=MyTabCtrl.cpp

[CLS:COptionTab]
Type=0
BaseClass=CDialog
HeaderFile=OptionTab.h
ImplementationFile=OptionTab.cpp

[CLS:CRecordNoteDlg]
Type=0
BaseClass=CDialog
HeaderFile=RecordNoteDlg.h
ImplementationFile=RecordNoteDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=btnScheduleClose

[CLS:CReportCtrl]
Type=0
BaseClass=CListCtrl
HeaderFile=ReportCtrl.h
ImplementationFile=ReportCtrl.cpp

[CLS:CWinStartup]
Type=0
BaseClass=CWnd
HeaderFile=WinStartup.h
ImplementationFile=WinStartup.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=19
Control1=txtActiveCode,edit,1350633600
Control2=txtNewLicense,edit,1350631552
Control3=txtContact,edit,1342249028
Control4=btnRegister,button,1342242816
Control5=IDC_OK,button,1342373888
Control6=IDC_STATIC,static,1342177283
Control7=lblVersion,static,1342308480
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=txtStartDate,edit,1484783744
Control14=txtEndDate,edit,1484783744
Control15=IDC_STATIC,static,1342308352
Control16=txtCurrentLicense,edit,1484783744
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,button,1342177287
Control19=lblRegisterMsg,static,1342308353

[DLG:IDD_ACS_FORM]
Type=1
Class=CACSView
ControlCount=6
Control1=IDC_TABCTRL1,SysTabControl32,1342177280
Control2=IDC_btnHide,button,1342242816
Control3=IDC_btnExit,button,1342242816
Control4=chbSetActive,button,1208025091
Control5=chbMoveMouseBeforClick,button,1208025091
Control6=lblViewTimeRemain,static,1342308352

[DLG:IDD_DIALOG1]
Type=1
Class=CMainTab
ControlCount=8
Control1=IDC_LIST1,SysListView32,1350631424
Control2=btnRecord,button,1342242816
Control3=btnRun,button,1342242816
Control4=btnStop,button,1342242816
Control5=btnAdd,button,1208025088
Control6=btnEdit,button,1208025088
Control7=btnDelete,button,1208025088
Control8=txtWAIF,edit,1350631556

[DLG:IDD_DIALOG2]
Type=1
Class=COptionTab
ControlCount=9
Control1=chbStartup,button,1342242819
Control2=chbTopMost,button,1342242819
Control3=chbShowAtStart,button,1342242819
Control4=rbF10,button,1208090633
Control5=rbF11,button,1207959561
Control6=btnSave,button,1342242816
Control7=btnDefault,button,1342242816
Control8=IDC_STATIC,button,1207959559
Control9=chbRepeat,button,1342242819

[DLG:IDD_POPUP_RECORD_NOTE]
Type=1
Class=CRecordNoteDlg
ControlCount=13
Control1=txtMsg,edit,1350631552
Control2=dtpSchedule,SysDateTimePick32,1342242848
Control3=rbScheduleOnce,button,1342177289
Control4=rbScheduleDaily,button,1342177289
Control5=rbScheduleWeekly,button,1342177289
Control6=rbScheduleMonthly,button,1342177289
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,button,1342177287
Control10=chbScheduleEnable,button,1342242819
Control11=IDC_STATIC,button,1342177287
Control12=btnScheduleSave,button,1342242816
Control13=btnScheduleClose,button,1342242816

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
Command9=ID_TOOL_SCHEDULE
CommandCount=9

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CLEAR_ALL
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_VIEW_TOOLBAR
Command13=ID_VIEW_STATUS_BAR
Command14=ID_HELP_INDEX
Command15=ID_APP_ABOUT
CommandCount=15

[MNU:IDR_ContextNoMenu]
Type=1
Class=?
Command1=pmMitExit
Command2=pmMitShow
CommandCount=2

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=19
Control1=txtActiveCode,edit,1350633600
Control2=txtNewLicense,edit,1350631552
Control3=txtContact,edit,1342249028
Control4=btnRegister,button,1342242816
Control5=IDC_OK,button,1342373888
Control6=IDC_STATIC,static,1342177283
Control7=lblVersion,static,1342308480
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=txtStartDate,edit,1484783744
Control14=txtEndDate,edit,1484783744
Control15=IDC_STATIC,static,1342308352
Control16=txtCurrentLicense,edit,1484783744
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,button,1342177287
Control19=lblRegisterMsg,static,1342308353

[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
Command9=ID_TOOL_SCHEDULE
CommandCount=9

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CLEAR_ALL
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_VIEW_TOOLBAR
Command13=ID_VIEW_STATUS_BAR
Command14=ID_HELP_INDEX
Command15=ID_APP_ABOUT
CommandCount=15

[MNU:IDR_ContextNoMenu (English (U.S.))]
Type=1
Class=?
Command1=pmMitExit
Command2=pmMitShow
CommandCount=2

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_ACS_FORM (English (U.S.))]
Type=1
Class=?
ControlCount=6
Control1=IDC_TABCTRL1,SysTabControl32,1342177280
Control2=IDC_btnHide,button,1342242816
Control3=IDC_btnExit,button,1342242816
Control4=chbSetActive,button,1208025091
Control5=chbMoveMouseBeforClick,button,1208025091
Control6=lblViewTimeRemain,static,1342308352

[DLG:IDD_DIALOG1 (English (U.S.))]
Type=1
Class=?
ControlCount=8
Control1=IDC_LIST1,SysListView32,1350631424
Control2=btnRecord,button,1342242816
Control3=btnRun,button,1342242816
Control4=btnStop,button,1342242816
Control5=btnAdd,button,1208025088
Control6=btnEdit,button,1208025088
Control7=btnDelete,button,1208025088
Control8=txtWAIF,edit,1350631556

[DLG:IDD_DIALOG2 (English (U.S.))]
Type=1
Class=?
ControlCount=9
Control1=chbStartup,button,1342242819
Control2=chbTopMost,button,1342242819
Control3=chbShowAtStart,button,1342242819
Control4=rbF10,button,1208090633
Control5=rbF11,button,1207959561
Control6=btnSave,button,1342242816
Control7=btnDefault,button,1342242816
Control8=IDC_STATIC,button,1207959559
Control9=chbRepeat,button,1342242819

[DLG:IDD_POPUP_RECORD_NOTE (English (U.S.))]
Type=1
Class=?
ControlCount=13
Control1=txtMsg,edit,1350631552
Control2=dtpSchedule,SysDateTimePick32,1342242848
Control3=rbScheduleOnce,button,1342177289
Control4=rbScheduleDaily,button,1342177289
Control5=rbScheduleWeekly,button,1342177289
Control6=rbScheduleMonthly,button,1342177289
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,button,1342177287
Control10=chbScheduleEnable,button,1342242819
Control11=IDC_STATIC,button,1342177287
Control12=btnScheduleSave,button,1342242816
Control13=btnScheduleClose,button,1342242816

